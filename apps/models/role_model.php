<?php
/**
 * Role Models
 *
 * @package 	Alpha Core Bundles
 * @author		veelasky <riefky.alhuraibi@gmail.com>
 *
 * @version		1.0
 */

class Role_model extends Elegant\Model{

	/**
	 * Role has many and blongs to rules.
	 *
	 * @return  Rule
	 */

	protected $table = "roles";

}