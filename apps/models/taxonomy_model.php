<?php
/**
 * Taxonomy Models
 *
 * @package 	Alpha Core Bundles
 * @author		Migz <michael.lianto_dp@yahoo.com>
 *
 * @version		1.0
 */

class Taxonomy_model extends Elegant\Model {

	/*
	 * Initialize relation
	*/
	
	protected $table = "taxonomies";

	//public $includes = array('entries');

	public function __construct($attributes=array(), $exists = false) {
		parent::__construct($attributes, $exists);

		/*
		 * Attaching Event
		*/
		/*
		Event::listen('eloquent.deleted: Taxonomy', function($data){
			$data->entries()->pivot()->where('taxonomy_id','=',$data->id)->delete();
		});
		*/
	}

	/**
	 * Taxonomy has many and belongs to entry.
	 *
	 * @return Entry
	 */

	public function entries() {
		return $this->belongsToMany('Entry_model','entry_taxonomy','taxonomy_id','entry_id');
	}

	/**
	 * Taxonomy has many and belongs to item.
	 *
	 * @return Item
	 */

	public function item() {
		return $this->belongsToMany('Item');
	}

	/**
	 * Taxonomy has many Taxonomy.
	 *
	 * @return Taxonomy
	 */

	public function children() {
		return $this->hasMany('Taxonomy_model','parent');
	}

	/**
	 * Taxonomy belongs to Taxonomy.
	 *
	 * @return Taxonomy
	 */

	public function father() {
		return $this->belongsTo('Taxonomy_model','parent');
	}



	public function products() {
		return $this->belongsToMany('Ecomm_product_model','entry_taxonomy','taxonomy_id','product_id');
	}
}