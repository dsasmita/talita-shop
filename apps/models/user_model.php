<?php
/**
 * User Models
 *
 * @package 	Alpha Core Bundles
 * @author		veelasky <riefky.alhuraibi@gmail.com>
 *
 * @version		1.0
 */

class User_model extends Elegant\Model {
	
	/**
	 * Hide attributes on this model
	 * 
	 * @var array $hidden
	 */
	
	protected $table = "users";
	
	public function details() {
		return $this->hasMany('Usermeta_model','user_id');
	}
}
