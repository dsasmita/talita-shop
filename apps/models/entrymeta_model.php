<?php
/**
 * Entrymeta Models
*
* @package 		Alpha Core Bundles
* @author		Migz <michael.lianto_dp@yahoo.com>
*
* @version		1.0
*/

class Entrymeta_model extends Elegant\Model {

	/**
	 * Entrymeta belongs to Entry.
	 *
	 * @return Entry
	 */

	protected $table = "entrymetas";

	public function entry() {
		return $this->belongsTo('Entry_model','entry_id');
	}
}