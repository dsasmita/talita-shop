<?php
/**
 * Usermeta Models
 *
 * @package 	Alpha Core Bundles
 * @author		veelasky <riefky.alhuraibi@gmail.com>
 *
 * @version		1.0
 */

class Usermeta_model extends Elegant\Model {

	/**
	 * This has one and belongs to
	 *
	 * @return 	obj User
	 */
	protected $table = "usermetas";

	public function user() {
		return $this->belongsTo('User_model');
	}

}