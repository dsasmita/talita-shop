<?php
/**
 * Setting Models
*
* @package 		Alpha Core Bundles
* @author		Migz <michael.lianto_dp@yahoo.com>
*
* @version		1.0
*/

class Setting_model extends Elegant\Model {
	
	/**
	 * Gwet Plugins Setting
	 * 
	 * @return	static $this;
	 */
	
	public static function getPluginsSettings() {
		return static::where('setting_key', 'LIKE','_alpha_plugins_%')->paginate(10);
	}
}