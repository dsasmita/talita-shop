<?php

class Entry_model extends Elegant\Model{

	/*
	 * Initialize relation
	 */

	protected $table = "entries";

	public function __construct($attributes=array(), $exists = false) {
		parent::__construct($attributes, $exists);

	}

	/**
	 * Entry has many and belongs to Taxonomy.
	 *
	 * @return Taxonomy
	 */

	public function taxonomy() {
		return $this->belongsToMany('Taxonomy_model','entry_taxonomy','entry_id','taxonomy_id');
	}
	
	public function media() {
		return $this->belongsToMany('Media_model','entry_media','entry_id','media_id');
	}

	/**
	 * Entry has many EntryMeta.
	 *
	 * @return EntryMeta
	 */

	public function details() {
		return $this->hasMany('Entrymeta_model','entry_id');
	}

	/**
	 * Entry belongs to User.
	 *
	 * @return User
	 */

	public function user() {
		return $this->belongsTo('User_model','author');
	}

	/**
	 * Entry has many Item.
	 *
	 * @return Item
	 */

	public function item() {
		return $this->hasMany('Catalog_Item','product_id');
	}

	/**
	 * Entry has Sub Entry? No Problem.
	 *
	 * @return Entry
	 */

	public function childs() {
		return $this->hasMany('Entry_model', 'entry_parent');
	}

	/**
	 * Retrieve all Taxonomy by entry
	 *
	 * @param 	$entryid
	 * @return 	array $taxonomies_ids
	 */

	public static function getTaxonomyListByEntry($entryid = null) {
		if(empty($entryid)) {
			return false;
		}else{
			$taxo = Entry_taxonomy_model::where('entry_id', $entryid)->get();
			//dd($taxo);
			$return = array();
			if(!empty($taxo)){
				foreach ($taxo as $key => $value) {
					$return[] = $value->taxonomy_id;
				}
			}
		}
		return $return;
	}
	
	public static function getMediaListByEntry($entryid = null) {
		if(empty($entryid)) {
			return false;
		}else{
			$media = Entry_media_model::where('entry_id', $entryid)->get();
			//dd($media);
			$return = array();
			if(!empty($media)){
				foreach ($media as $m) {
					$med = Media_model::where('id',($m->media_id))->first();
					//mpr($med);
					if(!empty($med)){
						$return[] = array(
								'id' => $med->id,
								'path' => $med->path
						);
					}
				}
			}
		}
		return $return;
	}

	/**
	 * Override lastUpdate and determine by entry type
	 *
	 * @param string $type
	 */

	public static function lastUpdate($type='page') {
		return static::where('entry_type', $type)->order_by('updated_at', 'desc')->first();
	}

	public function comments() {
		return $this->hasMany('Comment');
	}

	public static function getFirstMedia($productid = null){
		if(empty($productid)) {
			return false;
		}else{
			$media = Entry_media_model::where('entry_id', $productid)->order_by('updated_at','desc')->first();
			//dd($media);
			if(!empty($media)){
				$med = Media_model::where('id',($media->media_id))->first();

				if(!empty($med)){
					return $med;
				}
			}
		}
		return false;
	}
}
