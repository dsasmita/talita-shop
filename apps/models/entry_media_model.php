<?php

class Entry_media_model extends Elegant\Model{

	/*
	 * Initialize relation
	 */

	protected $table = "entry_media";
	
	public function entry_media(){
		return $this->hasMany('Media_model', 'media_id');
	}
}
