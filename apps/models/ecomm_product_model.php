<?php

class Ecomm_product_model extends Elegant\Model{

	/*
	 * Initialize relation
	 */

	protected $table = "ecom_product";

	public function __construct($attributes=array(), $exists = false) {
		parent::__construct($attributes, $exists);

	}

	public static function getTaxonomyListByProduct($productid = null) {
		if(empty($productid)) {
			return false;
		}else{
			$taxo = Entry_taxonomy_model::where('product_id', $productid)->get();

			$return = array();
			if(!empty($taxo)){
				foreach ($taxo as $key => $value) {
					$return[] = $value->taxonomy_id;
				}
			}
		}
		return $return;
	}
	
	public static function getMediaListByProduct($productid = null) {
		if(empty($productid)) {
			return false;
		}else{
			$media = Entry_media_model::where('product_id', $productid)->get();
			//dd($media);
			$return = array();
			if(!empty($media)){
				foreach ($media as $m) {
					$med = Media_model::where('id',($m->media_id))->first();
					//mpr($med);
					if(!empty($med)){
						$return[] = array(
								'id' => $med->id,
								'path' => $med->path
						);
					}
				}
			}
		}
		return $return;
	}

	public static function getFirstMedia($productid = null){
		if(empty($productid)) {
			return false;
		}else{
			$media = Entry_media_model::where('product_id', $productid)->order_by('updated_at','desc')->first();
			//dd($media);
			if(!empty($media)){
				$med = Media_model::where('id',($media->media_id))->first();

				if(!empty($med)){
					return $med;
				}
			}
		}
		return false;
	}
}
