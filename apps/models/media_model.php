<?php
/**
 * Media Models
 *
 * @package 	Alpha Core Bundles
 * @author		Migz <michael.lianto_dp@yahoo.com>
 *
 * @version		1.0
 */

class Media_model extends Elegant\Model {

	/*
	 * Initialize relation
	*/

	protected $table = "medias";

	public function __construct($attributes=array(), $exists = false) {
		parent::__construct($attributes, $exists);
	}

}