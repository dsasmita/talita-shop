<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class untuk mendapatkan setting dan menuliskan setting.
 * 
 * @author Rio Astamal <me@rioastamal.net>
 * @version 1.0
 */
class Superconfig {
	private $config = array();
	
	/**
	 * Class constructor.
	 *
	 * Load semua setting disini.
	 */
	public function __construct() {
		// ambil semua setting dari tabel PREFIX_settings
		$this->ci = & get_instance();
		$this->ci->load->model(array('setting_m'));
		try {
			
			$settings = $this->ci->setting_m->get_all();
			
			// fill the config property
			$this->config = array();
			$i = 0;
			foreach ($settings as $setting) {
				//mpr($setting->setting_key);
				$this->config[$setting->setting_key] = $setting->setting_value;
				$i++;
			}
		} catch (Exception $e) {
		}
	}
	
	public function add_config($config_name, $config_val) {
		$this->config[$config_name] = $config_val;
		
		return $this;
	}
	
	public function get_config($config_name) {
		
		if (array_key_exists($config_name, $this->config)) {
			return $this->config[$config_name];
		}
		
		return NULL;
	}
	
	function getSetting($key=null) {
		
		return $this->get_config($key);
	}
}
