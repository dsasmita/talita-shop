<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

define('STATUS_ACTIVATED', '1');
define('STATUS_NOT_ACTIVATED', '0');
define('ALLOW', '1');
define('NOT_ALLOW', '0');

Class Autentifikasi {

	private $ci;
	private $error = array();

	public function __construct() {
		$this->ci = & get_instance();
		$this->ci->load->model(array('user_m'));
	}

	public function login($username, $password) {
		if ($this->ci->user_m->count_many(array('username' => $username, 'password' => md5($password))) == 1) {
			$user = $this->ci->user_m->get_by(array('username' => $username, 'password' => md5($password)));
			
			//find avatar
			$avatar = '0';
			$media = getUserMeta($user->id,'avatar');
			if($media){
				$media_upload = Media_model::find($media->meta_value_text);
				if($media_upload){
					$avatar = 'public/'.$media_upload->path;
				}
			}

			$this->ci->session->set_userdata(array(
				'user_id' => $user->id,
				'username' => $user->username,
				'email' => $user->email,
				'user_status' => ($user->status == 'active') ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
				'level' => $user->type,
				'avatar' => $avatar
			));
			$this->ci->user_m->update_by(array('id'=>$user->id), array('last_login' => date('Y-m-d H:i:s', time())));
			redirect(getSetting('admin_path').'/dashboard');
		}
		return false;
	}

	public function logout() {
		$this->ci->session->sess_destroy();
	}

	public function sudah_login($activated = TRUE) {
		return $this->ci->session->userdata('status') === ($activated ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED);
	}

	public function role($level = array()) {
		foreach ($level as $key => $val) {
			$status = $this->ci->session->userdata('level') == $val ? ALLOW : NOT_ALLOW;
			if ($status == 1) 
				break;
		}
		return $status;
	}

}
