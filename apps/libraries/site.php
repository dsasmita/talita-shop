<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

Class Site {
	
	private $ci;

	/**
	 * The System Registered Entries type
	 *
	 * @static	entry
	 * @return	array
	 */

	private static $entry 	= array();

	/**
	 * The System Registered Taxonomy
	 *
	 * @static	taxonomy
	 * @return 	array
	*/

	private static $taxonomy = array();

	/**
	 * The System Registered Widget
	 *
	 * @static	widget
	 * @return 	array
	*/

	private static $widget	= array();

	/**
	 * The System Registered Region
	 *
	 * @static	region
	 * @return	array
	*/

	private static $region = array();

	/**
	 * The System Registered Thumbnail
	 *
	 * @var unknown_type
	*/

	private static $thumbnail = array();

	/**
	 * The System Registered Usertype and meta
	 *
	 * @var unknown_type
	*/

	private static $usertype = array();

	/**
	 * Private Constructor so this class cannot be instantiated;
	 *
	 * @return	void
	*/

	public function __construct() {
		$this->ci = & get_instance();
		$beta = $this->ci->config->item('beta_config');
		$this->Init($beta);
	}

	/**
	 * Initialized this singleton and populate the variable with existing entries
	 *
	 * @return 	void
	 */

	public static function Init($beta = array()) {

		/**
		 * Registering Entry Type
		 *
		 * @return		array
		 */

		self::$entry = $beta['entries'];

		/**
		 * Registering Taxonomy
		 *
		 * @return	array
		 */

		self::$taxonomy = $beta['taxonomies'];

		/**
		 * Registering Default Thumbnails
		 *
		 * @return 	array
		 */

		self::$thumbnail = $beta['thumbnails'];// Config::get('alpha.thumbnails');

		/**
		 * Registering Usertype
		 *
		 * @return 	array
		 */

		self::$usertype = $beta['users'];//Config::get('alpha.users');
	}

	/**
	 * Register New Entry Type
	 *
	 * @param	str $entryname
	 * @param 	array $args
	 * @return 	void
	 *
	 * @tutorial
	 * 	array(
	 * 		'entry-type-name' => array(
	 * 			'hierarchical'		=> true|false,
	 * 			'show_in_menu'		=> true|false,
	 * 			'comment_status'	=> open|closed,
	 * 			'supports'			=> array('title', 'content', 'excerpt', 'thumbnail'),
	 * 			'meta'				=> array(
	 * 				'key			=>	array(
	 * 						'label'			=> default_label,
	 * 						'value'			=> predefined_value,
	 * 						'visibility'	=> public|hidden|system,
	 * 						'type'			=> text|textarea|dropdown|checkbox|radio|multiselect|date,
	 * 						'data_type'		=> text|int|datetime
	 * 				),
	 * 			),
	 * 			'taxonomy'			=> array('category', 'tag', 'color'),
	 * 			'acl_support'		=> false|true
	 * 		)
	 *  );
	 */

	public static function addEntry($entryname, array $args, $addtomenu = false) {
		if (empty($entryname)) return false;

		$template = array(
				'label'				=> array('singular' => null, 'plural' => null),
				'hierarchical'		=> true,
				'show_in_menu'		=> true,
				'comment_status'	=> 'open',
				'supports'			=> array(),
				'meta'				=> array(),
				'taxonomy'			=> array(),
				'acl_support'		=> false,
		);

		$combined = array_merge($template, $args);

		self::$entry = array_merge(self::$entry, array($entryname => $combined));

		if ($addtomenu):
		Menu::container('backend')
		->add(array('key' => $entryname, 'label' => $combined['label']['singular'], 'link' => 'javascript:void(0)', 'icon' => 'icon-entry'))
		->addChild(array('key' => 'view-' . $entryname, 'label' => 'View ' . $combined['label']['plural'], 'link' => url(getBackendPath() . '/entry/' . $entryname)))
		->addChild(array('key' => 'add-' . $entryname, 'label' => 'Add ' . $combined['label']['singular'], 'link' => url(getBackendPath() . '/entry/' . $entryname .'/create')));
		endif;
	}

	/**
	 * Get All Instance of Entry Types
	 *
	 * @return	array
	 */

	public static function getEntry() {
		return self::$entry;
	}

	/**
	 * Get Entry Setting
	 *
	 * @return array
	 */

	public static function getEntrySetting($entryname) {
		if (array_key_exists($entryname, self::$entry)) return self::$entry[$entryname];

		return false;
	}

	/**
	 * Get Taxonomy for each setting
	 *
	 * @return array
	 */
	public static function getEntryTaxonomySetting($entryname) {
		if (array_key_exists($entryname, self::$entry)) return self::$entry[$entryname]['taxonomies'];

		return false;
	}

	/**
	 * Remove Entry Type From Instances
	 *
	 * @param 	str $entryname
	 * @return	boolean
	 */

	public static function removeEntry($entryname) {
		if (array_key_exists($entryname, self::$entry)):
		unset (self::$entry[$entryname]);
		return true;
		endif;

		return false;
	}

	
	/**
	 * Get All Instance of Taxonomy Type
	 *
	 * @return 	array
	 */

	public static function getTaxonomy() {
		return self::$taxonomy;
	}

	/**
	 * Get Taxonomy Setting
	 *
	 * @return array
	 */
	public static function getTaxonomySetting($taxoname) {
		if (array_key_exists($taxoname, self::$taxonomy)) return self::$taxonomy[$taxoname];

		return false;
	}

	/**
	 * Remove Taxonomy From Instances
	 *
	 * @param 	str $taxonomy_name
	 * @return	boolean
	 */

	public static function removeTaxonomy($taxonomy_name) {
		if (array_key_exists($taxonomy_name, self::$taxonomy)):
		unset (self::$taxonomy[$taxonomy_name]);
		return true;
		endif;

		return false;
	}

	/**
	 * Get All Instance of Thumbnail Type
	 *
	 * @return 	array
	 */

	public static function getThumbnail() {
		return self::$thumbnail;
	}

	/**
	 * Remove Thumbnail From Instances
	 *
	 * @param 	str $thumbnail_name
	 * @return	boolean
	 */

	public static function removeThumbnail($thumbnail_name) {
		if (array_key_exists($taxonomy_name, self::$thumbnail)):
		unset (self::$thumbnail[$thumbnail_name]);
		return true;
		endif;

		return false;
	}

	/**
	 * Get All Instance of Usertype and meta
	 *
	 * @return \Alpha\unknown_type Array
	 */
	public static function getUsertype() {
		return self::$usertype;
	}

	public static function getUsertypeSetting($usertype = 'default') {
		if (array_key_exists($usertype, self::$usertype)) return self::$usertype[$usertype];

		return false;
	}
}