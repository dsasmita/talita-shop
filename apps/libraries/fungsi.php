<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Fungsi {

	protected $_ci;

	function __construct() {
		$this->_ci = &get_instance();
	}

	public function halaman($link='', $count_all=0, $limit=0, $segment=0) {
		$this->_ci->load->library('pagination');
		$config['base_url'] = site_url($link);
		$config['total_rows'] = $count_all;
		$config['per_page'] = $limit;
		$config['uri_segment'] = $segment;
		$config['next_link'] = 'next';
		$config['prev_link'] = 'prev';
		$config['last_link'] = '&gt&gt';
		$config['first_link'] = '&lt&lt';
		$this->_ci->pagination->initialize($config);
		return $this->_ci->pagination->create_links();
	}

	public function halaman_admin($link='', $count_all='', $limit='', $segment='') {
		$this->_ci->load->library('pagination');
		$config['num_links'] = 4;
		$config['base_url'] = site_url($link);
		$config['total_rows'] = $count_all;
		$config['per_page'] = $limit;
		$config['uri_segment'] = $segment;
		//number
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		//active
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		//next
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-chevron-right"></i>';
		//prev
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
		//last
		$config['last_tag_open'] = '<li>';
		$config['last_tag_clode'] = '</li>';
		$config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
		//first
		$config['first_tag_open'] = '<li>';
		$config['first_tag_clode'] = '</li>';
		$config['first_link'] = '<i class="fa fa-angle-double-left"></i>';

		$this->_ci->pagination->initialize($config);
		return $this->_ci->pagination->create_links();
	}

	public function halaman_umum($link='', $count_all='', $limit='', $segment='') {
		$this->_ci->load->library('pagination');
		$config['num_links'] = 4;
		$config['base_url'] = site_url($link);
		$config['total_rows'] = $count_all;
		$config['per_page'] = $limit;
		$config['uri_segment'] = $segment;
		//number
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		//active
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		//next
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = 'next';
		//prev
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['prev_link'] = 'prev';
		//last
		$config['last_tag_open'] = '<li>';
		$config['last_tag_clode'] = '</li>';
		$config['last_link'] = 'last';
		//first
		$config['first_tag_open'] = '<li>';
		$config['first_tag_clode'] = '</li>';
		$config['first_link'] = 'first';

		$this->_ci->pagination->initialize($config);
		return $this->_ci->pagination->create_links();
	}

	function kirim_email($from='',$address='', $subject='', $message='') {
		$config = array(
			'protocol' => 'mail',
			'mailpath' => '/usr/sbin/sendmail',
			'wordwrap' => TRUE,
			'mailtype' => 'html'
		);
		$this->_ci->load->library('email');
		$this->_ci->email->initialize($config);

		$this->_ci->email->to($address);
		$this->_ci->email->from($from);
		$this->_ci->email->subject($subject);
		$this->_ci->email->message($message);

		if ($this->_ci->email->send()) {
			return TRUE;
		} else {
			//echo $this->_ci->email->print_debugger();
			return FALSE;
		}
	}



	function auto_tweet($pesan='test') {
		require_once('twitteroauth/twitteroauth.php');

		$tConsumerKey = $this->_ci->config->item('tConsumerKey');
		$tConsumerSecret = $this->_ci->config->item('tConsumerSecret');
		$tAccessToken = $this->_ci->config->item('tAccessToken');
		$tAccessTokenSecret = $this->_ci->config->item('tAccessTokenSecret');

		$tweet = new TwitterOAuth($tConsumerKey, $tConsumerSecret, $tAccessToken, $tAccessTokenSecret);

		$message = $pesan;

		//$msg = $tweet->post('statuses/update', array('status' => $message));
	}

	function auth_ga() {
		require_once('GoogleAnalyticsAPI.php');

		$ga = new GoogleAnalyticsAPI('service');
		$ga->auth->setClientId('728094741367-5q42p5pmdj6tv0qeck05464bom8055e3.apps.googleusercontent.com'); // From the APIs console
		$ga->auth->setEmail('728094741367-5q42p5pmdj6tv0qeck05464bom8055e3@developer.gserviceaccount.com'); // From the APIs console
		$ga->auth->setPrivateKey('./asset/images/d00782f652c3ce8619c9318960e71454e9d83316-privatekey.p12'); // Path to the .p12 file

		if ($this->_ci->session->userdata('access_token')) {
			if ((time() - $this->_ci->session->userdata('tokenCreated') ) >= $this->_ci->session->userdata('tokenExpires')) {
				$auth = $ga->auth->getAccessToken();
				$this->_ci->session->set_userdata(
						array(
							'access_token' => $auth['access_token'],
							'tokenExpires' => $auth['expires_in'],
							'tokenCreated' => time()
						)
				);
			}
		} else {
			$auth = $ga->auth->getAccessToken();
			if ($auth['http_code'] == 200) {
				$this->_ci->session->set_userdata(
						array(
							'access_token' => $auth['access_token'],
							'tokenExpires' => $auth['expires_in'],
							'tokenCreated' => time()
						)
				);
			}
		}
	}

}

/*
 * project :
 * programed by : dadang sasmita
 * @thadangs
 * dadangsasmita@gmail.com
 * location : libraries/fungsi.php
 */

