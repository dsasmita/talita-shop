<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "home";
$route['404_override'] = 'errors';

$kantor = "kantor";

//kantor
//Dashboard
$route[$kantor] = 'kantor/dashboard/index';
$route[$kantor.'/dashboard'] = 'kantor/dashboard/index';

//settings
$route[$kantor.'/setting'] = 'kantor/setting/index';
$route[$kantor.'/setting/(:any)'] = 'kantor/setting/$1';

//credential
$route[$kantor.'/logout'] = 'kantor/credential/logout';
$route[$kantor.'/login'] = 'kantor/credential/index';

//entry
$route[$kantor.'/entry/(:any)/edit/(:any)'] = 'kantor/entry/edit/($1)/$2';
$route[$kantor.'/entry/(:any)/delete/(:any)'] = 'kantor/entry/delete/($1)/$2';
$route[$kantor.'/entry/(:any)/add'] = 'kantor/entry/create/$1';
$route[$kantor.'/entry/(:any)'] = 'kantor/entry/index/($1)';

//taxonomy
$route[$kantor.'/taxonomy/(:any)/delete/(:any)'] = 'kantor/taxonomy/delete/($1)/$2';
$route[$kantor.'/taxonomy/(:any)/edit/(:any)'] = 'kantor/taxonomy/edit/($1)/$2';
$route[$kantor.'/taxonomy/(:any)/add'] = 'kantor/taxonomy/create/$1';
$route[$kantor.'/taxonomy/(:any)/add-ajax'] = 'kantor/taxonomy/create_ajax/$1';
$route[$kantor.'/taxonomy/(:any)/edit-ajax'] = 'kantor/taxonomy/edit_ajax/$1';
$route[$kantor.'/taxonomy/(:any)'] = 'kantor/taxonomy/index/($1)';


//media entry
$route[$kantor.'/media/add_entry_image_ajax'] = 'kantor/media/add_entry_image_ajax/';
$route[$kantor.'/media/delete_entry_image_ajax'] = 'kantor/media/delete_entry_image_ajax/';

//account
$route[$kantor.'/account/edit/(:any)'] = 'kantor/account/edit/$1';
$route[$kantor.'/account/delete/(:any)'] = 'kantor/account/delete/$1';
$route[$kantor.'/account/add'] = 'kantor/account/create/';
$route[$kantor.'/account/(:num)'] = 'kantor/account/index/';
$route[$kantor.'/account'] = 'kantor/account/index/';

//product
$route[$kantor.'/product/edit/(:any)'] = 'kantor/product/edit/$1';
$route[$kantor.'/product/delete/(:any)'] = 'kantor/product/delete/$1';
$route[$kantor.'/product/add'] = 'kantor/product/create';
$route[$kantor.'/product/(:num)'] = 'kantor/product/index/($1)';
$route[$kantor.'/product/variant_add'] = 'kantor/product/variant_add';
$route[$kantor.'/product/variant_del'] = 'kantor/product/variant_del';
$route[$kantor.'/product/variant_update'] = 'kantor/product/variant_update';
$route[$kantor.'/product/variant_update_proses'] = 'kantor/product/variant_update_proses';

//orders
$route[$kantor.'/orders'] = 'kantor/order/index';
$route[$kantor.'/orders/(:num)'] = 'kantor/order/index/($1)';
$route[$kantor.'/order/detail/(:num)'] = 'kantor/order/detail/$1';
$route[$kantor.'/order/status_order'] = 'kantor/order/status_order';
$route[$kantor.'/order/detail_edit'] = 'kantor/order/detail_edit';
$route[$kantor.'/order/orders-confirmation'] = 'kantor/order/orders_confirmation';
$route[$kantor.'/order/orders-confirmation/(:num)'] = 'kantor/order/orders_confirmation/$1';
$route[$kantor.'/order/orders-confirmation-detail/(:num)'] = 'kantor/order/orders_confirmation_detail/$1';

/*------------------------------------------- site routes ----------------------------------------*/
$route['produk/(:any)'] = 'home/produkDetail/$1';
$route['categories/(:any)'] = 'home/categories/$1';
$route['tentang-kami'] = 'home/tentangKami';
$route['kontak-kami'] = 'home/kontakKami';

/* End of file routes.php */
/* Location: ./application/config/routes.php */