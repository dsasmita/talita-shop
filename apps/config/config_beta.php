<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

$config['beta_config'] = array(
	/**
	 * ==========================================================================================================================
	 * List of Default Entry Type
	 */
	'entries'		=> array(
		/*
		 * Entry Type : Page
		 */
		'page'		=> array(
			'label'				=> array('singular' => 'Static Page', 'plural' => 'Static Page\'s'),
			'hierarchical'		=> true,
			'show_in_menu'		=> true,
			'comment_status'	=> 'closed',
			'supports'			=> array('title', 'content', 'excerpt'),
			'meta'				=> array(
				'seo_title'			=> array(
					'label'			=> 'SEO Title',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				),
				'seo_description'		=> array(
					'label'			=> 'SEO Description',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				),
				'seo_keywords'		=> array(
					'label'			=> 'SEO Keywords',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				)
			),
			'taxonomy'			=> array('category'),
			'acl_support'		=> false,
		),
		'blog'		=> array(
			'label'				=> array('singular' => 'Blog', 'plural' => 'Blog\'s'),
			'hierarchical'		=> true,
			'show_in_menu'		=> true,
			'comment_status'	=> 'closed',
			'supports'			=> array('title', 'content', 'excerpt'),
			'meta'				=> array(
				'seo_title'			=> array(
					'label'			=> 'SEO Title',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				),
				'seo_description'		=> array(
					'label'			=> 'SEO Description',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				),
				'seo_keywords'		=> array(
					'label'			=> 'SEO Keywords',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				)
			),
			'taxonomy'			=> array('katblog'),
			'acl_support'		=> false,
		),
		'carousel'		=> array(
			'label'				=> array('singular' => 'Carousel', 'plural' => 'Carousel\'s'),
			'hierarchical'		=> true,
			'show_in_menu'		=> true,
			'comment_status'	=> 'closed',
			'supports'			=> array('title', 'content', 'excerpt'),
			'meta'				=> array(
				'seo_title'			=> array(
					'label'			=> 'SEO Title',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				),
				'seo_description'		=> array(
					'label'			=> 'SEO Description',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				),
				'seo_keywords'		=> array(
					'label'			=> 'SEO Keywords',
					'type'			=> 'text',
					'data_type'		=> 'text',
					'multilanguage'	=> false,
					'value'			=> 0,
					'visibility'	=> 'public',
				)
			),
			'taxonomy'			=> array('katblog'),
			'acl_support'		=> false,
		),
		'email_template'		=> array(
				'label'				=> array('singular' => 'Email Template', 'plural' => 'Email Template'),
				'hierarchical'		=> true,
				'show_in_menu'		=> true,
				'comment_status'	=> 'closed',
				'supports'			=> array('title', 'content', 'excerpt'),
				'meta'				=> array(
						'seo_title'			=> array(
								'label'			=> 'SEO Title',
								'type'			=> 'text',
								'data_type'		=> 'text',
								'multilanguage'	=> false,
								'value'			=> 0,
								'visibility'	=> 'public',
						),
						'seo_description'		=> array(
								'label'			=> 'SEO Description',
								'type'			=> 'text',
								'data_type'		=> 'text',
								'multilanguage'	=> false,
								'value'			=> 0,
								'visibility'	=> 'public',
						),
						'seo_keywords'		=> array(
								'label'			=> 'SEO Keywords',
								'type'			=> 'text',
								'data_type'		=> 'text',
								'multilanguage'	=> false,
								'value'			=> 0,
								'visibility'	=> 'public',
						)
				),
				'taxonomy'			=> array(),
				'acl_support'		=> false,
		)
	),


	/**
	 * ==========================================================================================================================
	 * List of Default Taxonomy Type
	 */
	'taxonomies'	=> array(
		/*
		 * Taxonomy Type : Category
		 */
		'category'		=> array(
				'label'				=> array('single' => 'Category', 'plural' => 'Categories'),
				'hierarchical'		=> true,
				'show_ui'			=> false,
				'slug'				=> 'category',
		)
		,

		/*
		 * Taxonomy Type : Category Blog
		 */
		'katblog'		=> array(
				'label'				=> array('single' => 'Kategori Blog', 'plural' => 'Kategori Blog'),
				'hierarchical'		=> true,
				'show_ui'			=> true,
				'slug'				=> 'katblog',
		),

		/*
		 * Taxonomy Type : Category Product
		 */
		'katproduct'		=> array(
				'label'				=> array('single' => 'Category Product', 'plural' => 'Category Product\'s'),
				'hierarchical'		=> true,
				'show_ui'			=> true,
				'slug'				=> 'katproduct',
		),
		/*
		 * Taxonomy Type : Tag
		 */
		'tag'			=> array(
				'label'				=> array('single' => 'Tag', 'plural' => 'Tags'),
				'hierarchical'		=> false,
				'show_ui'			=> false,
				'slug'				=> 'tag'
		)
	),

	/**
	 * ==========================================================================================================================
	 * List of Default Thumbnail Size
	 */
	'thumbnails'	=> array(
		'default'	=> array(
			'width'		=> 265,
			'height'	=> 265,
			'crop'		=> true
		),
		'thumbnail'	=> array(
			'width'		=> 120,
			'height'	=> 120,
			'crop'		=> true
		),
		'smallthumb' => array(
			'width'		=> 53,
			'height'	=> 40,
			'crop'		=> true
		),
		'artikel' => array(
			'width'		=> 350,
			'height'	=> false,
			'crop'		=> false
		)
	),

	/**
	 * List of Default User Type
	 */
	'users'		=> array(
		'default'	=> array(
			'first_name'	=> array(
				'label'			=> 'First Name',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'last_name'		=> array(
				'label'			=> 'Last Name',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'about'		=> array(
				'label'			=> 'About',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'birth_date'		=> array(
				'label'			=> 'Birth Date',
				'type'			=> 'text',
				'data_type'		=> 'datetime',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'postal_code'		=> array(
				'label'			=> 'Postal Code',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'address'		=> array(
				'label'			=> 'Address',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'city'		=> array(
				'label'			=> 'City',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'province'		=> array(
				'label'			=> 'Province',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'country'		=> array(
				'label'			=> 'Country',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'avatar'		=> array(
				'label'			=> 'Avatar',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'contact_phone'		=> array(
				'label'			=> 'Contact Phone',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			),
			'contact_hp'		=> array(
				'label'			=> 'Contact Hp',
				'type'			=> 'text',
				'data_type'		=> 'text',
				'value'			=> '',
				'visibility'	=> 'public',
			)
		)
	),

	/**
	 * Third Party Setting
	 */
	'thirdparty'	=> array(
		'recaptcha' => array(
			'public_key' 	=> '6LdgaeMSAAAAABli1XA-_fJEnU-9EMpXZsGibCIq',
			'private_key' 	=> '6LdgaeMSAAAAAA3jzr1c5WQIGSKllpIWI1HEo1xP',
		),
		'facebook'	=> array(),
		'twitter'	=> array(),
	)
);

$config['beta_media'] = array(

		/**
		 * List of acceptable mime for specific type
		 *
		 */

		'image'		=> array(
				'image/bmp',
				'image/gif',
				'image/jpeg',
				'image/pjpeg',
				'image/png',
		),

		'audio'	 	=> array(
				'audio/midi',
				'audio/mpeg',
				'audio/mpg',
				'audio/mpeg3',
				'audio/mp3',
				'audio/x-wav'
		),

		'video'		=> array(
				'video/mpeg',
				'video/quicktime',
				'video/x-msvideo',
				'video/x-sgi-movie'
		)
);