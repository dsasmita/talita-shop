<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model {

    private $table;
    private $pk;
    private $table_dml;

    public function __construct() {
        parent::__construct();
    }

    public function set_table($table = '', $pk ='', $dml='') {
        $this->table = $table;
        $this->pk = $pk;
        $this->table_dml = $dml;
        return $this;
    }

    public function get_all($select='*') {
        $this->db->select($select);
        return $this->_get()->result();
    }

    public function get_all_like($select='*', $key='') {
        $this->db->select($select);
        $this->db->or_like($key);
        return $this->_get()->result();
    }

    public function get_all_like_limit($select='*', $key='', $limit=20, $offset=0, $order_by='', $order_rule='DESC') {
        $this->db->select($select);
        $this->db->or_like($key);
        $this->db->limit($limit, $offset);
        $this->db->order_by($order_by, $order_rule);
        return $this->_get()->result();
    }

    public function get_all_limit($select='', $limit=20, $offset=0, $order_by='', $order_rule='DESC') {
        $this->db->limit($limit, $offset);
        if ($order_by == '') {
            $order_by = $this->pk;
        }
        $this->db->order_by($order_by, $order_rule);
        return $this->get_all($select);
    }

    public function get_all_distinct_limit($select='', $limit=20, $offset=0) {
        $this->db->distinct();
        $this->db->limit($limit, $offset);
        return $this->get_all($select);
    }

    public function get_num() {
        return $this->_get()->num_rows();
    }

    public function get($id = '0', $select='*') {
        $this->db->select($select);
        $this->db->where($this->pk, $id);
        return $this->_get()->row();
    }

    public function get_array_by($param, $select='*') {
        $this->db->where($param);
        return $this->get_array($select);
    }

    public function get_by($param, $select='*') {
        $this->db->select($select);
        if (($param)) {
            $this->db->where($param);
            return $this->_get()->row();
        }
        return FALSE;
    }

    public function get_many_array($param, $select='*') {
        if (($param)) {
            $this->db->where($param);
            return self::get_array($select);
        }
        return FALSE;
    }

    public function get_array($select='') {
        $this->db->select($select);
        return $this->_get()->result_array();
    }

    public function get_many($param, $select='*', $limit=100, $offset=0, $order_by='', $order_rule='DESC') {
        if (($param)) {
            $this->db->where($param);
            return self::get_all_limit($select, $limit, $offset, $order_by, $order_rule);
        }
        return FALSE;
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->_count();
    }

    public function count_many($param) {
        if (($param)) {
            $this->db->from($this->table);
            $this->db->where($param);
            return self::_count();
        }
        return FALSE;
    }

    public function count_like($key='') {
        $this->db->from($this->table);
        $this->db->or_like($key);
        return self::_count();
    }

    public function insert($data = array()) {
        if ($this->db->insert($this->table_dml, $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function delete($id = 0) {
        if ($this->db->delete($this->table_dml, array($this->pk => $id))) {
            return true;
        }
        return false;
    }

    public function delete_by($data = array()) {
        $this->db->where($data);
        if ($this->db->delete($this->table_dml)) {
            return true;
        }
        return false;
    }

    public function update($id = 0, $data = array()) {
        $this->db->where(array($this->pk => $id));
        if ($this->db->update($this->table_dml, $data)) {
            return true;
        }
        return false;
    }

    public function update_by($where = array(), $data = array()) {
        $this->db->where($where);
        if ($this->db->update($this->table_dml, $data)) {
            return true;
        }
        return false;
    }

    protected function _get() {
        return $this->db->get($this->table);
    }

    protected function _count() {
        return $this->db->count_all_results();
    }

    protected function query($table='', $where=array()) {
        $this->db->where($where);
        return $this->db->get($table)->result();
    }

}

?>