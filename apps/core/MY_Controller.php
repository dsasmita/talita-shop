<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class MY_Controller_umum extends CI_Controller {

	protected $limit = 20; //limit /page
	protected $segment = 2; //posisi segment

	function __construct() {
		parent::__construct();

		$this->data = new stdClass();
		$this->data->level = 'guest';

		$this->data->level = $this->session->userdata('level');

		$this->load->helper('language');
		$this->lang->load('layout');

		$this->load->helper('cookie');
		
		$pool_key = get_cookie('pool_key');
		if($pool_key){
			
		}else{
			$cookie = array(
				'name' => 'pool_key',
				'value' => md5(time()),
				'expire' => 60*60*24*360,
				'path'   => '/',
			);
			$this->input->set_cookie($cookie);
		}

		$pool_status = get_cookie('pool_status');
		if($pool_status){
			
		}else{
			$cookie = array(
				'name' => 'pool_status',
				'value' => 'belum',
				'expire' => 60*60*24*360,
				'path'   => '/',
			);
			$this->input->set_cookie($cookie);
		}

		$pool_refresh = get_cookie('pool_refresh');
		if($pool_status){
			if($pool_refresh <= 3){
				$cookie = array(
					'name' => 'pool_refresh',
					'value' => $pool_refresh + 1,
					'expire' => 60*60*24*7,
					'path'   => '/',
				);
				$this->input->set_cookie($cookie);
			}
		}else{
			$cookie = array(
				'name' => 'pool_refresh',
				'value' => 0,
				'expire' => 60*60*24*7,
				'path'   => '/',
			);
			$this->input->set_cookie($cookie);
		}

		//$this->output->enable_profiler(TRUE);
	}

	protected function _viewSite($view, $data) {
		if(array_key_exists('sidebar',$data)){
			$data['sidebar'] = $this->load->view('site/partial/sidebar-'.$data['sidebar'].'.blade.php',$data,true);
		}
		$this->load->view('site/partial/header.blade.php', $data);
		$this->load->view('site/partial/topmenu.blade.php', $data);
		$this->load->view('site/'.$view.'.blade.php', $data);
		$this->load->view('site/partial/footer.blade.php', $data);
	}
}

class MY_Controller_admin extends CI_Controller {


	public function __construct() {
		parent::__construct();

		if (!$this->autentifikasi->role(array('Super Administrator', 'Administrator', 'Marketing','Kontributor')))
			redirect(site_url('/'));

		$this->data = new stdClass();

		$this->data->user_id = $this->session->userdata('user_id');
		$this->data->username = $this->session->userdata('username');
		$this->data->user_status = $this->session->userdata('user_status');
		$this->data->user_level = $this->session->userdata('level');
		//dd($this->data->user_level);
		$this->data->judul = 'Halaman Admin';

		$this->data->username = $this->session->userdata('username');

		$this->base_admin = base_url(getSetting('admin_path'));
	}

	protected function _view($view, $data) {
		$data['base_admin'] = $this->base_admin;

		if (!empty($data['script']))
			$data['script'] = $this->load->view($data['script'].'.blade.php',$data,true);

		$this->load->view('admin/partial/header.blade.php', $data);
		$this->load->view('admin/partial/sidebar.blade.php', $data);
		$this->load->view('admin/partial/topmenu.blade.php', $data);
		$this->load->view('admin/'.$view.'.blade.php', $data);
		$this->load->view('admin/partial/footer.blade.php', $data);
	}

}
