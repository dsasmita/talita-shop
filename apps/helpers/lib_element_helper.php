<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

function form_text($params){
	$name		= (isset($params['name']) ? $params['name'] : '');
	$label 		= (isset($params['label']) ? $params['label'] : '');
	$id 		= (isset($params['id']) ? $params['id'] : '');
	$class 		= (isset($params['class']) ? $params['class'] : array('medium'));
	$value		= (isset($params['value']) ? $params['value'] : '');
	$lang 		= (isset($params['lang']) ? ' lang-'. $params['lang'] : '');
	$class 		= (!empty($class) ? implode(' ', $class) : '');
	/*
	$return = 
		'<div class="field '. $lang.'">
			<div class="label">
				<label for="'. $id.'">'. $label.'</label>
			</div>
			<div class="input">
				<input type="text" class="'.$class.'" id="'.$id.'" name="'.$name.'" value="'.$value.'"/>
			</div>
		</div>';
		*/
	$return = 
		'<div class="form-group">
            <label>'. $label.'</label>
            <input class="form-control" name="'.$name.'" placeholder="'. $label.'" value="'.$value.'">
        </div>';
	return $return;
}