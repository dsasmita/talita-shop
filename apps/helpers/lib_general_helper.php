<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Get Site Setting
 *
 * @return string;
 */

function getEntry($key = '') {
	$CI =& get_instance();
	return $CI->site->getEntry();
}

function getTaxonomy($key = '') {
	$CI =& get_instance();
	return $CI->site->getTaxonomy();
}

function getEmailContentReplace($action='', $array='') {
	$type='email_template';
	$email = Entry_model::where('slug',$action)->where('entry_type', $type)->first();

	$msg =  parse_multi_lang($email->content);
	foreach ($array as $key => $value) {
		$msg = str_replace($key, $value,$msg);
	}
	return $msg;
}

/*
 * project : e-pasar.net
 * programed by : dadang sasmita
 * @thadangs
 * dadangsasmita@gmail.com
 * location : helpers/lib_func_helper.php
 */


