<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * @author Rio Astamal <me@rioastamal.net>
 */
/*
function parse_multi_lang($content, $lang=null) {
	$CI =& get_instance();
	$result = parse_lang($content,$lang);
	$lang 		= (empty($lang)) ? $CI->lang->lang() : $lang;

	$return = "";

	if (isset($result[$lang]) and $result[$lang] != "")
		$return = $result[$lang];
	else if (isset($result[$CI->lang->lang()]) and $result[$CI->lang->lang()] != "")
		$return = $result[$CI->lang->lang()];

	return $return;
}

function parse_multi_lang_string($content, $input, $lang=null) {
	$CI =& get_instance();
	$result = parse_lang($content,$lang);
	$result[$CI->lang->lang()] =  $input;
	$out = "";
	foreach ($result as $k => $v)
		$out .= "[:$k]".$v;
	return $out;
}

function parse_lang($content,$lang) {
	$CI =& get_instance();
	//$lang 		= (empty($lang)) ? $CI->lang->lang() : $lang;
	$lang 		= (empty($lang)) ? 'id' : $lang;
	$splitter 	= '#(\[:[a-z]{2}\])#ism';
	$result		= array();

	foreach ($CI->lang->lang_available() as $language):
		$result[$language] = '';
	endforeach;

	$blocks = preg_split($splitter, $content, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
	$current = '';
	foreach ($blocks as $block):
		if(preg_match('#^\[:([a-z]{2})\]$#ism', $block, $matches)):
			$current = $matches[1];
			continue;
		endif;
		$result[$current] = trim($block);
	endforeach;
	return $result;
}

function ___($key, $replacements = array(), $language = null, $default = null) {
	$CI =& get_instance();

	$tmp = __($key, $replacements, $language)->get();
	if ($tmp == $key) {
		if (empty($default)) {
			$tmp2 = __($key, $replacements, $CI->lang->lang())->get();
			if ($tmp2 != $key)
				return $tmp2;
		}
		else
			return $default;
	}
	return $tmp;
}
*/
