<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function convert_harga($param) {
    return 'Rp. ' . number_format($param, 0);
}

function tampil_waktu($waktu ='') {
    $waktu_split = explode(' ', $waktu);
    $tanggal_split = explode('-', $waktu_split[0]);
    $bulan = array('bulan', 'Januari', 'Februari', 'Maret', 'April', 'Mei',
        'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    if (count($waktu_split) == 2) {
        return $tanggal_split[2] . ' ' . $bulan[(int) $tanggal_split[1]] . ' ' . $tanggal_split[0] . ' ' . $waktu_split[1];
    } else {
        return $tanggal_split[2] . ' ' . $bulan[(int) $tanggal_split[1]] . ' ' . $tanggal_split[0];
    }
}

function dd($value)
{
	echo "<pre>";
	var_dump($value);
	echo "</pre>";
	die;
}

/**
 * Preformatted var_dump()
 *
 * @author Morra Team
 * @author Irwan <irwan.wiyono@gmail.com>
 */
function mpr() {
	$args = func_get_args();
	echo "<pre>";
	foreach ($args as $k => $v) {
		echo "mpr".($k + 1).":\n";
		var_dump($v);
		echo "\n";
	}
	echo "</pre>";
}

function toBesarKecil($param='') {
    return trim(ucwords(strtolower($param)));
}
function select_tanggal($param='0000-00-00', $tahun=0) {
    //tanggal
    $waktu = explode('-', $param);

    $bulan = array('-bln-', '01', '02', '03', '04', '05', '06',
        '07', '08', '09', '10', '11', '12');

    //tahun
    echo '<input id="tahun" name="tahun" type="text" maxlength="4" class="span1" value="'.@$waktu[0].'" placeholder="yyyy">';

    //bulan
    echo '<select id="bulan" name="bulan" class="span1">';
    for ($i = 0; $i < count($bulan); $i++) {
        echo '<option value="' . $i . '"' . ($i == (int) @$waktu[1] ? 'selected' : '') . '>
            ' . $bulan[$i] . '</option>';
    }
    echo '</select>';

    //tanggal
    echo '<select id="tanggal" name="tanggal" class="span1">';
    echo '<option value="0">-tgl-</option>';
    for ($index = 1; $index <= 31; $index++) {
        echo '<option value="' . $index . '" ' . ($index == (int) @$waktu[2] ? 'selected' : '') . '>
            ' . $index . '</option>';
    }
    echo '</select>';
}

function getParsedLink($link, $profile = null, $is_S3 = false) {
	$link = str_replace('/', DS, $link);
	$path = $link;
	$name = pathinfo($link, PATHINFO_FILENAME);
	$ext = pathinfo($link, PATHINFO_EXTENSION);
	$link = pathinfo($link, PATHINFO_DIRNAME);
	//$template_profile = Alpha\Site::getThumbnail();
	$link = str_replace(DS, '/', $link);

	if($is_S3) {
	} else {
		if(empty($profile)) {
			$link = $link.'/'.$name.'.'.$ext;
		} else {
			//if(array_key_exists($profile, Alpha\Site::getThumbnail()))
				$link = $link.'/'.$name.'_'.$profile.'.'.$ext;
			//else
				//return false;
		}
		return $link;
	}
}

/* form validation */
function form_val_alpha_dash($str) {
	return (!preg_match("/^([-a-z0-9_-])+$/i", $str)) ? FALSE : TRUE;
}

function form_val_valid_email($str) {
	return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
}

/*
 * project : e-pasar.net
 * programed by : dadang sasmita
 * @thadangs
 * dadangsasmita@gmail.com
 * location : helpers/lib_func_helper.php
 */


