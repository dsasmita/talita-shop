<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Get Site Setting
 *
 * @return string;
 */

function getSetting($key = '') {
	$CI =& get_instance();
	return $CI->superconfig->getSetting($key);
}

/**
 * Update Site Setting
 *
 * @return string;
 */

function updateSiteSetting($settingKey=null,$settingValue=null) {
	if (empty($settingKey) and empty($settingValue))
		return false;
	else
	{
		$CI =& get_instance();
		$CI->load->model('setting_m');
		return $CI->setting_m->update_by(array('setting_key' => $settingKey), array('setting_value' => $settingValue));
	}
}
/*
 * project : e-pasar.net
 * programed by : dadang sasmita
 * @thadangs
 * dadangsasmita@gmail.com
 * location : helpers/lib_func_helper.php
 */


