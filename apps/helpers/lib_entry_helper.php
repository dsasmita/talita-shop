<?php

function getEntryMeta($entryId=null,$metaKey=null, $param = array()){
	if (empty($entryId) and empty($metaKey))
		return false;
	else {
		if(empty($param))
			return $meta = Entrymeta_model::where('entry_id', $entryId)->where('meta_key', $metaKey)->first();
		else
			return $meta = Entrymeta_model::where('entry_id', $entryId)->where('meta_key', $metaKey)->get();
	}
}