<?php

function getUserMeta($userId=null,$metaKey=null, $param = array()){
	if (empty($userId) and empty($metaKey))
		return false;
	else {
		if(empty($param))
			return $meta = Usermeta_model::where('user_id', $userId)->where('meta_key', $metaKey)->first();
		else
			return $meta = Usermeta_model::where('user_id', $userId)->where('meta_key', $metaKey)->get();
	}
}