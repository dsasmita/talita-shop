<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller_umum {

	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$this->load->helper('text');
		$data = array();
		$data['menu'] = 'home';
		$data['title'] = getSetting('site_name') .' | '. getSetting('site_slogan');
		$data['product'] = Ecomm_product_model::where('status !=','deleted')->order_by('updated_at','DESC')->limit(12)->get();
		$data['best_product'] = Ecomm_product_model::where('best','yes')->where('status !=','deleted')->order_by('updated_at','DESC')->limit(12)->get();
		$data['carousel'] = Entry_model::where('entry_type','carousel')->where('status !=','deleted')->order_by('updated_at','DESC')->get();
		$this->_viewSite('pages/front', $data);
	}

	public function produkDetail($slug = null){
		$detail = Ecomm_product_model::where('slug',$slug)->where('status !=','deleted')->order_by('updated_at','DESC')->first();

		if(!$detail){
			redirect(base_url('/'));
		}

		$this->load->helper('text');
		$data = array();
		$data['detail'] = $detail;
		$data['menu'] = 'produk-detail';
		$data['title'] = getSetting('site_name') .' | '. $detail->name;
		$data['product'] = Ecomm_product_model::where('status !=','deleted')->order_by('updated_at','DESC')->limit(6)->get();
		$data['best_product'] = Ecomm_product_model::where('best','yes')->where('status !=','deleted')->order_by('updated_at','DESC')->limit(12)->get();
		$this->_viewSite('pages/produk-detail', $data);
	}

	public function tentangKami(){
		$page = Entry_model::where('slug','tentang-kami')->where('status !=','deleted')->order_by('updated_at','DESC')->first();

		$this->load->helper('text');
		$data = array();
		$data['page'] = $page;
		$data['menu'] = 'tentang-kami';
		$data['title'] = getSetting('site_name') .' | '. $page->title;
		$data['product'] = Ecomm_product_model::where('status !=','deleted')->order_by('updated_at','DESC')->limit(6)->get();
		$this->_viewSite('pages/tentang-kami', $data);
	}

	public function kontakKami(){
		$page = Entry_model::where('slug','kontak-kami')->where('status !=','deleted')->order_by('updated_at','DESC')->first();

		$this->load->helper('text');
		$data = array();
		$data['page'] = $page;
		$data['menu'] = 'tentang-kami';
		$data['title'] = getSetting('site_name') .' | '. $page->title;
		$data['product'] = Ecomm_product_model::where('status !=','deleted')->order_by('updated_at','DESC')->limit(6)->get();
		$this->_viewSite('pages/kontak-kami', $data);
	}

	public function categories($slug = null){
		$taxonomy = Taxonomy_model::where('taxonomy_slug',$slug)->where('taxonomy_type','katproduct')->first();
		
		if(! empty($taxonomy)){
			$this->load->helper('text');

			$offset = $this->uri->segment(4);
			if (! is_numeric($offset)) {
				$offset = 0;
			}
			$limit = 12;
			$segment = 4;
			$link = base_url().'/categories/'.$slug;

			$data = array();
			$data['menu'] = $slug;

			//$entry = $taxonomy->entries()->where('status','published')->get();
			$entry =  $this->db->query('SELECT pr.`id` product_id
										FROM
											`beta_ecom_product` pr
											LEFT JOIN `beta_entry_taxonomy` et ON et.`product_id` = pr.`id`
										WHERE
											pr.`status` = "published"
											AND et.`taxonomy_id` = "'.$taxonomy->id.'"');
			//dd($entry);

			$count = $entry->num_rows();

			if($count > 0){
				$product = $taxonomy->products()->where('status','published')->limit($limit, $offset)->order_by('created_at','DESC')->get();
			}else {
				$product = array();
			}

			$data['pagination'] = $this->fungsi->halaman_umum($link, $count,
					$limit, $segment);

			$data['category'] = $taxonomy;
			$data['product'] = $product;
			$data['title'] = $taxonomy->name.' | '. getSetting('site_name');
			$data['best_product'] = Ecomm_product_model::where('best','yes')->where('status !=','deleted')->order_by('updated_at','DESC')->limit(12)->get();
			$this->_viewSite('pages/categories', $data);
		}else{
			redirect('/');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */