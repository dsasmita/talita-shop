<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Credential extends MY_Controller_umum {

	public function __construct() {
		parent::__construct();
	}

	function index() {
		$data = array();
		$data['title'] = $this->superconfig->getSetting('site_name');
		if ($this->input->post()) {

			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$data['status'] = $this->autentifikasi->login($username, $password);
		}

		$this->load->view('admin/pages/login.blade.php',$data);
	}
	public function logout() {
		$this->autentifikasi->logout();
		redirect('/');
	}

}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
