<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Media extends MY_Controller_admin {

	public function __construct() {
		parent::__construct();
	}

	function index() {
		$data = array();
		$data['title'] = 'Dashboard | '.$this->superconfig->getSetting('site_name');
		$this->_view('pages/dashboard', $data);
	}

	function add_entry_image_ajax(){
		if(!$this->input->is_ajax_request()){
			exit('failed');
		}

		$file = isset($_FILES['userfile'])?$_FILES['userfile']:false;
		$return = array();

		if ($file) {
			$dirPath = "uploads".DS.date("Y").DS.date("m").DS;

			$ext = pathinfo($file["name"], PATHINFO_EXTENSION);
			$name = url_title(pathinfo($file["name"], PATHINFO_FILENAME),'_',true) ;

			$this->load->library('getid3/getid3');

			$id3 = new getID3();
			$meta = $id3->analyze($file["tmp_name"]);
			if (!empty($meta["mime_type"])) {
				$type = explode("/", $meta["mime_type"]);
				$type = $type[0];

				$tmp = $this->config->item('beta_media');
				$tmp = $tmp['image'];
				if(!empty($tmp) and in_array($meta["mime_type"], $tmp)) {
					$exist = Media_model::where('path',str_replace(DS, "/", $dirPath).$name.'.'.$ext)->get();

					if(!empty($exist)) {
						$counter = 1;
						while (!empty($exist)) {
							$newtitle = $name . '-' . $counter;
							$counter ++;
							$exist = Media_model::where('path', str_replace(DS, "/", $dirPath).$newtitle.'.'.$ext)->get();
						}
					}else {
						$newtitle = $name;
					}

					$filename = $newtitle.".".$ext;

					$dir = 'public'.DS.$dirPath;
					$config['upload_path'] = $dir;
					$config['allowed_types'] = 'gif|jpg|png';
					$config['raw_name'] = 'dadang';
					$config['file_name'] = $filename;
					//dd($dirPath);

					if(!is_dir('public'.DS.$dirPath))
						mkdir( 'public'.DS.$dirPath,0777,true);

					$this->load->library('upload', $config);

					if ($this->upload->do_upload()) {

						$data['upload_data'] = (array) $this->upload->data();
						$path = $data['upload_data']['full_path'];
						$thumbnails = $this->config->item('beta_config');
						$thumbnails = $thumbnails['thumbnails'];
						$this->load->library('images');

						foreach ($thumbnails as $key=>$thumb){
							if ($thumb['crop']) {
								$this->images->resize($path,
										$dir.$newtitle."_".$key.".".$ext, $thumb['width'], $thumb['height']);
							}else {
								if (!$thumb['width']) {
									$this->images->resize_h($path,
											$dir.$newtitle."_".$key.".".$ext, $thumb['height']);
								}

								if (!$thumb['height']) {
									$this->images->resize_w($path,
											$dir.$newtitle."_".$key.".".$ext, $thumb['width']);
								}
							}
						}

						$id = Media_model::create(array(
								'title'			=> $newtitle,
								'description' 	=> '' ,
								'media_type' 	=> $type,
								'media_mime'	=> $meta['mime_type'],
								'media_meta'	=> serialize($meta),
								'path'			=> str_replace(DS, "/", $dirPath).$filename,
								'author' 		=> $this->data->user_id ,
								'modified_by' 	=> $this->data->user_id ,
								'created_at'	=> date('Y-m-d H:i:s', time()),
								'updated_at'	=> date('Y-m-d H:i:s', time())

						));
						$data = array(
								'status' => 'OK',
								'id' => $id->id,
								'path' => str_replace(DS, "/", $dirPath).$newtitle."_thumbnail.".$ext,
								'path_ori' => str_replace(DS, "/", $dirPath).$newtitle.".".$ext
						);
						echo json_encode($data);
					}else {
						$data = array(
							'status' => 'not OK',
							'message' => 'Terjadi kesalahan, silahkan ulangi kembali.'
						);
						echo json_encode($data);
					}

				}else {
					$data = array(
						'status' => 'not OK',
						'message' => 'format yang diterima PNG, JPG, GIF.'
					);
					echo json_encode($data);
				}
			}else {
				$data = array(
					'status' => 'not OK',
					'message' => 'Silahkan pilih file gambar.'
				);
				echo json_encode($data);
			}
		}else {
			$data = array(
					'status' => 'not OK',
					'message' => 'Silahkan pilih file gambar.'
			);
			echo json_encode($data);
		}
	}

	public function delete_entry_image_ajax(){
		if(!$this->input->is_ajax_request()){
			exit('failed');
		}

		$id = $this->input->post('id');
		$media = Media_model::find($id);
		if (!empty($media)){
			$image = explode('.',$media->path);
			$ext = $image[count($image)-1];

			$tmpImage = array();
			for ($i = 0; $i <= count($image)-2; $i++) {
				array_push($tmpImage, $image[$i]);
			}

			$image = implode('.',$tmpImage);
			$thumbnails = $this->config->item('beta_config');
			$thumbnails = $thumbnails['thumbnails'];

			foreach($thumbnails as $key => $thumb)
			{
				if(file_exists('public/'.$image.'_'.$key.'.'.$ext))
					unlink('public/'.$image.'_'.$key.'.'.$ext);
			}

			if(file_exists('public/'.$media->path))
				unlink('public/'.$media->path);

			$entry_media = Entry_media_model::where('media_id',$id)->delete();
			$media->delete();
		}
		$data = array(
				'status' => 'OK',
				'message' => 'Hapus image berhasil'
		);
		echo json_encode($data);
	}

	/*============================= end ===================================*/

	function form_tambah_image_ajax(){
		$data = array();
		$this->load->view('admin/media/media-add-ajax.blade.php', $data);
	}

	function add_image(){
		$file = $_FILES['userfile'];
		$return = array();

		if (!empty($file)) {
			$dirPath = "uploads".DS.date("Y").DS.date("m").DS;

			$ext = pathinfo($file["name"], PATHINFO_EXTENSION);
			$name = url_title(pathinfo($file["name"], PATHINFO_FILENAME),'_',true) ;

			$this->load->library('getid3/getid3');

			$id3 = new getID3();
			$meta = $id3->analyze($file["tmp_name"]);
			if (!empty($meta["mime_type"])) {
				$type = explode("/", $meta["mime_type"]);
				$type = $type[0];

				$tmp = $this->config->item('beta_media');
				$tmp = $tmp['image'];
				if(!empty($tmp) and in_array($meta["mime_type"], $tmp)) {
					$exist = Media_model::where('path',str_replace(DS, "/", $dirPath).$name.'.'.$ext)->get();

					if(!empty($exist)) {
						$counter = 1;
						while (!empty($exist)) {
							$newtitle = $name . '-' . $counter;
							$counter ++;
							$exist = Media_model::where('path', str_replace(DS, "/", $dirPath).$newtitle.'.'.$ext)->get();
						}
					}else {
						$newtitle = $name;
					}

					$filename = $newtitle.".".$ext;

					$dir = 'public'.DS.$dirPath;
					$config['upload_path'] = $dir;
					$config['allowed_types'] = 'gif|jpg|png';
					$config['raw_name'] = 'dadang';
					$config['file_name'] = $filename;
					//dd($dirPath);

					if(!is_dir('public'.DS.$dirPath))
						mkdir( 'public'.DS.$dirPath,0777,true);

					$this->load->library('upload', $config);

					if ($this->upload->do_upload()) {

						$data['upload_data'] = (array) $this->upload->data();
						$path = $data['upload_data']['full_path'];
						$thumbnails = $this->config->item('beta_config');
						$thumbnails = $thumbnails['thumbnails'];
						$this->load->library('images');

						foreach ($thumbnails as $key=>$thumb){
							if ($thumb['crop']) {
								$this->images->resize($path,
										$dir.$newtitle."_".$key.".".$ext, $thumb['width'], $thumb['height']);
							}else {
								if (!$thumb['width']) {
									$this->images->resize_h($path,
											$dir.$newtitle."_".$key.".".$ext, $thumb['height']);
								}

								if (!$thumb['height']) {
									$this->images->resize_w($path,
											$dir.$newtitle."_".$key.".".$ext, $thumb['width']);
								}
							}
						}

						$id = Media_model::create(array(
								'title'			=> $newtitle,
								'description' 	=> '' ,
								'media_type' 	=> $type,
								'media_mime'	=> $meta['mime_type'],
								'media_meta'	=> serialize($meta),
								'path'			=> str_replace(DS, "/", $dirPath).$filename,
								'author' 		=> $this->data->user_id ,
								'modified_by' 	=> $this->data->user_id ,
						));
						$data = array(
								'status' => 'OK',
								'id' => $id->id,
								'path' => str_replace(DS, "/", $dirPath).$newtitle."_thumbnail.".$ext,
								'path_ori' => str_replace(DS, "/", $dirPath).$newtitle.".".$ext
						);
						echo '<script type="text/javascript">';
						echo 'parent.doneUpload('.json_encode($data).');';
						echo '</script>';
					}else {
						$data = array(
							'status' => 'not OK',
							'message' => 'Terjadi kesalahan, silahkan ulangi kembali.'
						);
						echo '<script type="text/javascript">';
						echo 'parent.errorPorses('.json_encode($data).');';
						echo '</script>';
					}

				}else {
					$data = array(
						'status' => 'not OK',
						'message' => 'format yang diterima PNG, JPG, GIF.'
					);
					echo '<script type="text/javascript">';
					echo 'parent.errorPorses('.json_encode($data).');';
					echo '</script>';
				}
			}else {
				$data = array(
					'status' => 'not OK',
					'message' => 'Silahkan pilih file gambar.'
				);
				echo '<script type="text/javascript">';
				echo 'parent.errorPorses('.json_encode($data).');';
				echo '</script>';
			}
		}else {
			$data = array(
					'status' => 'not OK',
					'message' => 'Silahkan pilih file gambar.'
			);
			echo '<script type="text/javascript">';
			echo 'parent.errorPorses('.json_encode($data).');';
			echo '</script>';
		}
	}

	function avatar(){
		$file = $_FILES['userfile'];
		$return = array();

		if (!empty($file)) {
			$dirPath = "uploads".DS.date("Y").DS.date("m").DS;

			$ext = pathinfo($file["name"], PATHINFO_EXTENSION);
			$name = url_title(pathinfo($file["name"], PATHINFO_FILENAME),'_',true) ;

			$this->load->library('getid3/getid3');

			$id3 = new getID3();
			$meta = $id3->analyze($file["tmp_name"]);
			if (!empty($meta["mime_type"])) {
				$type = explode("/", $meta["mime_type"]);
				$type = $type[0];

				$tmp = $this->config->item('beta_media');
				$tmp = $tmp['image'];
				if(!empty($tmp) and in_array($meta["mime_type"], $tmp)) {
					$exist = Media_model::where('path',str_replace(DS, "/", $dirPath).$name.'.'.$ext)->get();

					if(!empty($exist)) {
						$counter = 1;
						while (!empty($exist)) {
							$newtitle = $name . '-' . $counter;
							$counter ++;
							$exist = Media_model::where('path', str_replace(DS, "/", $dirPath).$newtitle.'.'.$ext)->get();
						}
					}else {
						$newtitle = $name;
					}

					$filename = $newtitle.".".$ext;

					$dir = 'public'.DS.$dirPath;
					$config['upload_path'] = $dir;
					$config['allowed_types'] = 'gif|jpg|png';
					$config['raw_name'] = 'dadang';
					$config['file_name'] = $filename;
					//dd($dirPath);

					if(!is_dir('public'.DS.$dirPath))
						mkdir( 'public'.DS.$dirPath,0777,true);

					$this->load->library('upload', $config);

					if ($this->upload->do_upload()) {

						$data['upload_data'] = (array) $this->upload->data();
						$path = $data['upload_data']['full_path'];
						$thumbnails = $this->config->item('beta_config');
						$thumbnails = $thumbnails['thumbnails'];
						$this->load->library('images');

						foreach ($thumbnails as $key=>$thumb){
							if ($thumb['crop']) {
								$this->images->resize($path,
										$dir.$newtitle."_".$key.".".$ext, $thumb['width'], $thumb['height']);
							}else {
								if (!$thumb['width']) {
									$this->images->resize_h($path,
											$dir.$newtitle."_".$key.".".$ext, $thumb['height']);
								}

								if (!$thumb['height']) {
									$this->images->resize_w($path,
											$dir.$newtitle."_".$key.".".$ext, $thumb['width']);
								}
							}
						}

						$id = Media_model::create(array(
								'title'			=> $newtitle,
								'description' 	=> '' ,
								'media_type' 	=> $type,
								'media_mime'	=> $meta['mime_type'],
								'media_meta'	=> serialize($meta),
								'path'			=> str_replace(DS, "/", $dirPath).$filename,
								'author' 		=> $this->data->user_id ,
								'modified_by' 	=> $this->data->user_id ,
						));
						$return = array(
									"name" => $filename,
									"id"	=> $id->id,
									"status"=> 'OK'
							);

						$exist = Usermeta_model::where('user_id',$this->session->userdata('user_id'))->where('meta_key', 'avatar')->first();
						
						if (empty($exist)) {
							$usermetas = array(
											'user_id'			=> $this->session->userdata('user_id'),
											'meta_name'			=> 'Avatar',
											'meta_key'			=> 'avatar',
											'created_at'		=> date('Y-m-d H:i:s', time()),
											'updated_at'		=> date('Y-m-d H:i:s', time()),
											'meta_value_text'	=> $id->id,
									);
							Usermeta_model::create($usermetas);
						}else {
							$exist->meta_value_text = $id->id;

							$exist->updated_at = date('Y-m-d H:i:s', time());
							$exist->save();
						}

						
						$this->session->set_userdata(array(
							'avatar' => 'public/'.str_replace(DS, "/", $dirPath).$filename
						));
						
						echo json_encode($return);
					}else {
						$return = array(
							'message' => 'upload problem',
							"status"=> 'not OK'
						);
						echo json_encode($return);
					}
				}else {
					$return = array(
							'message' => 'File yang di terima gif|jpg|png',
							"status"=> 'not OK'
					);
					echo json_encode($return);
				}
			}else {
				$return = array(
						'status' => 'not OK',
						'message' => 'Silahkan pilih file gambar.'
				);
				echo json_encode($return);
			}
		}else {
			$return = array(
					'status' => 'not OK',
					'message' => 'Silahkan pilih file gambar.'
			);
			echo json_encode($return);
		}
		exit;
	}

	public function metadata_upload(){
		$file = $_FILES['userfile'];
		$return = array();
		if (!empty($file)) {
			$dirPath = "uploads".DS.date("Y").DS.date("m").DS;

			$ext = pathinfo($file["name"], PATHINFO_EXTENSION);
			$name = url_title(pathinfo($file["name"], PATHINFO_FILENAME),'_',true);

			//$this->load->library('getid3/getid3');

			//$id3 = new getID3();
			//$meta = $id3->analyze($file["tmp_name"]);
			//mpr($meta);
			//if (!empty($meta["mime_type"])) {
			if (true) {
				/*
				if (!empty($meta["mime_type"])) {
					$type = explode("/", $meta["mime_type"]);
					$type = $type[0];
				}
				*/
				$exist = Media_model::where('path',str_replace(DS, "/", $dirPath).$name.'.'.$ext)->get();

				if(!empty($exist)) {
					$counter = 1;
					while (!empty($exist)) {
						$newtitle = $name . '-' . $counter;
						$counter ++;
						$exist = Media_model::where('path', str_replace(DS, "/", $dirPath).$newtitle.'.'.$ext)->get();
					}
				}else {
					$newtitle = $name;
				}

				$filename = $newtitle.".".$ext;

				$dir = 'public'.DS.$dirPath;
				$config['upload_path'] = $dir;
				$config['allowed_types'] = 'gif|jpg|png|pdf|xls|doc|docx|xlsx';
				$config['raw_name'] = 'dadang';
				$config['file_name'] = $filename;
				//dd($dirPath);

				if(!is_dir('public'.DS.$dirPath))
					mkdir( 'public'.DS.$dirPath,0777,true);

				$this->load->library('upload', $config);

				if ($this->upload->do_upload()) {
					$id = Media_model::create(array(
							'title'			=> $newtitle,
							'description' 	=> '' ,
							'media_type' 	=> '',
							'media_mime'	=> '',
							'media_meta'	=> '',
							'path'			=> str_replace(DS, "/", $dirPath).$filename,
							'author' 		=> $this->data->user_id ,
							'modified_by' 	=> $this->data->user_id ,
					));
					$return = array(
									"name" => $filename,
									"id"	=> $id->id,
									"status"=> 'OK'
							);
					echo json_encode($return);
				}else {
					$return = array(
							'message' => 'upload problem',
							"status"=> 'not OK',
							'error' => $this->upload->display_errors()
					);
					echo json_encode($return);
				}
			}else {
				$return = array(
							'message' => 'file not chossen',
							"status"=> 'not OK'
					);
				echo json_encode($return);
			}
		}else{
			$return = array(
							'message' => 'super wrong',
							"status"=> 'not OK'
					);
			echo json_encode($return);
		}
		exit;
	}

	public function delete_avatar($id=0){
		$id = $this->input->post('id');
		$media = Media_model::find($id);
		if ($media){
			//dd($media->path);

			$image = explode('.',$media->path);
			$ext = $image[count($image)-1];

			$tmpImage = array();
			for ($i = 0; $i <= count($image)-2; $i++) {
				array_push($tmpImage, $image[$i]);
			}

			$image = implode('.',$tmpImage);
			$thumbnails = $this->config->item('beta_config');
			$thumbnails = $thumbnails['thumbnails'];

			foreach($thumbnails as $key => $thumb)
			{
				if(file_exists('public/'.$image.'_'.$key.'.'.$ext))
					unlink('public/'.$image.'_'.$key.'.'.$ext);
			}

			if(file_exists('public/'.$media->path))
				unlink('public/'.$media->path);

			$entry_media = Entry_media_model::where('media_id',$id)->delete();
			$media->delete();

			$this->session->set_userdata(array(
				'avatar' => '0'
			));
		}
		$return = array(
				'status' => 'OK',
				'message'=> 'seem fine'
		);
		echo json_encode($return);
	}

	public function delete_image($id=0){
		$id = $this->input->post('id');
		$media = Media_model::find($id);
		if ($media){
			//dd($media->path);

			$image = explode('.',$media->path);
			$ext = $image[count($image)-1];

			$tmpImage = array();
			for ($i = 0; $i <= count($image)-2; $i++) {
				array_push($tmpImage, $image[$i]);
			}

			$image = implode('.',$tmpImage);
			$thumbnails = $this->config->item('beta_config');
			$thumbnails = $thumbnails['thumbnails'];

			foreach($thumbnails as $key => $thumb)
			{
				if(file_exists('public/'.$image.'_'.$key.'.'.$ext))
					unlink('public/'.$image.'_'.$key.'.'.$ext);
			}

			if(file_exists('public/'.$media->path))
				unlink('public/'.$media->path);

			$entry_media = Entry_media_model::where('media_id',$id)->delete();
			$media->delete();
		}
		$return = array(
				'status' => 'OK',
				'message'=> 'seem fine'
		);
		echo json_encode($return);
	}

	public function delete_file($id=0){
		$id = $this->input->post('media_id');
		$media = Media_model::find($id);
		if (!empty($media)){
			if(file_exists('public/'.$media->path))
				unlink('public/'.$media->path);

			$media->delete();

			$return = array(
					'status' => 'OK',
					'message'=> 'seem fine'
			);
			echo json_encode($return);
		}else{
			$return = array(
					'status' => 'no OK',
					'message'=> 'seem bad'
			);
			echo json_encode($return);
		}
		exit();
	}

}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
