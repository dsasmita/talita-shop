<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Order extends MY_Controller_admin {

	public function __construct() {
		parent::__construct();
	}

	function index() {
		$offset = $this->uri->segment(3);
		if (! is_numeric($offset)) {
			$offset = 0;
		}
		$limit = 10;
		$segment = 3;

		$data['nav'] = 'ecomm-order';
		$data['nav_sub'] = 'ecomm-order-index';
		$data['orders'] = $this->db->query("SELECT o.created_at,o.`id`,o.`total_amount` total,o.status, u.`name`, a.`city`,a.province 
												FROM beta_ecom_orders o
													LEFT JOIN beta_users u ON u.`id` = o.`customer_id`
													LEFT JOIN beta_ecom_addresses a ON a.`id` = o.`shipping_address_id`
												ORDER BY o.id DESC
												LIMIT ".($offset*$limit).",".$limit);

		$jumlah = $this->db->query("SELECT COUNT(*) count
					FROM beta_ecom_orders o
						LEFT JOIN beta_users u ON u.`id` = o.`customer_id`
						LEFT JOIN beta_ecom_addresses a ON a.`id` = o.`shipping_address_id`");
		$jumlah = $jumlah->first_row();

		$link = getSetting('admin_path').'/orders';

		$data['pagination'] = $this->fungsi->halaman_admin($link, $jumlah->count,
				$limit, $segment);

		$data['title'] = 'List Order | '.$this->superconfig->getSetting('site_name');

		//js
		$data['script'] = 'admin/order/script.index';

		$view = 'order/index';
		$this->_view($view, $data);
	}

	public function detail(){
		$id = $this->uri->segment(4);

		$data['nav'] = 'ecomm-order';
		$data['nav_sub'] = 'ecomm-order-index';
		$data['order'] = Ecomm_order_model::find($id);
		if(empty($data['order']))
			redirect($this->base_admin.'/order');
		$data['customer'] = User_model::find($data['order']->customer_id);
		$data['order_details'] = $this->db->query("SELECT p.name,d.id,d.`qty`,d.`unit_price`,d.`discount`,p.`kode` 
														FROM beta_ecom_order_detail d
														LEFT JOIN beta_ecom_product p ON p.`id` = d.`product_id`
													WHERE d.`order_id` = '".$id."'");
		$data['address'] = Ecomm_addresses_model::find($data['order']->shipping_address_id);
		$data['title'] = 'Detail Order | '.$this->superconfig->getSetting('site_name');

		$data['script'] = 'admin/order/script.detail';
		
		$view = 'order/detail';
		$this->_view($view, $data);
	}

	public function status_order(){
		if(!$this->input->is_ajax_request()){
			echo 'failed';
			exit();
		}
		$input = $this->input->post();

		$order = Ecomm_order_model::find($input['order_id']);

		$order->status = empty($input['status'])?'':$input['status'];
		$order->carrier = empty($input['kurir'])?'':$input['kurir'];
		$order->tracking_number = empty($input['resi'])?'':$input['resi'];
		$order->updated_at = date('Y-m-d H:i:s', time());

		$order->save();

		echo json_encode(array('status'=>'OK'));
	}

	public function detail_edit(){
		if(!$this->input->is_ajax_request()){
			echo 'failed';
			exit();
		}

		$input = $this->input->post();

		$order = Ecomm_order_model::find($input['order_id']);

		$order->status = empty($input['status'])?'':$input['status'];
		$order->updated_at = date('Y-m-d H:i:s', time());

		$order->save();

		$address = Ecomm_addresses_model::find($order->shipping_address_id);

		if(! empty($address)){
			$address->penerima = $input['penerima'];
			$address->phone = $input['phone'];
			$address->address = $input['alamat'];
			$address->province = $input['province'];
			$address->city = $input['city'];
			$address->postal_code = $input['postal_code'];
			$address->updated_at = date('Y-m-d H:i:s', time());
			$address->save();
		}else{
			$address = Ecomm_addresses_model::create(array(
						'user_id'				=> $order->customer_id,
						'penerima'				=> $input['penerima'],
						'phone' 				=> $input['phone'],
						'address'				=> $input['alamat'],
						'postal_code'			=> $input['postal_code'],
						'city'					=> $input['city'],
						'province'				=> $input['province'],
						'created_at'			=> date('Y-m-d H:i:s', time()),
						'updated_at'			=> date('Y-m-d H:i:s', time())
				));
		}

		echo json_encode(array('status'=>'OK'));
	}

	public function orders_confirmation(){
		$offset = $this->uri->segment(4);
		if (! is_numeric($offset)) {
			$offset = 0;
		}
		$limit = 10;
		$segment = 4;

		$data['nav'] = 'ecomm-order';
		$data['nav_sub'] = 'ecomm-order-conf';
		$data['orders'] = Ecomm_order_confirmation_model::limit($limit, $offset)->order_by('id','DESC')->get();

		$data['title'] = 'List Order Confirmation | '.$this->superconfig->getSetting('site_name');

		$link = getSetting('admin_path').'/order/orders-confirmation';

		$data['pagination'] = $this->fungsi->halaman_admin($link, Ecomm_order_confirmation_model::count(),
				$limit, $segment);

		//js
		//$data['plugin']['js'][] = 'plugins/dataTables/jquery.dataTables.js';
		//$data['plugin']['js'][] = 'plugins/dataTables/dataTables.bootstrap.js';
		$data['script'] = 'admin/order/script.order-confirmation';

		$view = 'order/order-confirmation';
		$this->_view($view, $data);
	}

	public function orders_confirmation_detail(){
		$id = $this->uri->segment(4);

		

		$data['nav'] = 'ecomm-order';
		$data['nav_sub'] = 'ecomm-order-conf';
		$data['order_conf'] = $order_conf = Ecomm_order_confirmation_model::find($id);
		if(empty($data['order_conf']))
			redirect($this->base_admin.'/order/orders-confirmation');

		$post = $this->input->post();

		if($post){
			$order_conf->status = $post['status'];
			$order_conf->save();
			$this->session->set_flashdata('info', 'success');
			redirect($this->base_admin.'/order/orders-confirmation');
		}

		$data['title'] = 'Detail Order Confirmation | '.$this->superconfig->getSetting('site_name');

		//js
		//$data['script'] = 'admin/order/script.order-confirmation';

		$view = 'order/order-confirmation-detail';
		$this->_view($view, $data);
	}

}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
