<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Entry extends MY_Controller_admin {

	public function __construct() {
		parent::__construct();
		$this->type = $this->site->getEntrySetting($this->uri->segment(3));

		if (empty($this->type))
			redirect($this->base_admin.'/dashboard');
		$this->type_entry = $this->uri->segment(3);
	}

	function index() {
		$offset = $this->uri->segment(4);
		if (! is_numeric($offset)) {
			$offset = 0;
		}
		$limit = 10;
		$segment = 4;

		$data['nav'] = 'entries';
		$data['nav_sub'] = $this->type_entry;
		$data['entries'] = Entry_model::where('entry_type',$this->type_entry)->where('status !=','deleted')->limit($limit, $offset)->order_by('updated_at','DESC')->get();

		$data['title'] = 'List '.$this->type['label']['plural'].' | '.$this->superconfig->getSetting('site_name');

		$data['type'] = $this->type;
		$data['type_entry'] = $this->type_entry;

		$link = getSetting('admin_path').'/entry/'.$this->type_entry;

		$this->load->helper('text');

		$data['pagination'] = $this->fungsi->halaman_admin($link, Entry_model::where('entry_type',$this->type_entry)->where('status !=','deleted')->count(),
				$limit, $segment);

		$view = $this->type_entry.'/index';
		$tmp_view = file_exists(FCPATH."/apps/views/admin/$view.blade.php");
		if (!$tmp_view)
			$view = str_replace($this->type_entry, 'entry', $view);

		$this->_view($view, $data);
	}

	function create() {
		if ($this->input->post()) {

			$input = $this->input->post();
			$this->load->library('form_validation');

			//check validation if required
			$config = array(
				array(
					'field'   => 'entry.title',
					'label'   => 'Entry',
					'rules'   => 'required|trim'
				),
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == false && trim($input['entry']['title']) != ''):
				$tempslug = url_title($input['entry']['title'],'-',true);
				$oldtitle = $tempslug;
				$newtitle = $tempslug;
				$curtitle = Entry_model::where('slug',$newtitle)->get();
				$counter = 1;

				while (!empty($curtitle)) {
					$newtitle = $oldtitle . '-' . $counter;
					$counter++;
					$curtitle = Entry_model::where('slug',$newtitle)->get();
				}

				// Set parent var
				if(empty($input['entry']['entry_parent']))
					$parent = 0;
				else
					$parent = $input['entry']['entry_parent'];

				// Set title
				$title = null;
				if(!empty($input['entry']['title'])) {
					$title = $input['entry']['title'];
				}

				// Set Content
				$content = null;
				if(!empty($input['entry']['content'])) {
					$content = $input['entry']['content'];
				}
				// Set excerpt
				$excerpt = null;
				if(!empty($input['entry']['excerpt'])) {
					$excerpt = $input['entry']['excerpt'];
				}

				// Set status
				$status = 'published';
				if(!empty($input['entry']['status'])) {
					$status = $input['entry']['status'];
				}

				/*
				 * Saving Entry data
				*/
				$entry = Entry_model::create(array(
						'title'						=> $title,
						'content'					=> $content,
						'excerpt' 					=> $excerpt,
						'author'					=> $this->data->user_id,
						'status'					=> $status,
						'slug'						=> $newtitle,
						'entry_parent'				=> $parent,
						'entry_type'				=> $this->type_entry,
						'modified_by'				=> $this->data->user_id,
						'created_at'				=> date('Y-m-d H:i:s', time()),
						'updated_at'				=> date('Y-m-d H:i:s', time())
				));

				// set meta
				foreach($this->type['meta'] as $key => $value) {
					$entrymeta = $input['entrymeta'][$key];
					if($value['type']=='checkbox' || !is_array($entrymeta) || isset($entrymeta[$this->lang->lang()])) {
						if($value['type']=='checkbox') {
							if(empty($entrymeta)) {
								$entrymeta = $value['value'];
							}
							else
								$entrymeta = implode(',',$entrymeta);
						}

						if(isset($value['multilanguage']) && $value['multilanguage'] == true) {
							$temp = null;
							foreach ($entrymeta as $key2 => $value2 ) {
								$temp = $temp."[:$key2]".$value2;
							}
							$entrymeta = $temp;
						}

						if($value['data_type']=='datetime'){
							$meta = array(
									array(
											'entry_id'					=> $entry->id,
											'meta_name'					=> $value['label'],
											'meta_key'					=> $key,
											'created_at'				=> date('Y-m-d H:i:s', time()),
											'updated_at'				=> date('Y-m-d H:i:s', time()),
											'meta_value_'.$value['data_type']	=> (!empty($entrymeta) ? date('Y-m-d H:i:s',strtotime($entrymeta)) : (!empty($value["value"]) ? $value["value"] : null)),
									));
						}else {
							$meta = array(
									array(
											'entry_id'					=> $entry->id,
											'meta_name'					=> $value['label'],
											'meta_key'					=> $key,
											'created_at'				=> date('Y-m-d H:i:s', time()),
											'updated_at'				=> date('Y-m-d H:i:s', time()),
											'meta_value_'.$value['data_type']	=> (!empty($entrymeta) ? $entrymeta : $value['value']),
									));
						}

						$entry->details()->create($meta[0]);
					}else {
						foreach($entrymeta as $v) {
							if(empty($v))
								break;

							if(isset($value['multilanguage']) && $value['multilanguage'] == true) {
								$temp = null;
								foreach ($v as $key2 => $value2 ) {
									$temp = $temp."[:$key2]".$value2;
								}
								$v = $temp;
							}

							if($value['data_type']=='datetime'){
								$meta = array(
										array(
												'entry_id'					=> $entry->id,
												'meta_name'					=> $value['label'],
												'meta_key'					=> $key,
												'created_at'				=> date('Y-m-d H:i:s', time()),
												'updated_at'				=> date('Y-m-d H:i:s', time()),
												'meta_value_'.$value['data_type']	=> (!empty($v) ? date('Y-m-d H:i:s', strtotime($v)) : (!empty($value["value"]) ? $value["value"] : null)),
										));
							}else {
								$meta = array(
										array(
												'entry_id'					=> $entry->id,
												'meta_name'					=> $value['label'],
												'meta_key'					=> $key,
												'created_at'				=> date('Y-m-d H:i:s', time()),
												'updated_at'				=> date('Y-m-d H:i:s', time()),
												'meta_value_'.$value['data_type']	=> (!empty($v) ? $v : $value['value']),
										));
							}

							$entry->details()->create($meta[0]);
						}
					}
				}

				if (!empty($input['taxonomy'])){
					foreach ($input['taxonomy'] as $t){
						$data_et= array(
								'entry_id'=>$entry->id,
								'taxonomy_id'=>$t,
								'created_at'=>date('Y-m-d H:i:s', time()),
								'updated_at'=>date('Y-m-d H:i:s', time()));
						Entry_taxonomy_model::create($data_et);
					}
				}

				if (!empty($input['media'])){
					foreach ($input['media'] as $m){
						$media = Entry_media_model::create(
									array(
											'entry_id'=> $entry->id,
											'media_id'=>$m,
											'created_at'=>date('Y-m-d H:i:s', time()),
											'updated_at'=>date('Y-m-d H:i:s', time())
									)
							);
					}
				}

				$this->session->set_flashdata('info', 'success');
				redirect($this->base_admin.'/entry/'.$this->type_entry);
			else:
				$this->session->set_flashdata('info', 'error');
				redirect($this->base_admin.'/entry/'.$this->type_entry.'/add');
			endif;
		}else{
			$data['nav'] = 'entries';
			$data['nav_sub'] = $this->type_entry;
			//$data['entries'] = Entry_model::where('entry_type',$this->type_entry)->where('status !=','deleted')->get();
			$data['title'] = 'Create '.$this->type['label']['plural'].' | '.$this->superconfig->getSetting('site_name');

			$data['type'] = $this->type;
			$data['type_entry'] = $this->type_entry;

			$taxonomy = $this->site->getTaxonomy();

			foreach($taxonomy as $key => $val) {
				$taxonomy_key[] = $key;
			}

			$diff =  $this->type['taxonomy'];

			$tax = array();

			$taxonomy_available = array_intersect($taxonomy_key, $diff);

			foreach($taxonomy_available as $value) {
				/*
				if($taxonomy[$value]['hierarchical']) {
					$tax[$value] = Taxonomy_model::where('parent',0)->where('taxonomy_type',$value)->where('status','active')->get();
				}else{
					$tax[$value] = Taxonomy_model::where('taxonomy_type',$value)->where('status','active')->get();
				}
				*/
				$tax[$value] = Taxonomy_model::where('taxonomy_type',$value)->where('status','active')->get();
			}

			$data['taxonomy'] = $tax;

			$view = $this->type_entry.'/create';
			$tmp_view = file_exists(FCPATH."/apps/views/admin/$view.blade.php");
			if (!$tmp_view)
				$view = str_replace($this->type_entry, 'entry', $view);

			//js
			$data['plugin']['js'][] = 'plugins/ckeditor/ckeditor.js';
			$data['plugin']['js'][] = 'plugins/elastic/jquery.elastic.source.js';
			$data['plugin']['js'][] = 'plugins/form/jquery.form.js';
			
			$script = 'admin/'.$this->type_entry.'/script.create';

			$tmp_script = file_exists(FCPATH."/apps/views/".$script.".blade.php");
			if (!$tmp_script)
				$script = str_replace($this->type_entry, 'entry', $script);

			$data['script'] = $script;

			$this->_view($view, $data);
		}
	}

	function edit(){
		$id = $this->uri->segment(5);
		$entry = Entry_model::find($id);
		$data['entry'] = $entry;
		if(!empty($data['entry'])){
			if ($this->input->post()) {
				$input = $this->input->post();
				// Set parent var
				if(empty($input['entry']['entry_parent']))
					$parent = 0;
				else
					$parent = $input['entry']['entry_parent'];

				// Set title
				$title = null;
				if(!empty($input['entry']['title'])) {
					$title = $input['entry']['title'];
				}

				// Set Content
				$content = null;
				if(!empty($input['entry']['content'])) {
					$content = $input['entry']['content'];
				}

				// Set excerpt
				$excerpt = null;
				if(!empty($input['entry']['excerpt'])) {
					$excerpt = $input['entry']['excerpt'];
				}

				// Set status
				$status = 'published';
				if(!empty($input['entry']['status'])) {
					$status = $input['entry']['status'];
				}

				$entry->title = $title;
				$entry->content = $content;
				$entry->entry_parent = $parent;
				$entry->excerpt = $excerpt;
				$entry->status = $status;
				$entry->updated_at = date('Y-m-d H:i:s', time());
				$entry->modified_by = $this->data->user_id;
				$entry->save();

				foreach($this->type['meta'] as $key => $value) {
					$entrymeta = $input['entrymeta'][$key];
					if($value['type']=='checkbox' || !is_array($entrymeta) || isset($entrymeta[$this->lang->lang()])) {
						if($value['type']=='checkbox') {
							if(empty($entrymeta)) {
								$entrymeta = $value['value'];
							}
							else
								$entrymeta = implode(',',$entrymeta);
						}

						if(isset($value['multilanguage']) && $value['multilanguage'] == true) {
							$temp = null;
							foreach ($entrymeta as $key2 => $value2 ) {
								$temp = $temp."[:$key2]".$value2;
							}
							$entrymeta = $temp;
						}

						if($value['data_type']=='datetime'){
							$meta = array(
									array(
											'entry_id'					=> $entry->id,
											'meta_name'					=> $value['label'],
											'meta_key'					=> $key,
											'created_at'				=> date('Y-m-d H:i:s', time()),
											'updated_at'				=> date('Y-m-d H:i:s', time()),
											'meta_value_'.$value['data_type']	=> (!empty($entrymeta) ? date('Y-m-d H:i:s',strtotime($entrymeta)) : (!empty($value["value"]) ? $value["value"] : null)),
									));
						}else {
							$meta = array(
									array(
											'entry_id'					=> $entry->id,
											'meta_name'					=> $value['label'],
											'meta_key'					=> $key,
											'created_at'				=> date('Y-m-d H:i:s', time()),
											'updated_at'				=> date('Y-m-d H:i:s', time()),
											'meta_value_'.$value['data_type']	=> (!empty($entrymeta) ? $entrymeta : $value['value']),
									));
						}

						$meta_save = Entrymeta_model::where('entry_id',$entry->id)->where('meta_key',$meta[0]['meta_key'])->first();

						if (empty($meta_save)) {
							$entry->details()->create($meta[0]);
						}else{
							$meta_save->meta_name = $meta[0]['meta_name'];
							$meta_save->updated_at = date('Y-m-d H:i:s', time());
							$meta_save->entry_id = $entry->id;
							if($value['data_type'] == 'text')
								$meta_save->meta_value_text = $meta[0]['meta_value_text'];
							else if($value['data_type'] == 'int')
								$meta_save->meta_value_int = $meta[0]['meta_value_int'];
							else if($value['data_type'] == 'datetime')
								$meta_save->meta_value_datetime =   $meta[0]['meta_value_datetime'];
							$meta_save->save();
						}
					}else {
						foreach($entrymeta as $v) {
							if(empty($v))
								break;

							if(isset($value['multilanguage']) && $value['multilanguage'] == true) {
								$temp = null;
								foreach ($v as $key2 => $value2 ) {
									$temp = $temp."[:$key2]".$value2;
								}
								$v = $temp;
							}

							if($value['data_type']=='datetime'){
								$meta = array(
										array(
												'entry_id'					=> $entry->id,
												'meta_name'					=> $value['label'],
												'meta_key'					=> $key,
												'created_at'				=> date('Y-m-d H:i:s', time()),
												'updated_at'				=> date('Y-m-d H:i:s', time()),
												'meta_value_'.$value['data_type']	=> (!empty($v) ? date('Y-m-d H:i:s', strtotime($v)) : (!empty($value["value"]) ? $value["value"] : null)),
										));
							}else {
								$meta = array(
										array(
												'entry_id'					=> $entry->id,
												'meta_name'					=> $value['label'],
												'meta_key'					=> $key,
												'created_at'				=> date('Y-m-d H:i:s', time()),
												'updated_at'				=> date('Y-m-d H:i:s', time()),
												'meta_value_'.$value['data_type']	=> (!empty($v) ? $v : $value['value']),
										));
							}
							$meta_save = $entry->details()->where('meta_key',$meta[0]['meta_key'])->first();
							if (empty($meta_save)) {
								$entry->details()->create($meta[0]);
							}else{
								$meta_save->meta_name = $meta[0]['meta_name'];
								$meta_save->entry_id = $entry->id;
								$meta_save->updated_at = date('Y-m-d H:i:s', time());
								if($value['data_type'] == 'text')
									$meta_save->meta_value_text = $meta[0]['meta_value_text'];
								else if($value['data_type'] == 'int')
									$meta_save->meta_value_int = $meta[0]['meta_value_int'];
								else if($value['data_type'] == 'datetime')
									$meta_save->meta_value_datetime =   $meta[0]['meta_value_datetime'];

								$meta_save->save();
							}
						}
					}
				}
				$entry_taxonomy = Entry_taxonomy_model::where('entry_id',$id)->delete();
				//dd($input['taxonomy']);
				if (!empty($input['taxonomy'])){
					foreach ($input['taxonomy'] as $key=>$t){
						$data_et= array(
								'entry_id'=>$entry->id,
								'taxonomy_id'=>$t,
								'created_at'=>date('Y-m-d H:i:s', time()),
								'updated_at'=>date('Y-m-d H:i:s', time())
						);
						Entry_taxonomy_model::create($data_et);
					}
				}

				if (!empty($input['media'])){
					foreach ($input['media'] as $m){
						$exist = Entry_media_model::where('entry_id',$id)->where('media_id',$m)->get();
						//mpr($exist);
						if (empty($exist)) {
							$media = Entry_media_model::create(
								array(
									'entry_id'=> $id,
									'media_id'=>$m,
									'created_at'=>date('Y-m-d H:i:s', time()),
									'updated_at'=>date('Y-m-d H:i:s', time())
								)
							);
						}
					}
				}

				$this->session->set_flashdata('info', 'success');

				redirect($this->base_admin.'/entry/'.$this->type_entry.'/edit/'.$id);
			}else{
				$metas = array();
				foreach ($entry->details() as $value) {
					$metas[$value->meta_key]['text'] = $value->meta_value_text;
					$metas[$value->meta_key]['int'] = $value->meta_value_int;
				}

				//dd($metas);

				$taxonomy = $this->site->getTaxonomy();

				foreach($taxonomy as $key => $val) {
					$taxonomy_key[] = $key;
				}

				$diff =  $this->type['taxonomy'];

				$tax = array();

				$taxonomy_available = array_intersect($taxonomy_key, $diff);

				foreach($taxonomy_available as $value) {
					$data['taxonomies'] = Entry_taxonomy_model::where('entry_id',$id)->get();
					$tax[$value] = Taxonomy_model::where('taxonomy_type',$value)->where('status','active')->get();
				}



				$data['taxonomy'] = $tax;

				$data['taxonomy_list'] = $entry->getTaxonomyListByEntry($id);

				$data['media'] = $entry->getMediaListByEntry($id);

				$data['nav'] = 'entries';
				$data['nav_sub'] = $this->type_entry;
				$data['title'] = 'Update '.$this->type['label']['singular'].' | '.$this->superconfig->getSetting('site_name');

				$data['type'] = $this->type;
				$data['type_entry'] = $this->type_entry;
				$data['metas'] = $metas;
				$data['entry_id'] = $id;

				$view = $this->type_entry.'/update';
				$tmp_view = file_exists(FCPATH."/apps/views/admin/$view.blade.php");
				if (!$tmp_view)
					$view = str_replace($this->type_entry, 'entry', $view);

				//js
				$data['plugin']['js'][] = 'plugins/ckeditor/ckeditor.js';
				$data['plugin']['js'][] = 'plugins/elastic/jquery.elastic.source.js';
				$data['plugin']['js'][] = 'plugins/form/jquery.form.js';
				
				$script = 'admin/'.$this->type_entry.'/script.update';

				$tmp_script = file_exists(FCPATH."/apps/views/".$script.".blade.php");
				if (!$tmp_script)
					$script = str_replace($this->type_entry, 'entry', $script);

				$data['script'] = $script;

				$this->_view($view, $data);
			}
		}else{
			redirect($this->base_admin.'/entry/'.$this->type_entry);
		}
	}

	public function delete(){
		$id = $this->uri->segment(5);
		$entry = Entry_model::find($id);
		if(! empty($entry)){
			$entry->status = 'deleted';
			$entry->save();
		}

		redirect($this->base_admin.'/entry/'.$this->type_entry);
	}

}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
