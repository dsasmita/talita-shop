<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Taxonomy extends MY_Controller_admin {

	public function __construct() {
		parent::__construct();
		
		$this->type = $this->site->getTaxonomySetting($this->uri->segment(3));
		
		if (empty($this->type))
			redirect($this->base_admin.'/dashboard');
		$this->type_taxo = $this->uri->segment(3);
	}

	function index() {
		$data = array();
		$data['nav'] = 'taxo';
		$data['nav_sub'] = 'taxo_'.$this->type_taxo;
		$data['type'] = $this->type;
		$data['type_taxo'] = $this->type_taxo;
		$data['taxonomies'] = Taxonomy_model::where('taxonomy_type',$this->type_taxo)->where('status','active')->order_by('updated_at','DESC')->get();
		$data['title'] = 'Taxonomy '.$this->type['label']['single'].' | '.$this->superconfig->getSetting('site_name');
		$this->_view('taxonomy/index', $data);
	}
	
	function edit(){
		$id = $this->uri->segment(5);
		$taxonomy = Taxonomy_model::find($id);
		$data = array();
		$data['taxonomy'] = $taxonomy;
		if(empty($taxonomy))
			redirect($this->base_admin.'/taxonomy/'.$this->type_taxo);
		
		if ($this->input->post()) {
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			//check validation if required
			$config = array(
					array(
							'field'   => 'entry.title',
							'label'   => 'Entry',
							'rules'   => 'required'
					),
			);
			
			$this->form_validation->set_rules($config);
			
			if ($this->form_validation->run() == false):
				$tempslug = url_title($input['taxonomy']['name'],'-',true);
				if($tempslug != $taxonomy->taxonomy_slug){
					$oldtitle = $tempslug;
					$newtitle = $tempslug;
					$curtitle = Taxonomy_model::where('taxonomy_slug',$newtitle)->get();
					$counter = 1;
					
					while (!empty($curtitle)) {
						$newtitle = $oldtitle . '-' . $counter;
						$counter++;
						$curtitle = Taxonomy_model::where('taxonomy_slug',$newtitle)->get();
					}
				}

				// Set parent var
				if(empty($input['taxonomy']['parent']))
					$parent = 0;
				else
					$parent = $input['taxonomy']['parent'];
				
				// Set title
				if(!empty($input['taxonomy']['name'])) {
					$name = $input['taxonomy']['name'];
				}else{
					$name = 'no-name';
				}
				
				if(!empty($input['taxonomy']['description'])) {
					$description = $input['taxonomy']['description'];
				}else{
					$description = '';
				}
				
				$taxonomy->name = $name;
				$taxonomy->description = $description;
				$taxonomy->parent = $parent;
				$taxonomy->taxonomy_slug = $newtitle;
				$taxonomy->updated_at = date('Y-m-d H:i:s', time());
				$taxonomy->save();
				
				$this->session->set_flashdata('update_info', 'success');
				
				redirect($this->base_admin.'/taxonomy/'.$this->type_taxo);
				
			else:
				redirect($this->base_admin.'/taxonomy/'.$this->type_taxo);
			endif;
		}else {
			$data['nav'] = 'taxo';
			$data['nav_sub'] = 'taxo_'.$this->type_taxo;
			$data['type'] = $this->type;
			$data['type_taxo'] = $this->type_taxo;
			$data['parents'] = Taxonomy_model::where('taxonomy_type',$this->type_taxo)->where('id !=',$id)->where('status','active')->get();
			$data['title'] = 'Update Taxonomy '.$this->type['label']['single'].' | '.$this->superconfig->getSetting('site_name');
			
			//js
			$data['plugin']['js'][] = 'plugins/elastic/jquery.elastic.source.js';
			$data['script'] = 'admin/taxonomy/script.create';
			
			$this->_view('taxonomy/update', $data);
		}
	}
	
	function create(){
		if ($this->input->post()) {
			$input = $this->input->post();
			$this->load->library('form_validation');
			
			//check validation if required
			$config = array(
					array(
							'field'   => 'taxonomy.title',
							'label'   => 'Name',
							'rules'   => 'required'
					),
			);
			
			$this->form_validation->set_rules($config);
			
			if ($this->form_validation->run() == false):
				$tempslug = url_title($input['taxonomy']['name'],'-',true);
				$oldtitle = $tempslug;
				$newtitle = $tempslug;
				$curtitle = Taxonomy_model::where('taxonomy_slug',$newtitle)->get();
				$counter = 1;
				
				while (!empty($curtitle)) {
					$newtitle = $oldtitle . '-' . $counter;
					$counter++;
					$curtitle = Taxonomy_model::where('taxonomy_slug',$newtitle)->get();
				}
				
				// Set parent var
				if(empty($input['taxonomy']['parent']))
					$parent = 0;
				else
					$parent = $input['taxonomy']['parent'];
				
				// Set title
				if(!empty($input['taxonomy']['name'])) {
					$name = $input['taxonomy']['name'];
				}else{
					$name = 'no-name';
				}
				
				if(!empty($input['taxonomy']['description'])) {
					$description = $input['taxonomy']['description'];
				}else{
					$description = '';
				}
				
				$taxonomy = Taxonomy_model::create(array(
						'name'						=> $name,
						'description'				=> $description,
						'taxonomy_slug' 			=> $newtitle,
						'parent'					=> $parent,
						'taxonomy_type'				=> $this->type_taxo,
						'modified_by'				=> $this->data->user_id,
						'created_at'				=> date('Y-m-d H:i:s', time()),
						'updated_at'				=> date('Y-m-d H:i:s', time())
				));
				$this->session->set_flashdata('add_info', 'success');

				redirect($this->base_admin.'/taxonomy/'.$this->type_taxo);
			else:
				redirect($this->base_admin.'/taxonomy/'.$this->type_taxo);
			endif;
		}else {
			$data = array();
			$data['nav'] = 'taxo';
			$data['nav_sub'] = 'taxo_'.$this->type_taxo;
			$data['type'] = $this->type;
			$data['type_taxo'] = $this->type_taxo;
			$data['parents'] = Taxonomy_model::where('taxonomy_type',$this->type_taxo)->where('status','active')->get();
			$data['title'] = 'Add Taxonomy '.$this->type['label']['single'].' | '.$this->superconfig->getSetting('site_name');
			$data['plugin'] = array();
			
			//js
			$data['plugin']['js'][] = 'plugins/elastic/jquery.elastic.source.js';
			$data['script'] = 'admin/taxonomy/script.create';

			$this->_view('taxonomy/create', $data);
		}
	}

	function delete(){
		$id = $this->uri->segment(5);
		$taxo = Taxonomy_model::find($id);
		if(! empty($taxo)){
			$taxo->status = 'deleted';
			$taxo->save();
		}
		$this->session->set_flashdata('delete_info', 'success');

		redirect($this->base_admin.'/taxonomy/'.$this->type_taxo);
	}

	function create_ajax(){
		if(!$this->input->is_ajax_request()){
			exit();
		}
		$input = $this->input->post();

		$tempslug = url_title($input['name'],'-',true);
		$oldtitle = $tempslug;
		$newtitle = $tempslug;
		$curtitle = Taxonomy_model::where('taxonomy_slug',$newtitle)->get();
		$counter = 1;
		
		while (!empty($curtitle)) {
			$newtitle = $oldtitle . '-' . $counter;
			$counter++;
			$curtitle = Taxonomy_model::where('taxonomy_slug',$newtitle)->get();
		}
		
		// Set title
		if(!empty($input['name'])) {
			$name = $input['name'];
		}else{
			$name = 'no-name';
		}
		
		if(!empty($input['description'])) {
			$description = $input['description'];
		}else{
			$description = '';
		}
		
		$taxonomy = Taxonomy_model::create(array(
				'name'						=> $name,
				'description'				=> $description,
				'taxonomy_slug' 			=> $newtitle,
				'parent'					=> 0,
				'taxonomy_type'				=> $this->type_taxo,
				'modified_by'				=> $this->data->user_id,
				'created_at'				=> date('Y-m-d H:i:s', time()),
				'updated_at'				=> date('Y-m-d H:i:s', time())
		));

		echo json_encode(array('operation'=>'add','status'=>'OK','id'=>$taxonomy->id,'name'=>$name,'type'=>$taxonomy->taxonomy_type,'desc'=>$taxonomy->description));
		exit();
	}

	public function edit_ajax(){
		if(!$this->input->is_ajax_request()){
			echo 'failed';
			exit();
		}
		$input = $this->input->post();

		$taxonomy = Taxonomy_model::find($input['id']);

		$tempslug = url_title($input['name'],'-',true);
		$oldtitle = $tempslug;
		$newtitle = $tempslug;
		$curtitle = Taxonomy_model::where('taxonomy_slug',$newtitle)->get();
		$counter = 1;
		
		while (!empty($curtitle)) {
			$newtitle = $oldtitle . '-' . $counter;
			$counter++;
			$curtitle = Taxonomy_model::where('taxonomy_slug',$newtitle)->get();
		}

		if(!empty($input['name'])) {
			$name = $input['name'];
		}else{
			$name = 'no-name';
		}
		
		if(!empty($input['description'])) {
			$description = $input['description'];
		}else{
			$description = '';
		}
		
		$taxonomy->name = $name;
		$taxonomy->description = $description;
		$taxonomy->taxonomy_slug = $newtitle;
		$taxonomy->updated_at = date('Y-m-d H:i:s', time());
		$taxonomy->save();

		echo json_encode(array('operation'=>'update','status'=>'OK','id'=>$taxonomy->id,'name'=>$taxonomy->name,'type'=>$taxonomy->taxonomy_type,'desc'=>$taxonomy->description));
		exit();
	}

}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
