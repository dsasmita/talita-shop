<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Account extends MY_Controller_admin {

	public function __construct() {
		parent::__construct();
	}

	function index() {
		$offset = $this->uri->segment(3);
		if (! is_numeric($offset)) {
			$offset = 0;
		}
		$limit = 10;
		$segment = 3;

		$data['nav'] = 'account';
		$data['nav_sub'] = 'account_list';
		$data['accounts'] = User_model::where('type !=','Super Administrator')->where('status !=','deleted')->limit($limit, $offset)->order_by('last_login','desc')->get();
		$data['title'] = 'List Account | '.$this->superconfig->getSetting('site_name');

		$link = getSetting('admin_path').'/account/';

		$data['pagination'] = $this->fungsi->halaman_admin($link, User_model::where('type !=','Super Administrator')->where('status !=','deleted')->order_by('last_login','desc')->count(),
				$limit, $segment);

		$view = 'account/index';
		$this->_view($view, $data);
	}

	function create() {
		if ($this->input->post()) {
			$input = $this->input->post();

			if($input['role'] != '0' && strlen($input['username'])>5 && form_val_valid_email($input['email']) && form_val_alpha_dash($input['username'])){
				if (trim($input['password']) != '') {
					if ($input['password'] == $input['password_reff']) {
						$tmp = array(
								'email'					=> $input['email'],
								'username'				=> $input['username'],
								'type'					=> $input['role'],
								'status'				=> 'active',
								'created_at'			=> date('Y-m-d H:i:s', time()),
								'updated_at'			=> date('Y-m-d H:i:s', time()),
								'last_login'			=> date('Y-m-d H:i:s', time()),
								'password'				=> md5($input['password'])
						);
						$user = User_model::create($tmp);

						$metas_setting = $this->config->item('beta_config');
						$metas_setting = $metas_setting['users']['default'];

						foreach ($metas_setting as $key=>$value){
							$usermetas = $input['usermetas'][$key];

							$usermetas = array(
									'user_id'					=> $user->id,
									'meta_name'					=> $value['label'],
									'meta_key'					=> $key,
									'created_at'				=> date('Y-m-d H:i:s', time()),
									'updated_at'				=> date('Y-m-d H:i:s', time()),
									'meta_value_'.$value['type']	=> (!empty($usermetas) ? $usermetas : $value['value']),
							);
							$user->details()->create($usermetas);
						}

						redirect(getSetting('admin_path').'/account/');
					}else {
						$this->session->set_flashdata('info', 'error');
						redirect(getSetting('admin_path').'/account/add/');
					}
				}else {
					$tmp = array(
							'email'					=> $input['email'],
							'username'				=> $input['username'],
							'type'					=> $input['role'],
							'status'				=> 'active',
							'created_at'			=> date('Y-m-d H:i:s', time()),
							'updated_at'			=> date('Y-m-d H:i:s', time()),
							'last_login'			=> date('Y-m-d H:i:s', time())
					);
					$user = User_model::create($tmp);

					$metas_setting = $this->config->item('beta_config');
					$metas_setting = $metas_setting['users']['default'];

					foreach ($metas_setting as $key=>$value){
						$usermetas = $input['usermetas'][$key];

						$usermetas = array(
								'user_id'					=> $user->id,
								'meta_name'					=> $value['label'],
								'meta_key'					=> $key,
								'created_at'				=> date('Y-m-d H:i:s', time()),
								'updated_at'				=> date('Y-m-d H:i:s', time()),
								'meta_value_'.$value['type']	=> (!empty($usermetas) ? $usermetas : $value['value']),
						);
						$user->details()->create($usermetas);
					}
					redirect(getSetting('admin_path').'/account/');
				}
			}else {
				$this->session->set_flashdata('info', 'error');
				redirect(getSetting('admin_path').'/account/add/');
			}
		}else {
			$data['nav'] = 'account';
			$data['nav_sub'] = 'account_add';
			$data['title'] = 'Add Account | '.$this->superconfig->getSetting('site_name');

			$tmp = $this->config->item('beta_config');

			$data['setting'] = $tmp['users']['default'];

			$data['roles'] = Role_model::where('id !=',1)->get();

			$view = 'account/create';
			$this->_view($view, $data);
		}
	}

	function edit(){
		$id = $this->uri->segment(4);
		$user = User_model::find($id);
		$data['account'] = $user;

		if (!empty($user)) {
			if ($this->input->post()) {
				$input = $this->input->post();

				if($input['role'] != '0' && strlen($input['username'])>5 && form_val_valid_email($input['email']) && form_val_alpha_dash($input['username'])){

					$user->username = $input['username'];
					$user->email = $input['email'];
					$user->type = $input['role'];
					$user->save();

					$metas_setting = $this->config->item('beta_config');
					$metas_setting = $metas_setting['users']['default'];

					foreach ($metas_setting as $key=>$value){
						$usermetas = $input['usermetas'][$key];

						$exist = Usermeta_model::where('user_id',$id)->where('meta_key', $key)->first();

						if (empty($exist)) {
							$usermetas = array(
											'user_id'					=> $id,
											'meta_name'					=> $value['label'],
											'meta_key'					=> $key,
											'created_at'				=> date('Y-m-d H:i:s', time()),
											'updated_at'				=> date('Y-m-d H:i:s', time()),
											'meta_value_'.$value['type']	=> (!empty($usermetas) ? $usermetas : $value['value']),
									);
							$user->details()->create($usermetas);
						}else {
							if ($value['type'] == 'text') {
								$exist->meta_value_text = $usermetas;
							}elseif ($value['type'] == 'int'){
								$exist->meta_value_int = $usermetas;
							}
							$exist->updated_at = date('Y-m-d H:i:s', time());
							$exist->save();
						}
					}
					//exit();
					$this->session->set_flashdata('info', '1');
				}else {
					$this->session->set_flashdata('info', 'error');
				}
				redirect(getSetting('admin_path').'/account/edit/'.$id);
			}else {
				$data['nav'] = 'account';
				$data['nav_sub'] = 'account_list';
				$data['title'] = 'Update Account | '.$this->superconfig->getSetting('site_name');

				$usermetas = array();
				foreach ($user->details() as $value) {
					$usermetas[$value->meta_key] = $value->meta_value_text;
				}
				$data['usermetas'] = $usermetas;

				$tmp = $this->config->item('beta_config');

				$data['setting'] = $tmp['users']['default'];

				$data['roles'] = Role_model::where('id !=',1)->get();

				$view = 'account/update';
				$this->_view($view, $data);
			}
		}else{
			redirect($this->base_admin.'/account/');
		}
	}

public function delete(){
		$id = $this->uri->segment(4);
		$user = User_model::find($id);
		if(! empty($user)){
			$user->status = 'deleted';
			$user->save();
		}

		redirect($this->base_admin.'/account');
	}
}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
