<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Setting extends MY_Controller_admin {

	public function __construct() {
		parent::__construct();
		$this->data = array();
		$this->data['nav'] = 'setting';
	}

	function index() {
		$this->data['nav_sub'] = 'setting_general';
		$this->data['title'] = 'Setting | '.$this->superconfig->getSetting('site_name');
		$this->_view('setting/general', $this->data);
	}
	
	function extras() {
		$this->data['nav_sub'] = 'setting_extras';
		$this->data['title'] = 'Setting | '.$this->superconfig->getSetting('site_name');
		$this->_view('setting/extras', $this->data);
	}
	
	function save(){
		$data = $this->input->post();
		if ($this->input->post()) :
			foreach ( $data as $key => $value ) :
				updateSiteSetting( $key, $value );
			endforeach;
		endif;
		$this->session->set_flashdata('info', '1');
		if ($data['from'] == 'extras')
			redirect(getSetting('admin_path').'/setting/extras');
		else
			redirect(getSetting('admin_path').'/setting');
	}
	
	function profile(){
		$id = $this->session->userdata('user_id');
		$user = User_model::find($id);
		$data['account'] = $user;
		
		if (!empty($user)) {
			if ($this->input->post()) {
				$input = $this->input->post();
				
				if(strlen($input['username'])>5 && form_val_valid_email($input['email']) && form_val_alpha_dash($input['username'])){
					
					$user->username = $input['username'];
					$user->email = $input['email'];
					
					
					if (trim($input['password']) != '') {
						if ($input['password'] == $input['password_reff']) {
							$user->password = md5($input['password']);
						}
					}
					$user->updated_at =  date('Y-m-d H:i:s', time());
					$user->save();
					
					$metas_setting = $this->config->item('beta_config');
					$metas_setting = $metas_setting['users']['default'];
					
					foreach ($metas_setting as $key=>$value){
						$usermetas = $input['usermetas'][$key];
						
						$exist = Usermeta_model::where('user_id',$id)->where('meta_key', $key)->first();
						
						if (empty($exist)) {
							$usermetas = array(
											'user_id'					=> $id,
											'meta_name'					=> $value['label'],
											'meta_key'					=> $key,
											'created_at'				=> date('Y-m-d H:i:s', time()),
											'updated_at'				=> date('Y-m-d H:i:s', time()),
											'meta_value_'.$value['type']	=> (!empty($usermetas) ? $usermetas : $value['value']),
									);
							$user->details()->create($usermetas);
						}else {
							if ($value['type'] == 'text') {
								$exist->meta_value_text = $usermetas;
							}elseif ($value['type'] == 'int'){
								$exist->meta_value_int = $usermetas;
							}
							$exist->updated_at = date('Y-m-d H:i:s', time());
							$exist->save();
						}
					}
					//exit();
					$this->session->set_flashdata('info', '1');
				}else {
					$this->session->set_flashdata('info', 'error');
				}
				redirect(getSetting('admin_path').'/setting/profile');
			}else {
				$data['nav'] = 'setting';
				$data['nav_sub'] = 'setting_profile';
				$data['title'] = 'Update Profile | '.$this->superconfig->getSetting('site_name');
				
				$usermetas = array();
				foreach ($user->details() as $value) {
					$usermetas[$value->meta_key] = $value->meta_value_text;
				}
				$data['usermetas'] = $usermetas;
				
				$tmp = $this->config->item('beta_config');
				
				$data['setting'] = $tmp['users']['default'];

				//js
				$data['script'] = 'admin/pages/script.profile';

				$view = 'pages/profile';
				$this->_view($view, $data);
			}
		}else{
			redirect(getSetting('admin_path').'/setting/profile');
		}
	}

	public function password(){
		if(!$this->input->is_ajax_request()){
			echo 'failed';
			exit();
		}

		$id = $this->session->userdata('user_id');
		$user = User_model::find($id);

		$input = $this->input->post();

		if (strlen(trim($input['password'])) > 5) {
			if ($input['password'] == $input['repassword']) {
				$user->password = md5($input['password']);
				$user->save();

				echo json_encode(array('status'=>'OK'));
				exit;
			}
		}

		echo json_encode(array('status'=>'not OK'));
	}

}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
