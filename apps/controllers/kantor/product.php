<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Product extends MY_Controller_admin {

	public function __construct() {
		parent::__construct();
	}

	function index() {
		$offset = $this->uri->segment(3);
		if (! is_numeric($offset)) {
			$offset = 0;
		}
		$limit = 10;
		$segment = 3;

		$data['nav'] = 'ecomm-product';
		$data['nav_sub'] = 'ecomm-product-index';
		$data['product'] = Ecomm_product_model::where('status !=','deleted')->order_by('updated_at','DESC')->limit($limit, $offset)->get('name,id,harga,qty,status');

		$data['title'] = 'List Product | '.$this->superconfig->getSetting('site_name');

		$link = getSetting('admin_path').'/product';

		$data['pagination'] = $this->fungsi->halaman_admin($link, Ecomm_product_model::where('status !=','deleted')->count(),
				$limit, $segment);

		//js
		$data['script'] = 'admin/product/script.index';

		$this->_view('product/index', $data);
	}

	function create(){
		if ($this->input->post()) {
			$input = $this->input->post();
			
			if(trim($input['name']) != ''){
				//slug
				$tempslug = url_title($input['name'],'-',true);
				$oldtitle = $tempslug;
				$newtitle = $tempslug;
				$curtitle = Ecomm_product_model::where('slug',$newtitle)->get();
				$counter = 1;

				while (!empty($curtitle)) {
					$newtitle = $oldtitle . '-' . $counter;
					$counter++;
					$curtitle = Ecomm_product_model::where('slug',$newtitle)->get();
				}

				//name
				$name = null;
				if(!empty($input['name'])) {
					$name = $input['name'];
				}

				//price
				$price = null;
				if(!empty($input['price'])) {
					$price = $input['price'];
				}

				//special price
				$special_price = null;
				if(!empty($input['special_price'])) {
					$special_price = $input['special_price'];
				}

				//qty
				$qty = null;
				if(!empty($input['qty'])) {
					$qty = $input['qty'];
				}

				//description
				$description = null;
				if(!empty($input['description'])) {
					$description = $input['description'];
				}

				//excerpt
				$excerpt = null;
				if(!empty($input['excerpt'])) {
					$excerpt = $input['excerpt'];
				}

				// Set status
				$status = 'published';
				if(!empty($input['status'])) {
					$status = $input['status'];
				}

				// Set best
				$best = 'no';
				if(!empty($input['best'])) {
					$best = $input['best'];
				}
				
				//seo title
				$seo_title = $name;
				if(!empty($input['seo_title'])) {
					$seo_title = $input['seo_title'];
				}

				//seo keywords
				$seo_keywords = $name;
				if(!empty($input['seo_keywords'])) {
					$seo_keywords = $input['seo_keywords'];
				}

				//seo description
				$seo_description = $name;
				if(!empty($input['seo_description'])) {
					$seo_description = $input['seo_description'];
				}

				/*
				 * Saving Entry data
				*/
				$product = Ecomm_product_model::create(array(
						'name'				=> $name,
						'harga'				=> $price,
						'harga_special'		=> $special_price,
						'qty' 				=> $qty,
						'description'		=> $description,
						'excerpt'			=> $excerpt,
						'status'			=> $status,
						'best'				=> $best,
						'slug'				=> $newtitle,
						'seo_title'			=> $seo_title,
						'seo_keywords'		=> $seo_keywords,
						'seo_description'	=> $seo_description,
						'modified_by'		=> $this->data->user_id,
						'created_at'		=> date('Y-m-d H:i:s', time()),
						'updated_at'		=> date('Y-m-d H:i:s', time())
				));

				if (!empty($input['variant'])){
					foreach ($input['variant'] as $v){
						$variant = Ecomm_variant_model::find($v);
						if(!empty($variant)){
							$variant->product_id = $product->id;
							$variant->updated_at = date('Y-m-d H:i:s', time());
							$variant->save();
						}
					}
				}

				if (!empty($input['taxonomy'])){
					foreach ($input['taxonomy'] as $t){
						$data_et= array(
								'product_id'=>$product->id,
								'taxonomy_id'=>$t,
								'created_at'=>date('Y-m-d H:i:s', time()),
								'updated_at'=>date('Y-m-d H:i:s', time()));
						Entry_taxonomy_model::create($data_et);
					}
				}

				if (!empty($input['media'])){
					foreach ($input['media'] as $m){
						$media = Entry_media_model::create(
									array(
											'product_id'=> $product->id,
											'media_id'=>$m,
											'created_at'=>date('Y-m-d H:i:s', time()),
											'updated_at'=>date('Y-m-d H:i:s', time())
									)
							);
					}
				}
				$this->session->set_flashdata('info', 'success');
				redirect($this->base_admin.'/product');
			}else{
				$this->session->set_flashdata('info', 'error');
				redirect($this->base_admin.'/product/add');
			}
		}else{
			$data['nav'] = 'ecomm-product';
			$data['nav_sub'] = 'ecomm-product-create';
			
			$data['title'] = 'Create new product | '.$this->superconfig->getSetting('site_name');
		
			$data['taxonomies'] = Taxonomy_model::where('taxonomy_type','katproduct')->where('status','active')->get();

			//js
			$data['plugin']['js'][] = 'plugins/ckeditor/ckeditor.js';
			$data['plugin']['js'][] = 'plugins/elastic/jquery.elastic.source.js';
			$data['plugin']['js'][] = 'plugins/form/jquery.form.js';
			
			$data['script'] = 'admin/product/script.create';

			$this->_view('product/create', $data);
		}
	}

	function edit(){
		$id = $this->uri->segment(4);
		$product = Ecomm_product_model::find($id);
		if(empty($product)){
			redirect($this->base_admin.'/product');
		}
		if ($this->input->post()) {
			$input = $this->input->post();
			
			//slug
			$tempslug = url_title($input['name'],'-',true);
			$oldtitle = $tempslug;
			$newtitle = $tempslug;
			if($newtitle != $product->slug){
				$curtitle = Ecomm_product_model::where('slug',$newtitle)->get();
				$counter = 1;

				while (!empty($curtitle)) {
					$newtitle = $oldtitle . '-' . $counter;
					$counter++;
					$curtitle = Ecomm_product_model::where('slug',$newtitle)->get();
				}
			}

			//name
			$name = null;
			if(!empty($input['name'])) {
				$name = $input['name'];
			}

			//price
			$price = null;
			if(!empty($input['price'])) {
				$price = $input['price'];
			}

			//special price
			$special_price = null;
			if(!empty($input['special_price'])) {
				$special_price = $input['special_price'];
			}

			//qty
			$qty = null;
			if(!empty($input['qty'])) {
				$qty = $input['qty'];
			}

			//description
			$description = null;
			if(!empty($input['description'])) {
				$description = $input['description'];
			}

			//excerpt
			$excerpt = null;
			if(!empty($input['excerpt'])) {
				$excerpt = $input['excerpt'];
			}

			// Set status
			$status = 'published';
			if(!empty($input['status'])) {
				$status = $input['status'];
			}

			// Set best
			$best = 'no';
			if(!empty($input['best'])) {
				$best = $input['best'];
			}
			
			//seo title
			$seo_title = $name;
			if(!empty($input['seo_title'])) {
				$seo_title = $input['seo_title'];
			}

			//seo keywords
			$seo_keywords = $name;
			if(!empty($input['seo_keywords'])) {
				$seo_keywords = $input['seo_keywords'];
			}

			//seo description
			$seo_description = $name;
			if(!empty($input['seo_description'])) {
				$seo_description = $input['seo_description'];
			}

			$product->name = $name;
			$product->slug = $newtitle;
			$product->harga = $price;
			$product->harga_special = $special_price;
			$product->qty = $qty;
			$product->description = $description;
			$product->excerpt = $excerpt;
			$product->status = $status;
			$product->best = $best;
			$product->seo_title = $seo_title;
			$product->seo_keywords = $seo_keywords;
			$product->seo_description = $seo_description;
			$product->updated_at = date('Y-m-d H:i:s', time());
			$product->modified_by = $this->data->user_id;
			$product->save();

			if (!empty($input['variant'])){
				foreach ($input['variant'] as $v){
					$variant = Ecomm_variant_model::find($v);
					if(!empty($variant)){
						$variant->product_id = $product->id;
						$variant->updated_at = date('Y-m-d H:i:s', time());
						$variant->save();
					}
				}
			}

			if (!empty($input['variant'])){
				foreach ($input['variant'] as $v){
					$variant = Ecomm_variant_model::find($v);
					if(!empty($variant)){
						$variant->product_id = $product->id;
						$variant->updated_at = date('Y-m-d H:i:s', time());
						$variant->save();
					}
				}
			}

			$entry_taxonomy = Entry_taxonomy_model::where('product_id',$id)->delete();
			if (!empty($input['taxonomy'])){
				foreach ($input['taxonomy'] as $key=>$t){
					$data_et= array(
							'product_id'=>$product->id,
							'taxonomy_id'=>$t,
							'created_at'=>date('Y-m-d H:i:s', time()),
							'updated_at'=>date('Y-m-d H:i:s', time())
					);
					Entry_taxonomy_model::create($data_et);
				}
			}

			if (!empty($input['media'])){
				foreach ($input['media'] as $m){
					$exist = Entry_media_model::where('product_id',$id)->where('media_id',$m)->get();
					//mpr($exist);
					if (empty($exist)) {
						$media = Entry_media_model::create(
							array(
								'product_id'=> $id,
								'media_id'=>$m,
								'created_at'=>date('Y-m-d H:i:s', time()),
								'updated_at'=>date('Y-m-d H:i:s', time())
							)
						);
					}
				}
			}

			$this->session->set_flashdata('info', 'success');

			redirect($this->base_admin.'/product/edit/'.$id);
		}else{
			
			$data['product'] = $product;

			$data['nav'] = 'ecomm-product';
			$data['nav_sub'] = 'ecomm-product-index';
			
			$data['title'] = 'Update product | '.$this->superconfig->getSetting('site_name');
		
			$data['taxonomies'] = Taxonomy_model::where('taxonomy_type','katproduct')->where('status','active')->get();

			$data['taxonomy_list'] = $product->getTaxonomyListByProduct($id);
			$data['media'] = $product->getMediaListByProduct($id);
			$data['variants'] = Ecomm_variant_model::where('product_id',$product->id)->get();

			//js
			$data['plugin']['js'][] = 'plugins/ckeditor/ckeditor.js';
			$data['plugin']['js'][] = 'plugins/elastic/jquery.elastic.source.js';
			$data['plugin']['js'][] = 'plugins/form/jquery.form.js';
			
			$data['script'] = 'admin/product/script.update';

			$this->_view('product/update', $data);
		}
	}

	function variant_add(){
		if(!$this->input->is_ajax_request()){
			echo 'failed';
			exit();
		}
		$input = $this->input->post();
		if(trim($input['label']) == ''){
			echo json_encode(array('status'=>'error','message'=>'Label harap di isi.'));
			exit;
		}else{
			if(empty($input['options'])){
				echo json_encode(array('status'=>'error','message'=>'Option minimal 1'));
				exit;
			}
			$option = false;
			$form_option = '';
			foreach ($input['options'] as $value) {
				if(trim($value) != '' ){
					$option = $option.'::'.$value;
					$form_option = $form_option.' <i class="fa fa-tags fa-fw"></i> '.$value;
				}
			}

			if(!$option){
				echo json_encode(array('status'=>'error','message'=>'Option minimal 1'));
				exit;
			}

			$variant = Ecomm_variant_model::create(array(
						'label'						=> $input['label'],
						'option'					=> $option,
						'modified_by'				=> $this->data->user_id,
						'created_at'				=> date('Y-m-d H:i:s', time()),
						'updated_at'				=> date('Y-m-d H:i:s', time())
				));

			$message = '<div class="form-group variant variant-'.$variant->id.'">
                            <label>
                            	'.$input['label'].'
                            	<input type="hidden" value="'.$variant->id.'" name="variant[]">
                            	<a href="javascript:void(0)" class="btn btn-warning btn-xs" data-id="'.$variant->id.'" role="button"><i class="fa fa-times fa-fw"></i></a>
                            	<a href="javascript:void(0)" class="btn btn-info btn-xs" data-id="'.$variant->id.'" role="button"><i class="fa fa-edit fa-fw"></i></a>
                            </label>
                            <p class="form-control-static">'.$form_option.'</p>
                        </div>';

			echo json_encode(
				array(
					'status' => 'OK',
					'id' => $variant->id,
					'message' => $message,
					'operation' => 'add'
					)
				);
			exit;
		}
	}

	function variant_del(){
		if(!$this->input->is_ajax_request()){
			echo 'failed';
			exit();
		}
		$variant = Ecomm_variant_model::find($this->input->post('id'));

		$variant->delete();

		echo json_encode(array('status'=>'OK'));
		exit;
	}

	function variant_update(){
		if(!$this->input->is_ajax_request()){
			echo 'failed';
			exit();
		}
		$variant = Ecomm_variant_model::find($this->input->post('id'));
		if(!empty($variant)){
			$varTemp = explode('::',$variant->option);
			$option = '';
			foreach ($varTemp as $value) {
				if(trim($value) != ''){
					$option = $option.'<div class="form-group input-group input-option">
		                            <input type="text" class="form-control" value="'.$value.'" name="options[]" placeholder="options">
		                            <span class="input-group-btn">
		                                <button class="btn btn-warning" type="button"><i class="fa fa-times"></i>
		                                </button>
		                            </span>
		                        </div>';
		        }
			}
			echo json_encode(
				array(
					'status'=>'OK',
					'label'=> $variant->label,
					'options'=>'<label>Options <a href="javascript:void(0)" class="btn btn-info btn-xs addOptions"><i class="fa fa-plus fa-fw"></i> Add new</a></label>'
								.$option
					)
			);
			exit;
		}else{
			echo json_encode(array('status'=>'error'));
			exit;
		}
	}

	public function variant_update_proses(){
		if(!$this->input->is_ajax_request()){
			echo 'failed';
			exit();
		}
		$input = $this->input->post();
		if(trim($input['label']) == ''){
			echo json_encode(array('status'=>'error','message'=>'Label harap di isi.'));
			exit;
		}else{
			$variant = Ecomm_variant_model::find($this->input->post('id'));
			if(empty($variant)){
				echo json_encode(array('status'=>'error','message'=>'Data tidak dikenali'));
				exit;
			}
			$option = false;
			$form_option = '';
			foreach ($input['options'] as $value) {
				if(trim($value) != '' ){
					$option = $option.'::'.$value;
					$form_option = $form_option.' <i class="fa fa-tags fa-fw"></i> '.$value;
				}
			}

			if(!$option){
				echo json_encode(array('status'=>'error','message'=>'Option minimal 1'));
				exit;
			}

			$variant->label = $input['label'];
			$variant->option = $option;
			$variant->updated_at = date('Y-m-d H:i:s', time());
			$variant->modified_by = $this->data->user_id;
			$variant->save();

			$message = '<label>
                        	'.$input['label'].'
                        	<input type="hidden" value="'.$variant->id.'" name="variant[]">
                        	<a href="javascript:void(0)" class="btn btn-warning btn-xs" data-id="'.$variant->id.'" role="button"><i class="fa fa-times fa-fw"></i></a>
                        	<a href="javascript:void(0)" class="btn btn-info btn-xs" data-id="'.$variant->id.'" role="button"><i class="fa fa-edit fa-fw"></i></a>
                        </label>
                        <p class="form-control-static">'.$form_option.'</p>';

			echo json_encode(
				array(
					'status' => 'OK',
					'id' => $variant->id,
					'message' => $message,
					'operation' => 'update'
					)
				);
			exit;
		}
	}
}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
