<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dashboard extends MY_Controller_admin {

	public function __construct() {
		parent::__construct();
	}

	function index() {
		$data = array();
		$data['nav'] = 'dashboard';
		$data['title'] = 'Dashboard | '.$this->superconfig->getSetting('site_name');
		$this->_view('pages/dashboard', $data);
	}

}

/*
 * Programmed by : Dadang Sasmita
 * dadangsasmita[at]gmail[dot]com
 * follow me @d_sasmita
 *
 */
