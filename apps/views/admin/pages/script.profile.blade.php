<script src="<?php echo base_url('public/backend/js/jquery.form.js') ?>"></script>
<script>
var delete_upload;
$(document).ready(function(){
	//manage pdf file
	var percent = $('.percent');

	$('#uploadfile').ajaxForm({
		dataType:  'json',
		beforeSend: function() {
			var percentVal = '0%';
			$('#btn_upload').hide();
			$('.percent').show();
			percent.html(percentVal);
		},
		uploadProgress: function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			percent.html('Uploading .... ' + percentVal);
		},
		success: function(xhr) {
			console.log(xhr);
			if(xhr.status == 'OK'){
				$("input[name=\"usermetas[avatar]\"]").val(xhr.id);
				percent.html(xhr.name+ "  (<a id='btn_remove' href='javascript:void(0)' onclick='delete_upload()'>remove</a>)");
			}else{
				alert('upload failed.');
				percent.hide();
				$('#btn_upload').show();
			}
		}
	});
	$('#btn_upload').click(function(){
		$("#input_file_upload").trigger("click");
	});
	$("#input_file_upload").change(function(event) {
		 $('#uploadfile').submit();
	});
	delete_upload = function(){
		if(confirm("Are you sure to delete the associated file?"))
		{
			//Remove pdf_id file
			var media_id = $("input[name=\"usermetas[avatar]\"]").val();
			$("input[name=\"usermetas[avatar]\"]").val('');

			//Ajax to destroy
			$.ajax({
				dataType : 'json',
				type: "POST",
				url: '<?php echo $base_admin.'/media/delete_avatar' ?>',
				data: {'id':media_id},
				success : function(result) {
					if(result.status == 'OK'){
						$('#btn_upload').show();
						$(".percent").hide();
					}else{
						alert('file not found');
					}
				}
			});
		}
	}
});
</script>