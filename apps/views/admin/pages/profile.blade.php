<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-user"></i>Profile<br>
                <small>Update Profile</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li>Profile</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Profile</h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <form id="form" class="form-data" action="<?php echo current_url(); ?>" method="post">
			<div class="form">
			<?php if ($this->session->flashdata('info') == 'error'){ ?>
	            <div class="row">
	                <div class="col-lg-12">
	                    <div class="alert alert-warning alert-dismissable">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                        <i class="fa fa-info-circle"></i> Silahkan Isi data dengan benar.
	                    </div>
	                </div>
	            </div>
	        <?php } ?>
			<?php if ($this->session->flashdata('info') == 1){ ?>
				<div class="row">
	                <div class="col-lg-12">
	                    <div class="alert alert-success alert-dismissable">
	                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                        <i class="fa fa-info-circle"></i> Profil berhasil di ubah.
	                    </div>
	                </div>
	            </div>
			<?php } ?>
				<div class="fields">
					<div class="field">
						<div class="form-group">
			                <label>First Name*</label>
			                <input placeholder="nama depan" class="form-control" required type="text" id="first-name" name='usermetas[first_name]' value="<?php echo @$usermetas['first_name'] ?>">
			            </div>
			            <div class="form-group">
			                <label>Last Name</label>
			                <input placeholder="nama belakang" class="form-control" type="text" id="last-name" name='usermetas[last_name]' value="<?php echo @$usermetas['last_name'] ?>">
			            </div>
					</div>
					<div class="field">
						<div class="form-group">
			                <label>Avatar</label>
			                <?php
							$upload_id = getUserMeta($account->id,'avatar');
							$media_upload = array();
							if(!empty($upload_id)){
								$media_upload = Media_model::find($upload_id->meta_value_text);
								if(! empty($media_upload)){
									$ext = explode('/', $media_upload->media_mime)
									?>
									<a href="javascript:void(0)" id='btn_upload' style="display: none">Upload</a>
									<div class="percent"><?php echo $media_upload->title.'.'.$ext[1]; ?> (<a id='btn_remove' href='javascript:void(0)' onclick='delete_upload()'>remove</a>)</div >
									<?php
								}else {
									?>
									<a href="javascript:void(0)" id='btn_upload'>Upload</a>
									<div class="percent"></div >
									<?php
								}
							}else {
									?>
									<a href="javascript:void(0)" id='btn_upload'>Upload</a>
									<div class="percent"></div >
									<?php
								}
							?>
							<span style="font-size: smaller;">File diterima PNG, Gif, JPG</span>
							<input type="hidden" name="usermetas[avatar]" value="<?php echo @$usermetas['avatar'] ?>" />
			            </div>
					</div>
					<?php
					$blacklisted = array(
						'first_name',
						'last_name',
						'avatar'
					);
					foreach ($setting as $key => $val) {
						$params = array();
						if($val['visibility'] != 'system'):
							if(!in_array($key, $blacklisted)):
								$func = 'form_'.$val['type'];
								$params = array(
									'name'	=> "usermetas[$key]",
									'label'	=> $val['label'],
								);

								if (isset($val["help-block"]))
									$params["help-block"] = $val["help-block"];

								if (!empty($val["class"]))
									$params["class"] = $val["class"];
								if($val['type'] == 'checkbox' OR $val['type'] == 'radio' OR $val['type'] == 'pulldown') {
									$params['option'] = $val['option'];
								}

								if(isset($val['multilanguage']) && $val['multilanguage'] == true) {
									$params['name'] = "usermetas[$key][".$this->lang->lang()."]";
									$params['lang'] = $this->lang->lang();
									$params['value'] = @$usermetas[$key];

									echo $func($params);

									foreach($this->lang->lang_available() as $langs => $value):
										if($langs != $this->lang->lang()):
											$params['name'] = "usermetas[$key][$langs]";
											$params['lang'] = $langs;
											$params['value'] = @$usermetas[$key];

											echo $func($params);
										endif;
									endforeach;
								}else {
									$params['value'] = @$usermetas[$key];
									echo $func($params);
								}
							endif;
						endif;
					}
					?>
					<div class="title">
						<h5>Login Information</h5>
					</div>
					<div class="field">
						<div class="form-group">
			                <label>Username</label>
			                <input class="form-control" type="text" readonly="readonly" id="username" name="username" value="<?php echo @$account->username ?>">
			            </div>
					</div>
					<div class="field">
						<div class="form-group">
			                <label>Email*</label>
			                <input class="form-control" type="text" readonly="readonly" id="email" name="email" value="<?php echo @$account->email ?>">
			            </div>
					</div>
					<div class="field">
						<div class="form-group">
			                <label>Password</label>
			                <input class="form-control" placeholder="password baru" type="password" id="password" name="password">
			            </div>
					</div>
					<div class="field">
						<div class="form-group">
			                <label>Password Verifikasi</label>
			                <input class="form-control" placeholder="verifikasi password" type="password" id="password-reff" name="password_reff">
			            </div>
					</div>
					<div class="buttons">
						<input type="reset" class="btn btn-default" name="reset" value="Reset" />
						<input type="submit" class="btn btn-primary" name="submit.highlight" value="Save" />
					</div>
				</div>
			</div>
		</form>
    </div>
    <!-- END Datatables Content -->
<div style="display:none">
<form action="<?php echo $base_admin.'/media/avatar' ?>" method="post" id='uploadfile' enctype="multipart/form-data">
	<input type="file" id="input_file_upload" name="userfile"/>
</form>
</div>
</div>