<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-settings"></i>Pengaturan<br>
                <small>Pengaturan Umum</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li>Pengaturan</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Pengaturan</h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
        <?php if ($this->session->flashdata('info')){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i> Pengaturan berhasil di ubah.
                    </div>
                </div>
            </div>
        <?php } ?>
         <form id="form" action="<?php echo $base_admin.'/setting/save' ?>" method="post">
			<input type="hidden" name="from" value="general">
			<div class="form-group">
                <label>Site Name*</label>
                <input class="form-control" type="text" id="site_name" name='site_name' value="<?php echo getSetting('site_name') ?>">
            </div>
            <div class="form-group">
                <label>Slogan</label>
                <input class="form-control" type="text" id="site_slogan" name='site_slogan' value="<?php echo getSetting('site_slogan') ?>">
            </div>
            <div class="form-group">
                <label>Keyword</label>
                <input class="form-control" type="text" id="site_keywords" name='site_keywords' value="<?php echo getSetting('site_keywords') ?>">
            </div>
            <div class="form-group">
                <label>Deskripsi Situs</label>
            	<textarea class="form-control" rows="3" id="site_description" style="form-control width: 350px; height: 100px" name='site_description' ><?php echo getSetting('site_description') ?></textarea>
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <textarea id="contact_address" class="form-control" name='contact_address' ><?php echo getSetting('contact_address') ?></textarea>
            </div>
            <button type="reset" class="btn btn-default">Reset</button>
        	<button type="submit" class="btn btn-primary">Simpan</button>
		</form>
    </div>
    <!-- END Datatables Content -->
</div>