<div id="right">
	<div class="box">
		<!-- box / title -->
		<div class="title">
			<h5>Extras Setting</h5>
		</div>
		<!-- end box / title -->

	<form id="form" action="<?php echo $base_admin.'/setting/save' ?>" method="post">
	<input type="hidden" name="from" value="extras">
		<div class="form">
		<?php if ($this->session->flashdata('info')){ ?>
		<div id="message-success" class="message message-success">
			<div class="image">
				<img src="<?php echo base_url('public/backend/images/icons/success.png') ?>" alt="Success" height="32">
			</div>
			<div class="text">
				<h6>Success Message</h6>
				<span>Extras Setting Berhasil disimpan.</span>
			</div>
			<div class="dismiss">
				<a href="#message-success"></a>
			</div>
		</div>
		<?php } ?>
			<div class="fields">
				<div class="field  field-first">
					<div class="label">
						<label for="contact_phone">Contact Phone:</label>
					</div>
					<div class="input">
						<input type="text" id="contact_phone" name='contact_phone' value="<?php echo getSetting('contact_phone') ?>" class="small" />
					</div>
				</div>
				<div class="field">
					<div class="label">
						<label for="contact_email">Contact Email:</label>
					</div>
					<div class="input">
						<input type="text" id="contact_email" name='contact_email' value="<?php echo getSetting('contact_email') ?>" class="medium" />
					</div>
				</div>
				<div class="field">
					<div class="label">
						<label for="contact_email">Receive Email:</label>
					</div>
					<div class="input">
						<input type="text" id="contact_email" name='received_email' value="<?php echo getSetting('received_email') ?>" class="medium" />
					</div>
				</div>
				<div class="field">
					<div class="label">
						<label for="social_facebook">Facebook Page:</label>
					</div>
					<div class="input">
						<input type="text" id="social_facebook" name='social_facebook' value="<?php echo getSetting('social_facebook') ?>" class="medium" />
					</div>
				</div>
				<div class="field">
					<div class="label">
						<label for=social_twitter>Twitter:</label>
					</div>
					<div class="input">
						<input type="text" id="social_twitter" name='social_twitter' value="<?php echo getSetting('social_twitter') ?>" class="small" />
					</div>
				</div>
				<div class="buttons">
					<input type="reset" name="reset" value="Reset" />
					<div class="highlight">
						<input type="submit" name="submit.highlight" value="Save" />
					</div>
				</div>
			</div>
		</div>
	</form>
	</div>
</div>