<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-group"></i>Akun<br>
                <small>Akun Tambah</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li><a href="<?php echo $base_admin.'/account'; ?>">Akun</a></li>
        <li>Tambah</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Akun</h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <?php if ($this->session->flashdata('info') == 'error'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  Silahkan isi data dengan benar.
                    </div>
                </div>
            </div>
        <?php } ?>
        <form role="form" action="<?php echo current_url(); ?>" method="post">
            <div class="form-group">
                <label>First Name*</label>
                <input class="form-control" name="usermetas[first_name]" placeholder="first name account" required>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input class="form-control" name="usermetas[last_name]" placeholder="last name account">
            </div>
            <h3>Additional Information</h3>
            <?php
            $blacklisted = array(
                'first_name',
                'last_name',
                'avatar',
                'about',
                'birth_date',
                'postal_code',
                'address',
                'city',
                'province',
                'country',
                'contact_phone'
            );
            foreach ($setting as $key => $val) {
                $params = array();
                if($val['visibility'] != 'system'):
                    if(!in_array($key, $blacklisted)):
                        $func = 'form_'.$val['type'];
                        $params = array(
                            'name'  => "usermetas[$key]",
                            'label' => $val['label'],
                        );

                        if (isset($val["help-block"]))
                            $params["help-block"] = $val["help-block"];

                        if (!empty($val["class"]))
                            $params["class"] = $val["class"];
                        if($val['type'] == 'checkbox' OR $val['type'] == 'radio' OR $val['type'] == 'pulldown') {
                            $params['option'] = $val['option'];
                        }

                        if(isset($val['multilanguage']) && $val['multilanguage'] == true) {
                            $params['name'] = "usermetas[$key][".$this->lang->lang()."]";
                            $params['lang'] = $this->lang->lang();

                            echo $func($params);

                            foreach($this->lang->lang_available() as $langs => $value):
                                if($langs != $this->lang->lang()):
                                    $params['name'] = "usermetas[$key][$langs]";
                                    $params['lang'] = $langs;

                                    echo $func($params);
                                endif;
                            endforeach;
                        }else {
                            echo $func($params);
                        }
                    endif;
                endif;
            }
            ?>
            <h3>Login Information</h3>
            <div class="form-group">
                <label>Username*</label>
                <input class="form-control" name="username" placeholder="username" required>
                <p class="help-block">form isian huruf tanpa spasi, minimal 5 karakter</p>
            </div>
            <div class="form-group">
                <label>Email*</label>
                <input class="form-control" id="email" name="email" placeholder="email" type="email" required>
                <p class="help-block">email account</p>
            </div>
            <div class="form-group">
                <label>Role / Jabatan*</label>
                <select class="form-control" name="role" id="role">
                    <option value="0">-role-</option>
                    <?php
                    foreach ($roles as $value) {
                        echo "<option value='$value->name' >$value->name</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Password*</label>
                <input class="form-control" type="password" id="password" name="password" placeholder="password" required>
                <p class="help-block">Password</p>
            </div>
            <div class="form-group">
                <label>Password Verifikasi*</label>
                <input class="form-control" type="password" id="password-reff" name="password_reff" placeholder="Password Verifikasi" required>
                <p class="help-block">Password</p>
            </div>

            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    <!-- END Datatables Content -->
</div>