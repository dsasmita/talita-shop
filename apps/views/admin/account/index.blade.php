<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-group"></i>Akun<br>
                <small>Akun Index</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li>Akun</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Akun</h2>
            <div class="block-options pull-right">
                <a href="<?php echo $base_admin.'/account/add';?>" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Tambah Akun</a>
            </div>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th><center>Username</center></th>
                        <th><center>Role</center></th>
                        <th><center>Last Login</center></th>
                        <th><center>Action</center></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(!empty($accounts)){

                        foreach ($accounts as $a) {
                            ?>
                    <tr>
                        <td>
                            <a href="<?php echo $base_admin.'/account/edit/'.$a->id ?>">
                                <?php echo $a->username ?>
                            </a>
                        </td>
                        <td><?php echo $a->type ?></td>
                        <td><?php echo date('j M Y, H:i',strtotime($a->last_login)) ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="<?php echo $base_admin.'/account/edit/'.$a->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a onClick="return confirm('Apakah anda akan menghapus akun ini?')" href="<?php echo $base_admin.'/account/delete/'.$a->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <ul class="pagination pagination-sm">
                <?php
                echo @$pagination;
                ?>
            </ul>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>