<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-adjust_alt"></i><?php echo $type['label']['plural'] ?><br>
                <small>Index</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li><?php echo $type['label']['plural'] ?></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>List <strong><?php echo $type['label']['single'] ?></strong></h2>
            <div class="block-options pull-right">
                <a href="<?php echo $base_admin.'/taxonomy/'.$type_taxo.'/add';?>" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Tambah <?php echo $type['label']['single'] ?></a>
            </div>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <?php if ($this->session->flashdata('update_info') == 'success'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i> Update taxonomy berhasil.
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('delete_info') == 'success'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i> Delete taxonomy berhasil.
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('add_info') == 'success'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i> Tambah taxonomy berhasil.
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>Titel</th>
                        <th class="text-center">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Update&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </th>
                        <th class="text-center">&nbsp;&nbsp;&nbsp;Aksi&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(!empty($taxonomies)){

                        foreach ($taxonomies as $a) {
                            ?>
                    <tr>
                        <td>
                            <a href="<?php echo $base_admin.'/taxonomy/'.$type_taxo.'/edit/'.$a->id ?>">
                                <?php echo $a->name ?>
                            </a>
                        </td>
                        <td><center><?php echo date('j M Y, H:i',strtotime($a->updated_at)) ?></center></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="<?php echo $base_admin.'/taxonomy/'.$type_taxo.'/edit/'.$a->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a onClick="return confirm('Apakah Anda akan menghapus kategori ini?')" href="<?php echo $base_admin.'/taxonomy/'.$type_taxo.'/delete/'.$a->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
