<script>
	$(document).ready(function() {
		/*text area*/
		jQuery('textarea').elastic();

		/*deskripsi*/
		var maxChar = 1500;
		var charNow = $('.deskripsi').val().length;
		var charLeft = maxChar - charNow;
		$('.notif-char').text(charLeft);
		
		$('.deskripsi').keyup(function(e){
			var charNow = $(this).val().length;
			var charLeft = maxChar - charNow;
			$('.notif-char').text(charLeft);
			if(charNow > maxChar){
				$('.notif-char').text(0);
				$(this).val($(this).val().substr(0, maxChar));
			}
		});
	});
</script>