<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-adjust_alt"></i><?php echo $type['label']['plural'] ?><br>
                <small>Tambah</small>
            </h1>
        </div>
    </div>
     <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li><a href="<?php echo $base_admin.'/taxonomy/'.$type_taxo; ?>"><?php echo $type['label']['plural'] ?></a></li>
        <li>Tambah</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Tambah <strong><?php echo $type['label']['single'] ?></strong></h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <form role="form" action="<?php echo current_url(); ?>" method="post">
            <div class="form-group">
                <label>Name*</label>
                <input class="form-control" id="name" name='taxonomy[name]' placeholder="name" required>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control deskripsi" maxlength="2000" rows="3" placeholder="description" name="taxonomy[description]"></textarea>
                <span class="list-desc">
                    isi deskripsi dengan lengkap dan jelas
                </span>
                <span class="pull-right">&nbsp; karakter</span>
                <span class="pull-right notif-char">
                    1500
                </span>
            </div>
            <?php if(!empty($parents)){ ?>
                <div class="form-group">
                    <label>Parents</label>
                    <select class="form-control"  name="taxonomy[parent]">
                        <option value="0">no parent</option>
                        <?php
                        foreach ($parents as $parent){
                            echo "<option value='$parent->id'>$parent->name</option>";
                        }
                        ?>
                    </select>
                </div>
            <?php } ?>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    <!-- END Datatables Content -->
</div>
