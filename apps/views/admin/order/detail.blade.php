<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-money"></i>Order<br>
                <small>Order Index</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li><a href="<?php echo $base_admin.'/orders'; ?>">Order</a></li>
        <li>Detail</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Order</h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <div class="row ">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Data Order Detail
                        <div class="pull-right">
                            <a href="#" class="btn btn-primary btn-xs edit-detail"><i class="fa fa-edit fa-fw"></i> Edit</a>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped text-detail">
                                <tbody>
                                    <tr>
                                        <td><strong>Inv.No</strong></td>
                                        <td>#<?php echo $order->id ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Email</strong></td>
                                        <td><?php echo empty($customer)?'-':$customer->email ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Nama Penerima</strong></td>
                                        <td>
                                            <?php echo empty($address)?'-':$address->penerima ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Phone</strong></td>
                                        <td>
                                            <?php echo empty($address)?'-':$address->phone ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Alamat</strong></td>
                                        <td>
                                            <?php echo empty($address)?'-':$address->address ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Provinsi</strong></td>
                                        <td>
                                            <?php echo empty($address)?'-':$address->province ?>
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td><strong>Kota</strong></td>
                                        <td>
                                            <?php echo empty($address)?'-':$address->city ?>
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td><strong>Kodepos</strong></td>
                                        <td>
                                            <?php echo empty($address)?'-':$address->postal_code ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Waktu Order</strong></td>
                                        <td><?php echo date('Y/m/d, G:i:s',strtotime($order->created_at)) ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Status Order</strong></td>
                                        <td><?php echo $order->status ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <form method="post" class="form_detail" action="<?php echo $base_admin.'/order/detail_edit' ?>">
                                <input type="hidden" name="order_id" value="<?php echo $order->id ?>">
                                <table class="table table-striped form-detail" style="display:none">
                                    <tbody>
                                        <tr>
                                            <td><strong>Inv.No</strong></td>
                                            <td>#<?php echo $order->id ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Email</strong></td>
                                            <td><?php echo empty($customer)?'-':$customer->email ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Nama Penerima*</strong></td>
                                            <td>
                                                <input placeholder="nama penerima" class="form-control" name="penerima" required type="text" value="<?php echo empty($address)?'-':$address->penerima ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Phone*</strong></td>
                                            <td>
                                                <input class="form-control" placeholder="no handphone" required name="phone" type="text" value="<?php echo empty($address)?'-':$address->phone ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Alamat*</strong></td>
                                            <td>
                                                <textarea class="form-control" placeholder="alamat" name="alamat"><?php echo empty($address)?'-':$address->address ?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Provinsi*</strong></td>
                                            <td>
                                                <input class="form-control" name="province" type="text" required value="<?php echo empty($address)?'-':$address->province ?>">
                                            </td> 
                                        </tr>
                                        <tr>
                                            <td><strong>Kota*</strong></td>
                                            <td>
                                                <input class="form-control" name="city" type="text" required value="<?php echo empty($address)?'-':$address->city ?>">
                                            </td> 
                                        </tr>
                                        <tr>
                                            <td><strong>Kodepos</strong></td>
                                            <td>
                                                <input class="form-control" name="postal_code" type="text" value="<?php echo empty($address)?'-':$address->postal_code ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Status Order</strong></td>
                                            <td>
                                                <select class="form-control" name="status">
                                                  <option <?php echo $order->status == 'New' ? 'selected' : '' ?> value="New">New Order</option>
                                                  <option <?php echo $order->status == 'Accepted' ? 'selected' : '' ?> value="Accepted">Accepted</option>
                                                  <option <?php echo $order->status == 'Shipped' ? 'selected' : '' ?> value="Shipped">Shipped</option>
                                                  <option <?php echo $order->status == 'Delivered' ? 'selected' : '' ?> value="Delivered">Delivered</option>
                                                  <option <?php echo $order->status == 'Canceled' ? 'selected' : '' ?> value="Canceled">Canceled</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <button class="btn btn-default" type="button">Batal</button>
                                                <button class="btn btn-primary" type="submit">Save</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Status Order
                    </div>
                    
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <form class="order_status" action="<?php echo $base_admin.'/order/status_order' ?>" method="post">
                            <input type="hidden" name="order_id" value="<?php echo $order->id ?>">
                            <select class="form-control" name="status">
                                  <option <?php echo $order->status == 'New' ? 'selected' : '' ?> value="New">New Order</option>
                                  <option <?php echo $order->status == 'Accepted' ? 'selected' : '' ?> value="Accepted">Accepted</option>
                                  <option <?php echo $order->status == 'Shipped' ? 'selected' : '' ?> value="Shipped">Shipped</option>
                                  <option <?php echo $order->status == 'Delivered' ? 'selected' : '' ?> value="Delivered">Delivered</option>
                                  <option <?php echo $order->status == 'Canceled' ? 'selected' : '' ?> value="Canceled">Canceled</option>
                            </select>
                            <br>
                            <?php if($order->status == 'Accepted' || $order->status == 'Shipped' || $order->status == 'Delivered'){ ?>
                                <input type="text" class="form-control" name="kurir" placeholder="Jasa Pengiriman"/>
                                <br>
                                <input type="text" class="form-control" name="resi" placeholder="No. Resi Pengiriman"/>
                                <br>
                            <?php } ?>
                            <button type="submit" class="btn btn-block btn-primary">Change</button>
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Order Items
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Kode Barang</th>
                                        <th>Harga</th>
                                        <th>QTY</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($order_details)){
                                        $total = 0;
                                        foreach ($order_details->result() as $value) {
                                            $total = $total+$value->unit_price*$value->qty;
                                    ?>
                                    <tr>
                                        <td><?php echo $value->kode ?><br>
                                            <small><em><?php echo $value->name ?></em></small>
                                        </td>
                                        <td><div class="pull-right"><?php echo 'Rp. '.number_format($value->unit_price,2) ?></div></td>
                                        <td><div class="pull-right"><?php echo $value->qty ?></div></td>
                                        <td><div class="pull-right"><?php echo 'Rp. '.number_format($value->unit_price*$value->qty,2) ?></div></td>
                                    </tr>
                                    <?php }} ?>
                                    <tr>
                                        <td colspan="3">Total</td>
                                        <td><div class="pull-right"><?php echo 'Rp. '.number_format($total,2) ?></div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>