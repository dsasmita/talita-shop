<script>
	$(document).ready(function() {
		$('.order_status').submit(function(e){
			e.preventDefault();
			var data = $(this).serialize();
			var url = $(this).attr('action');
			$("form.order_status button").button('loading');
			$.ajax({
				dataType: "json",
				type: "POST",
				url: url,
				data: data
			}).done(function(result) {
				$("form.order_status button").button('reset');
				if(result.status == 'OK'){
					alert('Ubah password berhasil');
					location.reload();
				}else{
					alert('Terjadi kesalahan silahkan ulangi kembali.')
				}
			});
		});

		$('.edit-detail').click(function(e){
			e.preventDefault();
			$(this).hide();
			$('.text-detail').hide();
			$('.form-detail').show();
		});

		$('.form_detail').submit(function(e){
			e.preventDefault();
			var data = $(this).serialize();
			var url = $(this).attr('action');
			$("form.form_detail button.btn-primary").button('loading');
			$.ajax({
				dataType: "json",
				type: "POST",
				url: url,
				data: data
			}).done(function(result) {
				$("form.form_detail button.btn-primary").button('reset');
				if(result.status == 'OK'){
					alert('Ubah status berhasil');
					location.reload();
				}else{
					alert('Terjadi kesalahan.')
				}
			});
		});

		$('.form_detail .btn-default').click(function(e){
			e.preventDefault();
			$('.edit-detail').show();
			$('.text-detail').show();
			$('.form-detail').hide();
		});
	});
</script>