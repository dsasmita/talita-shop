<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-money"></i>Order<br>
                <small>Order Index</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li>Order</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Order</h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <div class="table-responsive">
            <?php if ($this->session->flashdata('info') == 'error'){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i> Operasi berhasil.
                        </div>
                    </div>
                </div>
            <?php } ?>
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th width="40%">Order</th>
                        <th>Status</th>
                        <th>Total</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(!empty($orders)){

                        foreach ($orders->result() as $a) {
                            ?>
                    <tr class="odd gradeX" data-id="<?php echo $base_admin.'/order/detail/'.$a->id ?>" style="cursor:pointer;cursor:hand;">
                        <td>
                            <?php echo '<strong>#'.$a->id.' - '.$a->name.'</strong>' ?><br>
                            <small><em><?php echo $a->city.', '.$a->province ?></em></small>
                        </td>
                        <td><center><?php echo $a->status ?></center></td>
                        <td><div class="pull-right"><?php echo 'Rp. '.number_format($a->total,2) ?></div></td>
                        <td>
                            <center>
                                <?php echo date('Y/m/d',strtotime($a->created_at)) ?><br>
                                <small><em><?php echo date("G:i:s",strtotime($a->created_at));  ?></em></small>
                            </center>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <ul class="pagination pagination-sm">
                <?php
                echo @$pagination;
                ?>
            </ul>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>