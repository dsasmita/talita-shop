<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-money"></i>Order<br>
                <small>Konfirmasi Order</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li>Konfirmasi Order</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Konfirmasi Order</h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <div class="table-responsive">
            <?php if ($this->session->flashdata('info') == 'success'){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i> ubah status data berhasil.
                        </div>
                    </div>
                </div>
            <?php } ?>
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th width="40%">Order Confirmation</th>
                        <th>Status</th>
                        <th>Total</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(!empty($orders)){
                        foreach ($orders as $a) {
                        ?>
                        <tr class="odd gradeX" data-id="<?php echo $base_admin.'/order/orders-confirmation-detail/'.$a->id ?>" style="cursor:pointer;cursor:hand;">
                            <td>
                                <?php echo '<strong>'.$a->name.'</strong>' ?><br>
                                <small>
                                    No.Invoice: #<?php echo $a->order_id; ?><br>
                                    Transfer to: <?php echo $a->bankt_to; ?><br>
                                    From: <?php echo $a->bank_from.' - '. $a->account_name ; ?><br>
                                </small>
                            </td>
                            <td><center><?php echo $a->status ?></center></td>
                            <td><div class="pull-right"><?php echo 'Rp. '.number_format($a->nominal,2) ?></div></td>
                            <td>
                                <center>
                                    <?php echo date('Y/m/d',strtotime($a->tanggal)) ?>
                                </center>
                            </td>
                        </tr>
                        <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <ul class="pagination pagination-sm">
                <?php
                echo @$pagination;
                ?>
            </ul>
        </div>
    </div>
    <!-- END Datatables Content -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Image</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="" method="post" enctype="multipart/form-data">
                    <div class="alert alert-danger" role="alert" style="display: block;">
                        <strong>Notifikasi!</strong> Operasi berhasil.
                    </div>
                    <div class="alert alert-success" role="alert" style="display: block;">
                      <strong>Notifikasi!</strong> Operasi berhasil.</div>
                    <div class="form-group">
                        <label>File*</label>
                        <input type="file" name="userfile" value="">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>