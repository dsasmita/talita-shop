<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-money"></i>Order<br>
                <small>Detail Konfirmasi Order</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li><a href="<?php echo $base_admin.'/order/orders-confirmation'; ?>">Konfirmasi Order</a></li>
        <li>Detail</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Detail Konirmasi Order</h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <form action="" method="post">
             <dl class="dl-horizontal">
                <dt>Dikirim Dari</dt>
                <dd><?php echo $order_conf->name ?></dd>
                <dt>Waktu Transfer</dt>
                <dd><?php echo date('Y/m/d',strtotime($order_conf->tanggal)) ?></dd>
                <dt>Detail</dt>
                <dd>No.Invoice: <a href="<?php echo $base_admin.'/order/detail/'.$order_conf->order_id ?>" target="_blank">#<?php echo $order_conf->order_id; ?></a></dd>
                <dd>Transfer to: <?php echo $order_conf->bankt_to; ?></dd>
                <dd>From: <?php echo $order_conf->bank_from.' - '. $order_conf->account_name ; ?></dd>
                <dt>Jumlah Transfer</dt>
                <dd><?php echo 'Rp. '.number_format($order_conf->nominal,2) ?></dd>
                <dt>Ubah Status</dt>
                <dd>
                    <input type="hidden" name="id" value="<?php echo $order_conf->id ?>">
                    <select name="status" class="">
                        <option <?php echo $order_conf->status == 'pending' ? 'selected' : '' ?> value="pending">Pending</option>
                        <option <?php echo $order_conf->status == 'cancel' ? 'selected' : '' ?> value="cancel">Cancel</option>
                        <option <?php echo $order_conf->status == 'accepted' ? 'selected' : '' ?> value="accepted">Accepted</option>
                    </select>
                </dd>
                <dd>
                    <button class="btn btn-primary" type="submit">Ubah</button>
                </dd>
            </dl>
        </form>
    </div>
    <!-- END Datatables Content -->
</div>