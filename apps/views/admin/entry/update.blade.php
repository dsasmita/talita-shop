<div id="page-content">
    <!-- Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-table"></i><?php echo $type['label']['plural'] ?><br>
                <small>Update</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li><a href="<?php echo $base_admin.'/entry/'.$entry->entry_type; ?>"><?php echo $type['label']['plural'] ?></a></li>
        <li>Update</li>
    </ul>
    <!-- END Header -->

    <!-- form Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Update <strong><?php echo $type['label']['singular'] ?></strong></h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <?php if ($this->session->flashdata('info') == 'error'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  Silahkan isi data dengan benar.
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('info') == 'success'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  Update berhasil.
                    </div>
                </div>
            </div>
        <?php } ?>
        <form role="form" action="<?php echo current_url(); ?>" method="post">
            <div class="form-group">
                <label>Title*</label>
                <input class="form-control" id="title" name='entry[title]' value="<?php echo $entry->title ?>" placeholder="title" required>
            </div>
            <div class="form-group">
                <label>Content</label>
                <textarea class="form-control ckeditor" name='entry[content]'><?php echo $entry->content ?></textarea>
            </div>
            <div class="form-group">
                <label>Excerpt</label>
                <textarea placeholder="excerpt" class="form-control" id="excerpt" name='entry[excerpt]'><?php echo $entry->excerpt ?></textarea>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="entry[status]">
                    <option <?php echo $entry->status == 'published' ?'selected':'' ?> value="published">Published</option>
                    <option <?php echo $entry->status == 'draft' ?'selected':'' ?> value="draft">Draft</option>
                </select>
            </div>
            <?php
            $blacklisted = array(
                'seo_title',
                'seo_description',
                'seo_keywords'
            );
            foreach ($type['meta'] as $key => $val) {
                $params = array();
                if($val['visibility'] != 'system'):
                    if(!in_array($key, $blacklisted)):
                        $func = 'form_'.$val['type'];
                        $params = array(
                            'name'  => "entrymeta[$key]",
                            'label' => $val['label'],
                        );

                        if (isset($val["help-block"]))
                            $params["help-block"] = $val["help-block"];

                        if (!empty($val["class"]))
                            $params["class"] = $val["class"];
                        if($val['type'] == 'checkbox' OR $val['type'] == 'radio' OR $val['type'] == 'pulldown') {
                            $params['option'] = $val['option'];
                        }

                        if(isset($val['multilanguage']) && $val['multilanguage'] == true) {
                            $params['name'] = "entrymeta[$key][".$this->lang->lang()."]";
                            $params['lang'] = $this->lang->lang();
                            $params['value'] = parse_multi_lang(@$metas[$key][$val['data_type']]);

                            echo $func($params);

                            foreach($this->lang->lang_available() as $langs => $value):
                                if($langs != $this->lang->lang()):
                                    $params['name'] = "entrymeta[$key][$langs]";
                                    $params['lang'] = $langs;
                                    $params['value'] = parse_multi_lang(@$metas[$key][$val['data_type']], $langs);

                                    echo $func($params);
                                endif;
                            endforeach;
                        }else {
                            $params['name'] = "entrymeta[$key]";
                            $params['value'] = @$metas[$key][$val['data_type']];

                            echo $func($params);
                        }
                    endif;
                endif;
            }
            ?>
            <?php
            foreach ($taxonomy as $key=>$taxo){
            ?>
                <div class="form-group taxo-group taxo-group-<?php echo $key ?>">
                    <label>Pilihan <?php echo $key ?> <a href="#" data-id="<?php echo $key ?>" class="btn btn-info btn-xs taxo-add"><i class="fa fa-plus fa-fw"></i> Add</a></label>
                    <?php
					if($taxo){
						foreach ($taxo as $t){
						?>
							<div class="checkbox taxo-option-<?php echo $t->id ?>">
								<label>
									<input type="checkbox" <?php echo (in_array($t->id,$taxonomy_list)?'checked':'');?> value="<?php echo $t->id ?>" name="taxonomy[]"><?php echo $t->name ?> <a href="#" class="taxo-update" data-id="<?php echo $key ?>" data-desc="<?php echo $t->description ?>" data-name="<?php echo $t->name ?>" data-key="<?php echo $t->id ?>"><i class="fa fa-edit fa-fw"></i></a>
								</label>
							</div>
						<?php }
					} ?>
                </div>
            <?php
            }
            ?>
            <div class="alert alert-info">
                SEO
            </div>
            <div class="form-group">
                <label>Title</label>
                <input placeholder="SEO title" class="form-control" id="seo_title" name='entrymeta[seo_title]' value="<?php echo @$metas['seo_title']['text'] ?>">
            </div>
            <div class="form-group">
                <label>Keywords</label>
                <input placeholder="SEO keywords" class="form-control" id="seo_keywords" name='entrymeta[seo_keywords]' value="<?php echo @$metas['seo_keywords']['text'] ?>">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea placeholder="SEO description" class="form-control" name='entrymeta[seo_description]'><?php echo @$metas['seo_description']['text'] ?></textarea>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Image 
                    <a href="javascript:void(0)" class="btn btn-info btn-xs addImage"><i class="fa fa-plus fa-fw"></i> Add new</a>
                </div>
                <div class="panel-body image-group">
                    <?php
                    if(empty($media)){
                        ?>
                        <div class="alert alert-warning">
                            no image found!
                        </div>
                        <?php
                    }else{
                        foreach ($media as $m) {
                            ?>
                            <div class="col-sm-6 col-md-4 col-lg-2">
                                <div class="thumbnail">
                                    <img src="<?php echo base_url('public/'.getParsedLink($m['path'], 'thumbnail')) ?>" alt="">
                                    <div class="caption text-center">
                                        <p><a href="javascript:void(0)" class="btn btn-warning btn-xs" data-id="<?php echo $m['id'] ?>" role="button">delete</a>
                                        <a href="javascript:void(0)" data-path="<?php echo base_url('public/'.$m['path']) ?>" class="btn btn-info btn-xs imagePath" role="button">path</a></p>
                                        <input type="hidden" id="" name="media[]" value="<?php echo $m['id'] ?>" class="" />
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                    }
                    ?>
                </div>
            </div>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    <!-- END form Content -->
<!-- modal -->
<div class="modal fade" id="taxoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="" method="post">
                    <input type="hidden" name="type" class="type"/>
                    <input type="hidden" name="id" class="id" value="0"/>
                    <div class="alert alert-danger" role="alert" style="display: block;">
                      <strong>Notifikasi!</strong> Isi form dengan benar.</div>
                    <div class="alert alert-success" role="alert" style="display: block;">
                      <strong>Notifikasi!</strong> Operasi berhasil.</div>
                    <div class="form-group">
                        <label>Name*</label>
                        <input class="form-control name" id="name" name='name' placeholder="name" required>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control deskripsi" maxlength="2000" rows="3" placeholder="description" name="description"></textarea>
                    </div>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<!-- modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Image</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="" method="post" enctype="multipart/form-data">
                    <div class="alert alert-danger" role="alert" style="display: block;"></div>
                    <div class="alert alert-success" role="alert" style="display: block;">
                      <strong>Notifikasi!</strong> Operasi berhasil.</div>
                    <div class="form-group">
                        <label>File*</label>
                        <input type="file" name="userfile" value="">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
</div>