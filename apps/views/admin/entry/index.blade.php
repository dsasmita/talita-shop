<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-table"></i><?php echo $type['label']['plural'] ?><br>
                <small>Index</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li><?php echo $type['label']['plural'] ?></li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>List <strong><?php echo $type['label']['singular'] ?></strong></h2>
            <?php if ($type['label']['singular'] != 'Static Page'){ ?>
            <div class="block-options pull-right">
                <a href="<?php echo $base_admin.'/entry/'.$type_entry.'/add';?>" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Tambah <?php echo $type['label']['singular'] ?></a>
            </div>
            <?php } ?>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <?php if ($this->session->flashdata('info') == 'success'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  tambah data berhasil.
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>Titel</th>
                        <th class="text-center">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </th>
                        <th class="text-center">&nbsp;&nbsp;&nbsp;Aksi&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(!empty($entries)){
                        $i = 1;
                        foreach ($entries as $a) {
                            ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $base_admin.'/entry/'.$type_entry.'/edit/'.$a->id ?>"><?php echo $a->title ?></a>
                                    <br><small><em><?php echo word_limiter($a->excerpt, 10); ?></em></small>
                                    <br><em><small><?php echo date('M,j Y, H:i',strtotime($a->updated_at)) ?></small></em>
                                </td>
                                <td>
                                    <span class="label <?php echo $a->status == 'published' ? 'label-success' : 'label-default' ?>"><?php echo $a->status ?></span><br>                                        
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="<?php echo $base_admin.'/entry/'.$type_entry.'/edit/'.$a->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                        <a onClick="return confirm('Apakah anda akan menghapus entry ini?')" href="<?php echo $base_admin.'/entry/'.$type_entry.'/delete/'.$a->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <ul class="pagination pagination-sm">
                <?php
                echo @$pagination;
                ?>
            </ul>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>
