<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
<h1>
                <i class="hi hi-folder-open"></i>Produk<br>
                <small>Tambah Produk</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li><a href="<?php echo $base_admin.'/product'; ?>">Produk</a></li>
        <li>Tambah</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Produk</h2>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <?php if ($this->session->flashdata('info') == 'error'){ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  Silahkan isi data dengan benar.
                    </div>
                </div>
            </div>
        <?php } ?>
        <form role="form" action="<?php echo current_url(); ?>" method="post">
            <div class="form-group">
                <label>Name*</label>
                <input class="form-control" id="name" name='name' value="" placeholder="product name" required>
            </div>
            <div class="form-group">
                <label>Price</label>
                <div class="form-group input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control" placeholder="000" name="price">
                </div>
            </div>
            <div class="form-group">
                <label>Special Price</label>
                <div class="form-group input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control" placeholder="000" name="special_price">
                </div>
            </div>
            <div class="form-group">
                <label>Stock</label>
                <input class="form-control" name='qty' value="" placeholder="0">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control ckeditor" name='description'></textarea>
            </div>
            <div class="form-group">
                <label>Excerpt</label>
                <textarea placeholder="excerpt" class="form-control" id="excerpt" name='excerpt'></textarea>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status">
                    <option value="published">Published</option>
                    <option value="draft">Draft</option>
                </select>
            </div>

            <div class="form-group">
                <label>Best Seller</label>
                <select class="form-control" name="best">
                    <option value="no">Not</option>
                    <option value="yes">Best Seller</option>
                </select>
            </div>
            
            
            <div class="form-group taxo-group taxo-group-katproduct">
                <label>Pilihan Category <a href="#" data-id="katproduct" class="btn btn-info btn-xs taxo-add"><i class="fa fa-plus fa-fw"></i> Add</a></label>
                <?php
                if(empty($taxonomies)){
                    echo '<p>silahkan tambah category baru.</p>';
                }else{
                    foreach ($taxonomies as $t){
                    ?>
                        <div class="checkbox taxo-option-<?php echo $t->id ?>">
                            <label>
                                <input type="checkbox" value="<?php echo $t->id ?>" name="taxonomy[]"><?php echo $t->name ?> <a href="#" class="taxo-update" data-id="katproduct" data-desc="<?php echo $t->description ?>" data-name="<?php echo $t->name ?>" data-key="<?php echo $t->id ?>"><i class="fa fa-edit fa-fw"></i></a>
                            </label>
                        </div>
                    <?php 
                    }
                }
                ?>
            </div>
            <div class="alert alert-info">
                SEO
            </div>
            <div class="form-group">
                <label>Title</label>
                <input placeholder="SEO title" class="form-control" id="seo_title" name='seo_title'>
            </div>
            <div class="form-group">
                <label>Keywords</label>
                <input placeholder="SEO keywords" class="form-control" id="seo_keywords" name='seo_keywords'>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea placeholder="SEO description" class="form-control" name='seo_description'></textarea>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Variant 
                    <a href="javascript:void(0)" class="btn btn-info btn-xs addVariant"><i class="fa fa-plus fa-fw"></i> Add new</a>
                </div>
                <div class="panel-body variant-group">
                    <div class="alert alert-warning">
                        no variant found! (option jika terdapat variant warna, ukuran dll)
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Image 
                    <a href="javascript:void(0)" class="btn btn-info btn-xs addImage"><i class="fa fa-plus fa-fw"></i> Add new</a>
                </div>
                <div class="panel-body image-group">
                    <div class="alert alert-warning">
                        no image found!
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    <!-- END Datatables Content -->
<!-- modal -->
<div class="modal fade" id="taxoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="" method="post">
                    <input type="hidden" name="type" class="type"/>
                    <input type="hidden" name="id" class="id" value="0"/>
                    <div class="alert alert-danger" role="alert" style="display: block;">
                      <strong>Notifikasi!</strong> Isi form dengan benar.</div>
                    <div class="alert alert-success" role="alert" style="display: block;">
                      <strong>Notifikasi!</strong> Operasi berhasil.</div>
                    <div class="form-group">
                        <label>Name*</label>
                        <input class="form-control name" id="name" name='name' placeholder="name" required>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control deskripsi" maxlength="2000" rows="3" placeholder="description" name="description"></textarea>
                    </div>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Image</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="" method="post" enctype="multipart/form-data">
                    <div class="alert alert-danger" role="alert" style="display: block;"></div>
                    <div class="alert alert-success" role="alert" style="display: block;">
                      <strong>Notifikasi!</strong> Operasi berhasil.</div>
                    <div class="form-group">
                        <label>File*</label>
                        <input type="file" name="userfile" value="">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="variantModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Variant</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="" method="post">
                    <input type="hidden" name="id" class="id" value="0">
                    <div class="alert alert-danger" role="alert" style="display: block;">
                        <strong>Notifikasi!</strong> Isi data dengan benar.
                    </div>
                    <div class="alert alert-success" role="alert" style="display: block;">
                      <strong>Notifikasi!</strong> Operasi berhasil.</div>
                    <div class="form-group">
                        <label>Label*</label>
                        <input class="form-control labelName" name='label' placeholder="label" required>
                    </div>
                    <div class="form-group optionsGroup">
                        <label>Options <a href="javascript:void(0)" class="btn btn-info btn-xs addOptions"><i class="fa fa-plus fa-fw"></i> Add new</a></label>
                        <div class="form-group input-group input-option">
                            <input type="text" class="form-control" name="options[]" placeholder="options">
                            <span class="input-group-btn">
                                <button class="btn btn-warning" type="button"><i class="fa fa-times"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
</div>