<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="hi hi-folder-open"></i>Produk<br>
                <small>Produk Index</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="<?php echo $base_admin; ?>">Dashboard</a></li>
        <li>Produk</li>
    </ul>
    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2>Produk</h2>
            <div class="block-options pull-right">
                <a href="<?php echo $base_admin.'/product/add';?>" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Tambah Produk</a>
            </div>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus 
            et magnis dis parturient montes,
         nascetur ridiculus mus. Donec qu</p>
         <div class="table-responsive">
            <?php if ($this->session->flashdata('info') == 'error'){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i> Operasi berhasil.
                        </div>
                    </div>
                </div>
            <?php } ?>
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th width="35%">Nama</th>
                        <th>Status</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>&nbsp;&nbsp;&nbsp;Aksi&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(!empty($product)){

                        foreach ($product as $a) {
                            ?>
                    <tr class="odd gradeX">
                        <td>
                            <a href="<?php echo $base_admin.'/product/edit/'.$a->id ?>"><?php echo $a->name ?></a>
                        </td>
                        <td><center><?php echo $a->status ?></center></td>
                        <td><div class="pull-right"><?php echo 'Rp. '.number_format($a->harga,2) ?></div></td>
                        <td class="center">
                            <div class="pull-right"><?php echo $a->qty ?></div></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="<?php echo $base_admin.'/product/edit/'.$a->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a onClick="return confirm('Apakah anda akan menghapus produk ini?')" href="<?php echo $base_admin.'/product/delete/'.$a->id ?>" data-toggle="tooltip" title="Delete" class="hidden btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <ul class="pagination pagination-sm">
                <?php
                echo @$pagination;
                ?>
            </ul>
        </div>
    </div>
    <!-- END Datatables Content -->
</div>