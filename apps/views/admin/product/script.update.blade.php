<script>
	$(document).ready(function() {
		/*text area*/
		jQuery('textarea').elastic();

		$('.taxo-add').click(function(e){
			e.preventDefault();
			var category = $(this).data('id');
			$('#taxoModal form').attr('action','<?php echo $base_admin.'/taxonomy/';?>'+category+'/add-ajax');
			$('#taxoModal .modal-title').text('Tambah '+category);
			$('#taxoModal .type').val(category);
			$('#taxoModal .name').val('');
			$('#taxoModal .deskripsi').val('');
			$('#taxoModal .alert-success').hide();
			$('#taxoModal .alert-danger').hide();
			$('#taxoModal').modal('show');
		});

		$('.taxo-group').on('click','.taxo-update',function(e){
			e.preventDefault();
			var category = $(this).data('id');
			$('#taxoModal form').attr('action','<?php echo $base_admin.'/taxonomy/';?>'+category+'/edit-ajax');
			$('#taxoModal .modal-title').text('Update '+category);
			$('#taxoModal .type').val(category);
			$('#taxoModal .id').val($(this).data('key'));
			$('#taxoModal .name').val($(this).data('name'));
			$('#taxoModal .deskripsi').val($(this).data('desc'));
			$('#taxoModal .alert-success').hide();
			$('#taxoModal .alert-danger').hide();
			$('#taxoModal').modal('show');
		});

		$('#taxoModal form').submit(function(e){
			e.preventDefault();
			$('#taxoModal form .btn-primary').button('loading');

			$('#taxoModal .alert-success').hide();
			$('#taxoModal .alert-danger').hide();

			var data = $(this).serialize();
			var url = $(this).attr('action');

			$.ajax({
				dataType: "json",
				type: "POST",
				url: url,
				data: data
			}).done(function(result) {
				$('#taxoModal form .btn-primary').button('reset');
				if(result.status == 'OK'){
					$('#taxoModal .alert-success').show();
					if(result.operation == 'add'){
						$('.taxo-group-'+result.type).append('<div class="checkbox taxo-option-'+result.id+'">'+
	                                                '<label>'+
	                                                    '<input type="checkbox" value="'+result.id+'" name="taxonomy[]">'+result.name+''+
	                                                    '<a href="#" class="taxo-update" data-id="'+result.type+'" data-desc="'+result.desc+'" data-name="'+result.name+'" data-key="'+result.id+'">'+
														'<i class="fa fa-edit fa-fw"></i></a>'+
	                                                '</label>'+
	                                            '</div>');
					}else
					if(result.operation == 'update'){
						$('.taxo-option-'+result.id+' label').html('<input type="checkbox" value="'+result.id+'" name="taxonomy[]">'+
							result.name+' <a href="#" class="taxo-update" data-id="'+result.type+'" data-desc="'+result.desc+'" data-name="'+result.name+'" data-key="'+result.id+'">'+
							'<i class="fa fa-edit fa-fw"></i></a>');
					}
					window.setTimeout(function() {
						$('#taxoModal').modal('hide');
					}, 3000);
				}else{
					$('#taxoModal .alert-danger').show();
				}
			});
		});
		
		$('.image-group').on('click','.imagePath',function(e){
			e.preventDefault();
			var path = $(this).data('path');
			window.prompt ("Copy to clipboard: Ctrl+C, Enter", path);
		});

		$('.addImage').click(function(e){
			e.preventDefault();
			$('#imageModal form').attr('action','<?php echo $base_admin.'/media/add_entry_image_ajax';?>');
			$('#imageModal .alert-success').hide();
			$('#imageModal .alert-danger').hide();
			$('#imageModal input').val('');
			$('#imageModal form .btn-primary').button('reset');
			$('#imageModal').modal('show');
		});

		$('#imageModal form').ajaxForm({
			dataType:  'json',
			beforeSend: function() {
				var percentVal = '0%';
				$('#imageModal form .btn-primary').button('loading');
				//$('.percent').show();
				//percent.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var percentVal = percentComplete + '%';
				//percent.html('Uploading .... ' + percentVal);
			},
			success: function(xhr) {
				//console.log(xhr);
				$('#imageModal form .btn-primary').button('reset');
				$('#imageModal input').val('');
				if(xhr.status == 'OK'){
					$('.image-group .alert').hide();
					$('.image-group').append('<div class="col-sm-6 col-md-4 col-lg-2">'+
                                            '<div class="thumbnail">'+
                                             '<img src="<?php echo base_url() ?>public/'+xhr.path+'" alt="">'+
                                              '<div class="caption text-center">'+
                                                '<p><a href="javascript:void(0)" class="btn btn-warning btn-xs" data-id="'+xhr.id+'" role="button">delete</a> '+
                                                '<a href="javascript:void(0)" data-path="<?php echo base_url() ?>public/'+xhr.path_ori+'" class="btn btn-info btn-xs imagePath" role="button">path</a></p>'+
                                              '<input type="hidden" id="" name="media[]" value="'+xhr.id+'" class="" /></div></div></div>');
					$('#imageModal .alert-success').show();
					window.setTimeout(function() {
						$('#imageModal').modal('hide');
					}, 3000);
				}else{
					$('#imageModal .alert-danger').html("<strong>Notifikasi!</strong> "+xhr.message+".");
					$('#imageModal .alert-danger').show();
				}
				
			}
		});
		
		$('.image-group').on('click','.btn-warning',function(e){
			e.preventDefault();
			if (confirm("Are you sure you want to delete this media?")) {
				var img = $(this);
				var id = img.data('id');
				$.ajax({
					type: "POST",
					dataType:  'json',
					url: '<?php echo $base_admin; ?>/media/delete_entry_image_ajax',
					data : 'id='+id,
					success: function(result){
						if(result.status == 'OK'){
							img.parents('div.thumbnail').parents('div.col-lg-2').remove();
						}
					}
				});
			}
		});

		$('.addVariant').click(function(e){
			e.preventDefault();
			$('#variantModal form').attr('action','<?php echo $base_admin.'/product/variant_add';?>');
			$('#variantModal .alert-success').hide();
			$('#variantModal .alert-danger').hide();
			$('#variantModal .optionsGroup').html('<label>Options <a href="javascript:void(0)" class="btn btn-info btn-xs addOptions"><i class="fa fa-plus fa-fw"></i> Add new</a></label>'+
							                        '<div class="form-group input-group input-option">'+
							                            '<input type="text" class="form-control" name="options[]" placeholder="options">'+
							                            '<span class="input-group-btn">'+
							                                '<button class="btn btn-warning" type="button"><i class="fa fa-times"></i>'+
							                                '</button></span></div>');
			$('#variantModal input').val('');
			$('#variantModal form .btn-primary').button('reset');
			$('#variantModal').modal('show');
		});

		$('.optionsGroup').on('click','.addOptions',function(e){
			e.preventDefault();
			$('.optionsGroup').append('<div class="form-group input-group input-option">'+
				                            '<input type="text" class="form-control" name="options[]"" placeholder="options">'+
				                            '<span class="input-group-btn">'+
				                                '<button class="btn btn-warning" type="button"><i class="fa fa-times"></i>'+
				                                '</button></span></div>');
		});

		$('#variantModal form').submit(function(e){
			e.preventDefault();
			$('#variantModal form .btn-primary').button('loading');
			$('#variantModal .alert-success').hide();
			$('#variantModal .alert-danger').hide();
			var data = $(this).serialize();
			var url = $(this).attr('action');
			
			$.ajax({
				dataType: "json",
				type: "POST",
				url: url,
				data: data
			}).done(function(result) {
				$('#variantModal form .btn-primary').button('reset');
				if(result.status == 'OK'){
					$('.variant-group .alert').remove();
					if(result.operation == 'add'){
						$('.variant-group').append(result.message);
					}else if(result.operation == 'update'){
						$('.variant-group .variant-'+result.id).html(result.message);
					}
					$('#variantModal .alert-success').show();
					window.setTimeout(function() {
						$('#variantModal').modal('hide');
					}, 3000);
				}else{
					//$('#variantModal .alert-danger').show();
					$('#variantModal .alert-danger').html("<strong>Notifikasi!</strong> "+result.message+".");
					$('#variantModal .alert-danger').show();
				}
			});
		});

		$('.optionsGroup').on('click','.btn-warning',function(e){
			e.preventDefault();
			$(this).parents('.input-option').remove();
		});

		$('.variant-group').on('click','.btn-warning',function(e){
			e.preventDefault();
			if (confirm("Are you sure you want to delete this variants?")) {
				var variants = $(this);
				var id = variants.data('id');
				$.ajax({
					type: "POST",
					dataType:  'json',
					url: '<?php echo $base_admin; ?>/product/variant_del',
					data : 'id='+id,
					success: function(result){
						if(result.status == 'OK'){
							variants.parents('.form-group').remove();
						}
					}
				});
			}
		});

		$('.variant-group').on('click','.btn-info',function(e){
			e.preventDefault();
			var id = $(this).data('id');
			$.ajax({
				type: "POST",
				dataType:  'json',
				url: '<?php echo $base_admin; ?>/product/variant_update',
				data : 'id='+id,
				success: function(result){
					if(result.status == 'OK'){
						$('#variantModal form').attr('action','<?php echo $base_admin.'/product/variant_update_proses';?>');

						$('#variantModal .alert-success').hide();
						$('#variantModal .alert-danger').hide();

						$('#variantModal input.id').val(id);
						$('#variantModal input.labelName').val(result.label);
						$('#variantModal .optionsGroup').html(result.options);

						$('#variantModal form .btn-primary').button('reset');

						$('#variantModal').modal('show');
					}else{
						alert('data tidak ditemukan!');
					}
					
				}
			});
		})
	});
</script>