			<footer class="clearfix">
			    <div class="pull-right">
			        do with <i class="fa fa-heart text-danger"></i> by <a href="http://listbisnis.com" target="_blank">List Bisnis</a>
			    </div>
			    <div class="pull-left">
			        <span id="year-copy"></span> &copy; <a href="<?php echo base_url() ?>" target="_blank"><?php echo $this->superconfig->getSetting('site_name'); ?></a>
			    </div>
			</footer>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
                    </div>
                    <!-- END Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <form action="<?php echo $base_admin.'/setting/password' ?>" method="post" class="form-horizontal form-bordered change-pass">
                            <fieldset>
                                <legend>Vital Info</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Username</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?php echo ucwords($this->session->userdata('username'));?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-email">Email</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?php echo ucwords($this->session->userdata('email'));?></p>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Password Update</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                                    <div class="col-md-8">
                                        <input required type="password" id="user-settings-password" name="password" class="form-control" placeholder="Please choose a complex one..(min 5 karakter)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                                    <div class="col-md-8">
                                        <input required type="password" id="user-settings-repassword" name="repassword" class="form-control" placeholder="..and confirm it!">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Modal Body -->
                </div>
            </div>
        </div>
        <!-- END User Settings -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="<?php echo base_url('public/backend/js/vendor/jquery-1.11.1.min.js') ?>"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="<?php echo base_url('public/backend/js/vendor/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('public/backend/js/plugins.js') ?>"></script>
        <script src="<?php echo base_url('public/backend/js/app.js') ?>"></script>

        <?php
        if(!empty($plugin['js'])){
            foreach ($plugin['js'] as $value) {
                ?>
                <script src="<?php echo base_url('public/backend/js/'.$value) ?>"></script>
                <?php
            }
        }
        if(!empty($script)){
            echo @$script;
        }
        ?>
        <script>
        $(document).ready(function(){
            $('.change-pass').submit(function(e){
                e.preventDefault();
                var url = $(this).attr('action');
                var data = $(this).serialize();
                $(".change-pass .btn-primary").button('loading');
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: url,
                    data: data
                }).done(function(result) {
                    $(".change-pass .btn-primary").button('reset');
                    if(result.status == 'OK'){
                        alert('Ubah status berhasil');
                        location.reload();
                    }else{
                        alert('Terjadi kesalahan.')
                    }
                });
            });
        });
        </script>
    </body>
</html>