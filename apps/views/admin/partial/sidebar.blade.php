<body>
    <div id="page-container" class="header-fixed-top sidebar-partial sidebar-visible-lg sidebar-no-animations footer-fixed">    
        <!-- Main Sidebar -->
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div class="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <a href="<?php echo $base_admin; ?>" class="sidebar-brand">
                        <i class="fa fa-home"></i><strong>Adm</strong>Area
                    </a>
                    <!-- END Brand -->

                    <!-- User Info -->
                    <div class="sidebar-section sidebar-user clearfix">
                        <div class="sidebar-user-avatar">
                            <a href="<?php echo $base_admin.'/setting/profile' ?>">
                                <img src="<?php echo $this->session->userdata('avatar') == '0' ? base_url('public/backend/img/placeholders/avatars/default-user-image.png'): getParsedLink(base_url($this->session->userdata('avatar')),'thumbnail') ?>" alt="avatar">
                            </a>
                        </div>
                        <div class="sidebar-user-name"><?php echo ucwords($this->session->userdata('username'));?></div>
                        <div class="sidebar-user-links">
                            <a href="<?php echo $base_admin.'/setting/profile' ?>" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                            <!--
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                            <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                            <a href="#modal-user-settings" data-toggle="modal" class="enable-tooltip" data-placement="bottom" title="Ubah Password"><i class="gi gi-cogwheel"></i></a>
                            <a href="<?php echo $base_admin.'/logout' ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                            <a href="<?php echo base_url() ?>" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Beranda"><i class="gi gi-new_window"></i></a>
                        </div>
                    </div>
                    <!-- END User Info -->

                    <!-- Sidebar Navigation -->
                    <ul class="sidebar-nav">
                        <li>
                            <a href="<?php echo $base_admin.'/dashboard'; ?>" class="<?php echo !empty($nav) && $nav=='dashboard' ? 'active' :'' ?>"><i class="gi gi-stopwatch sidebar-nav-icon"></i>Dashboard</a>
                        </li>
                        <li class="sidebar-header">
                            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Menu mengenai hal-hal pengaturan E-Commerce"><i class="gi gi-lightbulb"></i></a></span>
                            <span class="sidebar-header-title">Menu E-Commerce</span>
                        </li>
                        <li>
                            <a href="<?php echo $base_admin.'/account'; ?>" class="<?php echo !empty($nav_sub) && $nav_sub=='account_list' ? 'active' :'' ?>"><i class="gi gi-group sidebar-nav-icon"></i>Akun</a>
                        </li>
                        <li class="hidden">
                            <a href="#" onclick="alert('dalam pengembangan!')"><i class="gi gi-charts sidebar-nav-icon"></i>Statistik</a>
                        </li>
                        <li class="hidden <?php echo !empty($nav) && $nav=='ecomm-order' ? 'active' :'' ?>">
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-money sidebar-nav-icon"></i>Order</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $base_admin.'/orders' ?>" class="<?php echo !empty($nav_sub) && $nav_sub=='ecomm-order-index' ? 'active' :'' ?>">List Order</a>
                                </li>
                                <li>
                                    <a href="<?php echo $base_admin.'/order/orders-confirmation' ?>" class="<?php echo !empty($nav_sub) && $nav_sub=='ecomm-order-conf' ? 'active' :'' ?>">Order Konfirmasi</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php echo !empty($nav) && $nav=='ecomm-product' ? 'active' :'' ?>">
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="hi hi-folder-open sidebar-nav-icon"></i>Produk</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $base_admin.'/product' ?>" class="<?php echo !empty($nav_sub) && $nav_sub=='ecomm-product-index' ? 'active' :'' ?>">Daftar Produk</a>
                                </li>
                                <li>
                                    <a href="<?php echo $base_admin.'/product/add' ?>" class="<?php echo !empty($nav_sub) && $nav_sub=='ecomm-product-create' ? 'active' :'' ?>">Tambah Produk</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php echo !empty($nav_sub) && $nav_sub == 'taxo_katproduct' ? 'active' : '';?>">
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-adjust_alt sidebar-nav-icon"></i>Kategori</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $base_admin.'/taxonomy/katproduct'; ?>" class="<?php echo !empty($nav_sub) && $nav_sub == 'taxo_katproduct' ? 'active' : '';?>">Kategori Produk</a>
                                </li>
                            </ul>
                        </li>

                        <!--
                        <li class="sidebar-header">
                            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Menu News & Blog"><i class="gi gi-lightbulb"></i></a></span>
                            <span class="sidebar-header-title">Blog</span>
                        </li>
                        <li>
                            <a href="<?php echo $base_admin.'/entry/blog'; ?>" class="<?php echo !empty($nav) && $nav=='entries' && !empty($nav_sub) && $nav_sub == 'blog' ? 'active' :'' ?>"><i class="gi gi-table sidebar-nav-icon"></i>List Blog</a>
                        </li>
                        <li class="<?php echo !empty($nav_sub) && $nav_sub == 'taxo_katblog' ? 'active' : '';?>">
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-adjust_alt sidebar-nav-icon"></i>Kategori</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $base_admin.'/taxonomy/katblog'; ?>" class="<?php echo !empty($nav_sub) && $nav_sub == 'taxo_katblog' ? 'active' : '';?>">Kategori Blog</a>
                                </li>
                            </ul>
                        </li>
                        -->
                        
                        <li class="sidebar-header">
                            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Menu Pengaturan"><i class="gi gi-lightbulb"></i></a></span>
                            <span class="sidebar-header-title">Pengaturan</span>
                        </li>
                        <li>
                            <a href="<?php echo $base_admin.'/entry/page'; ?>" class="<?php echo !empty($nav) && $nav=='entries' && !empty($nav_sub) && $nav_sub == 'page' ? 'active' :'' ?>"><i class="gi gi-file sidebar-nav-icon"></i>Halaman Statis</a>
                            <a href="<?php echo $base_admin.'/entry/carousel'; ?>" class="<?php echo !empty($nav) && $nav=='entries' && !empty($nav_sub) && $nav_sub == 'carousel' ? 'active' :'' ?>"><i class="gi gi-camera sidebar-nav-icon"></i>Carousel</a>
                            <a href="<?php echo $base_admin.'/setting'; ?>" class="<?php echo !empty($nav) && $nav=='setting' && !empty($nav_sub) && $nav_sub == 'setting_general' ? 'active' :'' ?>"><i class="gi gi-settings sidebar-nav-icon"></i>Pengaturan Umum</a>
                        </li>
                    </ul>
                    <!-- END Sidebar Navigation -->
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>