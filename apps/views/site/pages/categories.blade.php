<div class="page">
   <h6><?php echo $title ?></h6>
</div>
<div class="content">
   <div class="col-md-9">
      <div class="content-bottom">
         <h3><?php echo $category->name ?></h3>
         <div class="bottom-grid">
            <?php
               if(!empty($product)){
                   foreach ($product as $data) {
                       $media = $data->getFirstMedia($data->id);
               ?>
            <div class="col-md-4 shirt">
               <div class="bottom-grid-top" style="margin-bottom: 15px;">
                  <a href="<?php echo base_url('produk/'.$data->slug) ?>" title="<?php echo $data->name ?>">
                     <img class="img-responsive" src="<?php echo $media ? getParsedLink(base_url('public/'.$media->path),'artikel') :'' ?>" alt="" >
                     <div class="five">
                     </div>
                     <div class="pre">
                        <p><?php echo substr($data->name,0,15) ?></p>
                        <span>Rp. <?php echo number_format($data->harga) ?></span>
                        <div class="clearfix"> </div>
                     </div>
                  </a>
               </div>
            </div>
            <?php
               }
               }
               ?>
            <div class="clearfix"> </div>
         </div>
      </div>
   </div>
   <div class="col-md-3 col-md">
      <div class="content-bottom-grid">
         <h3>Best Sellers</h3>
         <?php
            if(!empty($best_product)){
                foreach ($best_product as $data) {
                    $media = $data->getFirstMedia($data->id);
            ?>
         <div class="latest-grid">
            <div class="news">
               <a href="<?php echo base_url('produk/'.$data->slug) ?>"><img class="img-responsive" src="<?php echo $media ? getParsedLink(base_url('public/'.$media->path),'thumbnail') :'' ?>" title="name" alt=""></a>
            </div>
            <div class="news-in">
               <h6><a href="<?php echo base_url('produk/'.$data->slug) ?>"><?php echo $data->name ?></a></h6>
               <ul>
                  <li>Harga: <span>Rp. <?php echo number_format($data->harga) ?></span> </li>
               </ul>
            </div>
            <div class="clearfix"> </div>
         </div>
         <?php
            }
            }
            ?>
      </div>
      <!---->
      <div class="money">
         <h3>Payment Options</h3>
         <ul class="money-in">
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p1.png" title="name" alt=""></a></li>
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p2.png" title="name" alt=""></a></li>
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p3.png" title="name" alt=""></a></li>
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p4.png" title="name" alt=""></a></li>
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p5.png" title="name" alt=""></a></li>
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p6.png" title="name" alt=""></a></li>
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p1.png" title="name" alt=""></a></li>
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p4.png" title="name" alt=""></a></li>
            <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p2.png" title="name" alt=""></a></li>
         </ul>
      </div>
   </div>
   <div class="clearfix"> </div>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b48133b4f57026"></script>