<link rel="stylesheet" href="<?php echo base_url('/') ?>public/site/css/etalage.css">
<script src="<?php echo base_url('/') ?>public/site/js/jquery.etalage.min.js"></script>
<script>
    jQuery(document).ready(function($) {

        $('#etalage').etalage({
            thumb_image_width: 300,
            thumb_image_height: 225,
            source_image_width: 800,
            source_image_height: 600,
            show_hint: true,
            click_callback: function(image_anchor, instance_id) {
                alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
            }
        });

    });
</script>
<script type="text/javascript" src="<?php echo base_url('/') ?>public/site/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url('/') ?>public/site/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
</script>
<div class="page">
    <h6><a href="<?php echo base_url('/') ?>">Beranda</a><b>|</b> <?php echo $title ?></h6>
</div>
<div class="content">

    <div class="col-md-9">
        <div class="col-md-5 single-top">
            <ul id="etalage">
            	<?php
            	$media = $detail->getMediaListByProduct($detail->id);
            	foreach ($media as $med) {
            	?>
                <li>
                    <a href="optionallink.html">
						<img class="etalage_thumb_image img-responsive" src="<?php echo $med ? getParsedLink(base_url('public/'.$med['path']),'artikel') :'' ?>" alt="" >
						<img class="etalage_source_image img-responsive" src="<?php echo $med ? getParsedLink(base_url('public/'.$med['path'])) :'' ?>" alt="" >
					</a>
                </li>
                <?php
            	}
                ?>
            </ul>

        </div>
        <div class="col-md-7 single-top-in">
            <div class="single-para">
                <h4><?php echo $detail->name ?></h4>
                <div class="para-grid">
                    <span class="add-to">Rp. <?php echo number_format($detail->harga) ?></span>
                    <div class="clearfix"></div>
                </div>
                <h5>Stok : <?php echo number_format($detail->qty) ?></h5>
                <div class="available">
                    <?php echo $detail->description ?>
                </div>
                <div class="share hidden">
                    <h4>Share Product :</h4>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <ul class="share_nav hidden">
                        <li><a href="#"><img src="<?php echo base_url('/') ?>public/site/images/facebook.png" title="facebook"></a>
                        </li>
                        <li><a href="#"><img src="<?php echo base_url('/') ?>public/site/images/twitter.png" title="Twiiter"></a>
                        </li>
                        <li><a href="#"><img src="<?php echo base_url('/') ?>public/site/images/rss.png" title="Rss"></a>
                        </li>
                        <li><a href="#"><img src="<?php echo base_url('/') ?>public/site/images/gpluse.png" title="Google+"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
        <ul id="flexiselDemo1">
        	<?php
            if(!empty($product)){
                foreach ($product as $data) {
                    $media = $data->getFirstMedia($data->id);
            ?>
	            <li><img src="<?php echo $media ? getParsedLink(base_url('public/'.$media->path),'artikel') :'' ?>" />
	                <div class="grid-flex"><a href="<?php echo base_url('produk/'.$data->slug) ?>"><?php echo $data->name ?></a>
	                    <p>Rp. <?php echo number_format($data->harga) ?></p>
	                </div>
	            </li>
            <?php
        		}
        	}
            ?>
        </ul>
        <script type="text/javascript">
            $(window).load(function() {
                $("#flexiselDemo1").flexisel({
                    visibleItems: 5,
                    animationSpeed: 1000,
                    autoPlay: true,
                    autoPlaySpeed: 3000,
                    pauseOnHover: true,
                    enableResponsiveBreakpoints: true,
                    responsiveBreakpoints: {
                        portrait: {
                            changePoint: 480,
                            visibleItems: 1
                        },
                        landscape: {
                            changePoint: 640,
                            visibleItems: 2
                        },
                        tablet: {
                            changePoint: 768,
                            visibleItems: 3
                        }
                    }
                });

            });
        </script>
        <script type="text/javascript" src="<?php echo base_url('/') ?>public/site/js/jquery.flexisel.js"></script>
        <!---->

        <!---->
    </div>
    <div class="col-md-3 col-md">
        <div class="content-bottom-grid">
            <h3>Best Sellers</h3>
            <?php
            if(!empty($best_product)){
                foreach ($best_product as $data) {
                    $media = $data->getFirstMedia($data->id);
            ?>
            <div class="latest-grid">
                <div class="news">
                    <a href="<?php echo base_url('produk/'.$data->slug) ?>"><img class="img-responsive" src="<?php echo $media ? getParsedLink(base_url('public/'.$media->path),'thumbnail') :'' ?>" title="name" alt=""></a>
                </div>
                <div class="news-in">
                    <h6><a href="<?php echo base_url('produk/'.$data->slug) ?>"><?php echo $data->name ?></a></h6>
                    <ul>
                        <li>Harga: <span>Rp. <?php echo number_format($data->harga) ?></span> </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <?php
                }
            }
            ?>
        </div>
        <!---->
        <div class="money">
            <h3>Payment Options</h3>
            <ul class="money-in">
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p1.png" title="name" alt=""></a>
                </li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p2.png" title="name" alt=""></a>
                </li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p3.png" title="name" alt=""></a>
                </li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p4.png" title="name" alt=""></a>
                </li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p5.png" title="name" alt=""></a>
                </li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p6.png" title="name" alt=""></a>
                </li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p1.png" title="name" alt=""></a>
                </li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p4.png" title="name" alt=""></a>
                </li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p2.png" title="name" alt=""></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"> </div>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b48133b4f57026"></script>