
<div class="page">
    <h6><?php echo $title ?></h6>
</div>
<div class="content">

    <div class="col-md-9">
        <div class="content-bottom">
            <h3><?php echo $page->title ?></h3>
            <div class="bottom-grid">
                <?php echo $page->content; ?>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-md">
        <div class="content-bottom-grid">
            <h3>Produk Terbaru</h3>
            <?php
            if(!empty($product)){
                foreach ($product as $data) {
                    $media = $data->getFirstMedia($data->id);
            ?>
            <div class="latest-grid">
                <div class="news">
                    <a href="<?php echo base_url('produk/'.$data->slug) ?>"><img class="img-responsive" src="<?php echo $media ? getParsedLink(base_url('public/'.$media->path),'thumbnail') :'' ?>" title="name" alt=""></a>
                </div>
                <div class="news-in">
                    <h6><a href="<?php echo base_url('produk/'.$data->slug) ?>"><?php echo $data->name ?></a></h6>
                    <ul>
                        <li>Harga: <span>Rp. <?php echo number_format($data->harga) ?></span> </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <?php
                }
            }
            ?>
        </div>
        <!---->
        <div class="money">
            <h3>Payment Options</h3>
            <ul class="money-in">
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p1.png" title="name" alt=""></a></li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p2.png" title="name" alt=""></a></li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p3.png" title="name" alt=""></a></li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p4.png" title="name" alt=""></a></li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p5.png" title="name" alt=""></a></li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p6.png" title="name" alt=""></a></li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p1.png" title="name" alt=""></a></li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p4.png" title="name" alt=""></a></li>
                <li><a href="#"><img class="img-responsive" src="<?php echo base_url('/') ?>public/site/images/p2.png" title="name" alt=""></a></li>

            </ul>
        </div>
    </div>
    <div class="clearfix"> </div>
</div>