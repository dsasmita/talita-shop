<!--header-->
<div class="container">
<div class="header" id="home">  
    <div class="header-para">
        <p><?php echo getSetting('site_slogan') ?></span></p>  
    </div>  
    <ul class="header-in">
        <li><a href="<?php echo base_url('tentang-kami'); ?>">TENTANG KAMI</a></li>
        <li ><a href="<?php echo base_url('kontak-kami'); ?>">KONTAK KAMI</a></li>
    </ul>
    <div class="clearfix"> </div>
</div>
<!---->
<div class="header-top">
    <div class="logo">
        <a href="<?php echo base_url('/') ?>"><img style="width:190px" src="<?php echo base_url('/') ?>public/site/images/logo.png" alt="" ></a>
    </div>
    <div class="header-top-on">
        <ul class="social-in">               
            <li><a href="#"><i class="ic"> </i></a></li>
            <li><a href="#"><i class="ic1"> </i></a></li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>
<div class="header-bottom">
    <div class="top-nav">
        <?php
        $categories = Taxonomy_model::where('taxonomy_type','katproduct')->where('status','active')->order_by('updated_at','DESC')->get();
        ?>
        <ul class="megamenu skyblue">
        <?php
        if(!empty($categories)){
            foreach ($categories as $cat) {
                echo '<li><a class="pink" href="'.base_url('categories/'.$cat->taxonomy_slug).'">'.$cat->name.'</a></li>';
                //echo '<li><a class="pink" href="#">'.$cat->name.'</a></li>';
            }
        }
        ?>
        </ul> 
    </div>
    <div class="clearfix"> </div>
</div>