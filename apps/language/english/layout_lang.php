<?php
$lang['layout.nav_home'] = "Home";
$lang['layout.nav_contact'] = "Contact";
$lang['layout.nav_about'] = "About";
$lang['layout.nav_product'] = "Product";
$lang['layout.nav_cart'] = "Cart";
$lang['layout.nav_my_profile'] = "Profile";

//navigasi instansi
$lang['layout.nav_instansi'] = "Company";
$lang['layout.nav_instansi_profil'] = "Profile";
$lang['layout.nav_instansi_visi_misi'] = "Vision & Mission";
$lang['layout.nav_instansi_sejarah'] = "History";
$lang['layout.nav_instansi_struktur'] = "Structure";
$lang['layout.nav_instansi_manajemen'] = "Management";
$lang['layout.nav_instansi_prestasi'] = "Achievement and Award";
$lang['layout.nav_instansi_mutu'] = "Quality Policy";
$lang['layout.nav_instansi_budaya'] = "Work Culture";

//side bar instansi
$lang['layout.sidebar_instansi_news'] = "News Update";
$lang['layout.readmore'] = "read more";

//navigasi news
$lang['layout.nav_berita'] = "News and Publication";
$lang['layout.nav_berita_berita-instansi'] = "News";
$lang['layout.nav_berita_tulisan-dan-opini'] = "Opinion";
$lang['layout.nav_berita_artikel'] = "Article";

$lang['layout.posted'] = "posted by";

//informasi
$lang['layout.nav_informasi'] = "Information";
$lang['layout.nav_informasi_statistik'] = "Statistics";
$lang['layout.nav_informasi_penelitian'] = "Research";
$lang['layout.nav_informasi_regulasi'] = "Regulation";
$lang['layout.nav_informasi_lowongan'] = "Career";
$lang['layout.nav_informasi_csr'] = "CSR";
$lang['layout.nav_informasi_download'] = "Downnload File";
$lang['layout.nav_informasi_tautan'] = "Link Directory";
$lang['layout.nav_informasi_laporan'] = "Report";
$lang['layout.nav_informasi_permintaan'] = "Request COnfirmation";
$lang['layout.nav_informasi_lelang'] = "Auction Announcement";
/* End of file */