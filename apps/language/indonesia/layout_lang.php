<?php
$lang['layout.nav_home'] = "Beranda";
$lang['layout.nav_contact'] = "Kontak";
$lang['layout.nav_about'] = "Tentang";
$lang['layout.nav_product'] = "Produk";
$lang['layout.nav_cart'] = "Keranjang";
$lang['layout.nav_my_profile'] = "Profil";

//navigasi instansi
$lang['layout.nav_instansi'] = "Instansi";
$lang['layout.nav_instansi_profil'] = "Profil";
$lang['layout.nav_instansi_visi_misi'] = "Visi Misi";
$lang['layout.nav_instansi_sejarah'] = "Sejarah";
$lang['layout.nav_instansi_struktur'] = "Struktur";
$lang['layout.nav_instansi_manajemen'] = "Manajemen";
$lang['layout.nav_instansi_prestasi'] = "Prestasi & Penghargaan";
$lang['layout.nav_instansi_mutu'] = "Kebijakan Mutu";
$lang['layout.nav_instansi_budaya'] = "Budaya Kerja";


//side bar instansi
$lang['layout.sidebar_instansi_news'] = "Berita Terbaru";
$lang['layout.readmore'] = "selengkapnya";

//navigasi news
$lang['layout.nav_berita'] = "Berita dan Publikasi";
$lang['layout.nav_berita_berita-instansi'] = "Berita Instansi";
$lang['layout.nav_berita_tulisan-dan-opini'] = "Tulisan dan Opini";
$lang['layout.nav_berita_artikel'] = "Artikel";

//posted
$lang['layout.posted'] = "ditulis oleh";

//informasi
$lang['layout.nav_informasi'] = "Informasi";
$lang['layout.nav_informasi_statistik'] = "Data Statistik";
$lang['layout.nav_informasi_penelitian'] = "Hasil Penelitian";
$lang['layout.nav_informasi_regulasi'] = "Regulasi";
$lang['layout.nav_informasi_lowongan'] = "Lowongan Kerja";
$lang['layout.nav_informasi_csr'] = "CSR";
$lang['layout.nav_informasi_kerjasama'] = "Kerjasama";
$lang['layout.nav_informasi_download'] = "Downnload File";
$lang['layout.nav_informasi_tautan'] = "Direktori Tautan";
$lang['layout.nav_informasi_laporan'] = "Laporan Kinerja";
$lang['layout.nav_ketentuan_layanan'] = "Ketentuan Layanan";
$lang['layout.nav_informasi_lelang'] = "Pengumuman Lelang";
/* End of file */